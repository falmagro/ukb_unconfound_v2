#!/bin/bash

for fil in "T1" "T2" "tfMRI" "rfMRI" "SWI" "dMRI" ; do  
    for elem in `cat $CONFDATA/subj.txt` ; do 
        f="$subjDir/$elem/DICOM/${fil}_strings.txt" ; 
        if [ -f $f ] ; then 
            val=`cat $f | grep sCoilSelectMeas.dOverallImageScaleCorrectionFactor | awk '{print $3}' | sort | uniq|tail -n 1`;
            if [ ! "$val" == "" ] ; then  
                echo "$elem $val" ;
            else
                echo "$elem NaN"
            fi 
        else 
            echo "$elem NaN" ; 
        fi ; 
    done > $CONFDATA/ID_${fil}SCALING.txt ;
done

paste $CONFDATA/ID_T1SCALING.txt $CONFDATA/ID_T2SCALING.txt \
      $CONFDATA/ID_SWISCALING.txt $CONFDATA/ID_dMRISCALING.txt \
      $CONFDATA/ID_rfMRISCALING.txt \
      $CONFDATA/ID_tfMRISCALING.txt  | awk '{print $1,$2,$4,$6,$8,$10,$12}' > $CONFDATA/ID_SCALING.txt

