#!/bin/bash

for elem in `cat $CONFDATA/subj.txt` ; do 
    f="$subjDir/$elem/DICOM/rfMRI.txt" ; 
    if [ -f $f ] ; then 
        echo "$elem `cat $f | grep \"Echo Time\" | awk '{print \$6}' | sed \"s|'||g\" | sed 's|\"||g' ` " ; 
    else 
        echo $elem NaN ; 
    fi ; 
done > $CONFDATA/ID_TERFMRI.txt ;

for elem in `cat $CONFDATA/subj.txt` ; do 
    f="$subjDir/$elem/DICOM/tfMRI.txt" ; 
    if [ -f $f ] ; then 
        echo "$elem `cat $f | grep \"Echo Time\" | awk '{print \$6}' | sed \"s|'||g\" | sed 's|\"||g' ` " ; 
    else 
        echo $elem NaN ; 
    fi ; 
done > $CONFDATA/ID_TETFMRI.txt ;

paste $CONFDATA/ID_TERFMRI.txt $CONFDATA/ID_TETFMRI.txt | awk '{print $1,$2,$4}' > $CONFDATA/ID_TE.txt
