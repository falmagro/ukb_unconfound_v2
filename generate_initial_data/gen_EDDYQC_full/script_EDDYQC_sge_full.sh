#!/bin/bash

for elem in `cat $CONFDATA/subj.txt` ; do
    echo "./script_EDDYQC_subject_full.py $elem"
done > jobs_EDDYQC_full.txt

jid=`fsl_sub -q short.qc -l logs_full -t jobs_EDDYQC_full.txt`

fsl_sub -j $jid -q short.qc  -l logs_full ./script_EDDYQC_gather_full.sh
