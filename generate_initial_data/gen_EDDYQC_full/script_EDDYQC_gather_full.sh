#!/bin/bash

find ./logs_full/ -empty -delete
for elem in  `find logs_full/ | sort ` ; do
    cat $elem ;
done > $CONFDATA/ID_EDDYQCFULL.txt ; 
cat $CONFDATA/ID_EDDYQCFULL.txt | awk '{print $1,$19}'> $CONFDATA/ID_YTRANSLATION.txt
#rm -r logs_full jobs_EDDYQC_full.txt

for elem in `cat $CONFDATA/subj.txt ` ; do 
    val1=`cat $CONFDATA/ID_EDDYQC.txt | grep "^$elem" | awk '{print $2,$3,$4}'` ; 
    val2=`cat $CONFDATA/ID_YTRANSLATION.txt | grep "^$elem" | awk '{print $2}'` ; 
    if [ "$val2" == "" ] ; then 
        val2="NaN" ; 
    fi ; 
    echo $elem $val1 $val2 ; 
done > $CONFDATA/ID_EDDYQC2.txt

mv $CONFDATA/ID_EDDYQC2.txt $CONFDATA/ID_EDDYQC.txt
