#!/bin/env python

import os
import sys
import argparse
import numpy as np

class MyParser(argparse.ArgumentParser):
    def error(self, message):
        sys.stderr.write('error: %s\n' % message)
        self.print_help()
        sys.exit(2)

class Usage(Exception):
    def __init__(self, msg):
        self.msg = msg

def main():
    parser = MyParser(description='BioBank Pipeline Manager')
    parser.add_argument("subjectFolder", nargs=1, help='Subject Folder')
    parser.add_argument("subjDir", nargs='?', help='Directory with all subjects', default=os.environ['subjDir'])


    argsa = parser.parse_args()

    subject = argsa.subjectFolder[0]
    subject = subject.strip()
    subjDir = argsa.subjDir.strip()

    if subject[-1] =='/':
        subject = subject[0:len(subject)-1]

    subject = subjDir + '/' + subject

    if os.path.exists(subject + '/dMRI/unusable/dMRI/data.eddy_parameters'):
        originalFile_dMRI_eddy_parameters = subject + '/dMRI/unusable/dMRI/data.eddy_parameters'
    elif os.path.exists(subject + '/dMRI/incompatible/dMRI/data.eddy_parameters'):
        originalFile_dMRI_eddy_parameters = subject + '/dMRI/incompatible/dMRI/data.eddy_parameters'
    elif os.path.exists(subject + '/dMRI/unusable/incompatible/dMRI/data.eddy_parameters'):
        originalFile_dMRI_eddy_parameters = subject + '/dMRI/unusable/incompatible/dMRI/data.eddy_parameters'
    else:
        originalFile_dMRI_eddy_parameters = subject + '/dMRI/dMRI/data.eddy_parameters'

    result=subject.split('/')[-1] + ' '

    if os.path.exists(originalFile_dMRI_eddy_parameters):
        raw_values = np.loadtxt(originalFile_dMRI_eddy_parameters)
        for i in range(16):
                result = result + str(np.nanmean(raw_values[:,i])) + ' '
        for i in range(16):
        	result = result + str(np.nanstd(raw_values[:,i])) + ' '
    else:
        result = result + 32 * ('NaN ')
    
    print(result)


if __name__ == "__main__":
    main()
