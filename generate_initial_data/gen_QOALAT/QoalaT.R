rm(list = ls())

packages <- c("caret", "corrplot", "gbm", "plyr", "randomForest", "e1071",
              "pROC", "DMwR","dplyr","pbkrtest","car","pbkrtest","doParallel",
              "ROSE","repmis")
if (length(setdiff(packages, rownames(installed.packages()))) > 0) {
  install.packages(setdiff(packages, rownames(installed.packages())),
                   repos='http://cran.us.r-project.org')
}
lapply(packages, library, character.only = TRUE)


inputFolder <- getwd()

#Download the file with the naming for the model
download.file("https://github.com/larawierenga/Qoala-T-under-construction/blob/master/Qoala_T_model.Rdata?raw=true","Qoala_T_model")
rf.tune <- get(load("Qoala_T_model"))


#Define the training dataset
trainInputFolder <- paste(inputFolder, "/TRAIN_DATA/",sep="")

setwd(trainInputFolder)
traindataset <- data.frame(read.table("aseg_stats.txt", sep=" ", header=TRUE))
row.names(traindataset) <- traindataset[,1]
traindataset <- traindataset[,-1]

row.names = "Visit_ID"

for (j in c("aparc_area_lh.txt","aparc_area_rh.txt",
            "aparc_thickness_lh.txt","aparc_thickness_rh.txt")) {
     data = data.frame(read.table(paste(j,sep=""),sep=" ",header=TRUE))
     row.names(data) <- data[,1]
     data <- data[,-1]
     traindataset <- merge(traindataset,data,by="row.names",all.x=T)
     row.names(traindataset) <- traindataset[,1]
     traindataset <- traindataset[,-1]
}

rating_yourdatafile <- data.frame(read.table("class.txt",sep=" ",header=TRUE))
dataset <- merge(rating_yourdatafile,traindataset,by.x="Visit_ID",by.y="row.names",all.y=T)
row.names(dataset) <- dataset[,1]
dataset <- dataset[,-1]

row.names = "Visit_ID"
col.names = colnames(traindataset)

dataset_names <- c("Rating",names(rf.tune$trainingData)[-ncol(rf.tune$trainingData)])
dataset_names2 <- c(names(dataset)[-ncol(dataset)+1])
dataset2 <- dataset[,dataset_names]
dataset2 <- dataset[complete.cases(dataset2[-1]),]

training = dataset[!is.na(dataset$Rating),]
training$Rating = as.factor(training$Rating)


#Define the testing dataset (where the predictions will be performe)
testInputFolder <- paste(inputFolder, "/TEST_DATA/",sep="")

setwd(testInputFolder)
testdataset <- data.frame(read.table("aseg_stats.txt", sep=" ", header=TRUE))
row.names(testdataset) <- testdataset[,1]
testdataset <- testdataset[,-1]

row.names = "Visit_ID"

for (j in c("aparc_area_lh.txt","aparc_area_rh.txt",
            "aparc_thickness_lh.txt","aparc_thickness_rh.txt")) {
     data = data.frame(read.table(paste(j,sep=""),sep=" ",header=TRUE))
     row.names(data) <- data[,1]
     data <- data[,-1]
     testdataset <- merge(testdataset,data,by="row.names",all.x=T)
     row.names(testdataset) <- testdataset[,1]
     testdataset <- testdataset[,-1]
}

rating_yourdatafile <- data.frame(read.table("class.txt",sep=" ",header=TRUE))
dataset <- merge(rating_yourdatafile,testdataset,by.x="Visit_ID",by.y="row.names",all.y=T)
row.names(dataset) <- dataset[,1]
dataset <- dataset[,-1]


row.names = "Visit_ID"
col.names = colnames(testdataset)

dataset_names <- c("Rating",names(rf.tune$trainingData)[-ncol(rf.tune$trainingData)])
dataset_names2 <- c(names(dataset)[-ncol(dataset)+1])
dataset2 <- dataset[,dataset_names]
dataset2 <- dataset[complete.cases(dataset2[-1]),]

testing = dataset[is.na(dataset$Rating),]
testing$Rating = as.factor(testing$Rating)


ctrl = trainControl(method = 'repeatedcv',
                    number = 2,
                    repeats = 10,
                    summaryFunction=twoClassSummary,
                    classProbs=TRUE,
                    allowParallel=FALSE,
                    sampling="rose") # 'rose' is used to oversample the imbalanced data

# -----------------------------------------------------------------
# Estimate model
# -----------------------------------------------------------------
rf.tune = train(y=training$Rating,
                  x=subset(training, select=-c(Rating)),
                  method = "rf",
                  metric = "ROC",
                  trControl = ctrl,
                  ntree = 501,
                  tuneGrid=expand.grid(mtry = c(8)),
                  verbose=FALSE)

# -----------------------------------------------------------------
# External cross validation on 90% of unseen data (1 repetition)
# -----------------------------------------------------------------
rf.pred <- predict(rf.tune,subset(testing, select=-c(Rating)))
rf.probs <- predict(rf.tune,subset(testing, select=-c(Rating)),type="prob")
head(rf.probs)

# -----------------------------------------------------------------
# Saving output
# ----------------------------------------------------------------
# create empty data frame
Qoala_T_predictions_subset_based <- data.frame(matrix(ncol = 4, nrow = nrow(rf.probs)))
colnames(Qoala_T_predictions_subset_based) = c('VisitID','Scan_QoalaT', 'Recommendation', 'manual_QC_adviced')

# fill data frame
Qoala_T_predictions_subset_based$VisitID <- row.names(rf.probs)
Qoala_T_predictions_subset_based$Scan_QoalaT <- rf.probs$Include*100
Qoala_T_predictions_subset_based$Recommendation <- rf.pred
Qoala_T_predictions_subset_based$manual_QC_adviced <- ifelse(Qoala_T_predictions_subset_based$Scan_QoalaT<60&Qoala_T_predictions_subset_based$Scan_QoalaT>40,"yes","no")
Qoala_T_predictions_subset_based <- Qoala_T_predictions_subset_based[order(Qoala_T_predictions_subset_based$Scan_QoalaT, Qoala_T_predictions_subset_based$VisitID),]

setwd(inputFolder)
write.csv(Qoala_T_predictions_subset_based, file = 'Qoala_T_predictions_UKB.csv', row.names=F)

