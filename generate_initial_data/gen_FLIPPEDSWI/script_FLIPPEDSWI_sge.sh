#!/bin/bash

rm -rf logs

for elem in `cat $CONFDATA/subj.txt` ; do
    echo "./script_FLIPPEDSWI_subject.sh $elem"
done > jobs_FLIPPEDSWI.txt

jid=`fsl_sub -l logs -t jobs_FLIPPEDSWI.txt`

fsl_sub -j $jid -l logs ././script_FLIPPEDSWI_gather.sh

