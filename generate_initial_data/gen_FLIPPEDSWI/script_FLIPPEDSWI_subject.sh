#!/bin/bash

elem="$1"

di="$subjDir/$elem/SWI/"
if [ -d $di/unusable ] ; then
    di="$di/unusable"
fi

dimSWI="256 288 48"

if [ -f  $di/SWI_TOTAL_MAG_orig.nii.gz ] ; then
  dimSWI1=$(fslval  $di/SWI_TOTAL_MAG_orig.nii.gz dim1 | awk '{print $1}')
  dimSWI2=$(fslval  $di/SWI_TOTAL_MAG_orig.nii.gz dim2 | awk '{print $1}')
  dimSWI3=$(fslval  $di/SWI_TOTAL_MAG_orig.nii.gz dim3 | awk '{print $1}')
  dimSWI="$dimSWI1 $dimSWI2 $dimSWI3"
elif [ -f  $di/SWI_TOTAL_MAG.nii.gz ] ; then
  dimSWI1=$(fslval  $di/SWI_TOTAL_MAG.nii.gz dim1 | awk '{print $1}')
  dimSWI2=$(fslval  $di/SWI_TOTAL_MAG.nii.gz dim2 | awk '{print $1}')
  dimSWI3=$(fslval  $di/SWI_TOTAL_MAG.nii.gz dim3 | awk '{print $1}')
  dimSWI="$dimSWI1 $dimSWI2 $dimSWI3"
else
  dimSWI="NaN"  
fi

if [ "$dimSWI" == "256 288 48" ] ; then
  flipped_SWI="0"
elif [ "$dimSWI" == "NaN" ] ; then
  flipped_SWI="NaN"
else
  flipped_SWI="1"
fi

echo "$elem $flipped_SWI"

