#!/bin/bash

deactivate
. $BB_BIN_DIR/bb_python/bb_python_funpack/bin/activate

alias funpack="python -m funpack"
alias ipython="python -m IPython"

input1="" #TODO: Fill up with the corresponding csv file coming from Biobank

rm   -rf logs names data logs_sge jobs.txt;
mkdir -p logs names data logs_sge;

while read ID NAME ; do
    cad=""
    cad="$cad cat config_template.cfg > config_${NAME}.cfg;"
    cad="$cad echo \"category           $ID\" >> config_${NAME}.cfg;"
    cad="$cad funpack --log_file logs/log_${NAME}.txt "
    cad="$cad         --description_file names/${NAME}_names.tsv "
    cad="$cad         -cfg config_${NAME}.cfg "
    cad="$cad         -s $CONFDATA/subj_2.txt "
    cad="$cad         data/${NAME}_data.txt "
    cad="$cad         $input1 $input2; "
    cad="$cad cat names/${NAME}_names.tsv | awk -F \"\t\" '{print \$2}' | "
    cad="$cad sed 's| |_|g' > names/${NAME}_names.txt; "
    cad="$cad rm  names/${NAME}_names.tsv ; "
    cad="$cad cat data/${NAME}_data.txt | tail -n +2 > $CONFDATA/ID_${NAME}.txt ;"
    cad="$cad cp  names/${NAME}_names.txt $CONFDATA/${NAME}_names.txt ; "
    cad="$cad rm config_${NAME}.cfg "
    echo $cad
done < table.txt >> jobs.txt

fsl_sub -q long.qc -l logs_sge -t jobs.txt

. $BB_BIN_DIR/bb_python/bb_python/bin/activate
