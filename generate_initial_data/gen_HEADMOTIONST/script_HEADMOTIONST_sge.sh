#!/bin/bash

for elem in `cat $CONFDATA/subj.txt` ; do
    echo "./script_HEADMOTIONST_subject.py $elem"
done > jobs_HEADMOTIONST.txt

jid=`fsl_sub -l logs -t jobs_HEADMOTIONST.txt`

fsl_sub -j $jid -l logs ./script_HEADMOTIONST_gather.sh

