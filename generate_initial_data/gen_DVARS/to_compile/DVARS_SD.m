function DVARS_SD(input, output)

    if ~isdeployed()
        addpath('DVARS-master');
        addpath('DVARS-master/Nifti_Util');
    end
    fileName=gunzip(input,tempname());
    
    [V,~] = DSEvars(fileName{1},'verbose',0);

    A = V.Avar_ts;
    S = V.Svar_ts;
    D = V.Dvar_ts;
    mean_A = mean(A);
    S_div_A = S ./ mean_A;
    D_div_A = D ./ mean_A;
    D__S_div_A = (D - S) ./ mean_A;

    dlmwrite(strcat('results/S_', output, '.txt'), S_div_A');
    dlmwrite(strcat('results/D_', output, '.txt'), D_div_A');

    delete(fileName{1});
end
