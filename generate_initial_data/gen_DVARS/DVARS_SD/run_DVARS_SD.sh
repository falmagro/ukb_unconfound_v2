#!/bin/sh
# script for execution of deployed applications
#
# Sets up the MATLAB Runtime environment for the current $ARCH and executes 
# the specified command.
#
exe_name=$0
exe_dir=`dirname "$0"`
echo "------------------------------------------"
if [ "x$1" = "x" ]; then
  echo Usage:
  echo    $0 \<deployedMCRroot\> args
else
    echo Setting up environment variables
 
    MCR_CR="mcr_${JOB_ID}"
    if [ "${SGE_TASK_ID}" != "undefined" ]; then
        MCR_CR="${MCR_CR}-${SGE_TASK_ID}"
    fi
    MCR_CACHE_ROOT="/tmp/${MCR_CR}"
    export MCR_CACHE_ROOT

    MCRROOT="$1"
    echo ---
    LD_LIBRARY_PATH=.:${MCRROOT}/runtime/glnxa64 ;
    LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:${MCRROOT}/bin/glnxa64 ;
    LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:${MCRROOT}/sys/os/glnxa64;
    LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:${MCRROOT}/sys/opengl/lib/glnxa64;
    export LD_LIBRARY_PATH;
    echo LD_LIBRARY_PATH is ${LD_LIBRARY_PATH};
    shift 1
    args=
    while [ $# -gt 0 ]; do
      token=$1
      args="${args} \"${token}\"" 
      shift
    done
    eval "\"${exe_dir}/run_mcr\"" "\"${exe_dir}/DVARS_SD\"" $args

    rm -rf "/tmp/${MCR_CR}"

fi
exit

