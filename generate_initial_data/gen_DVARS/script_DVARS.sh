#!/bin/sh

# TODO: Change appropiately for your system
module add matlab/mcr-2017a-v92
MCR="$MCR_DIR/MATLAB_Runtimes/v92"

rm   -rf results/ logs_1 logs_2
mkdir -p results/ logs_1 logs_2

rm -f jobs_1.txt jobs_2.txt

for elem in `cat $CONFDATA/subj.txt` ; do

    f1="$subjDir/$elem/fMRI/rfMRI.ica/filtered_func_data.nii.gz"
    f2="$subjDir/$elem/fMRI/rfMRI.ica/filtered_func_data_clean.nii.gz"

    if [ -f $f1 ] ; then
        echo "./DVARS_SD/run_DVARS_SD.sh $MCR $f1 ${elem}_dirty" >> jobs_1.txt
    fi 
    if [ -f $f2 ] ; then
        echo "./DVARS_SD/run_DVARS_SD.sh $MCR $f2 ${elem}_clean" >> jobs_2.txt
    fi
done

jobID1=`fsl_sub -q short.qc -l logs_1/ -t jobs_1.txt`
jobID2=`fsl_sub -q short.qc -l logs_2/ -t jobs_2.txt`

bb_wait_SGE_jobs $jobID1
bb_wait_SGE_jobs $jobID2

./script_DVARS_process.py > $CONFDATA/ID_DVARS.txt

module rm matlab/mcr-2017a-v92

