#!/bin/env python

import pickle
import numpy as np
from file_to_dict import file_to_dict

# Generate dictionaries with the data
train_data_dict   = file_to_dict('TRAIN_DATA/train_data.txt')
predict_data_dict    = file_to_dict('PREDICT_DATA/predict_data.txt')

# Get the lists of subjects 
train_subjects   = list(train_data_dict.keys())
train_subjects_1 = [ x for x in train_subjects if x.find('_1') != -1]
predict_subjects    = list(predict_data_dict.keys())

predict_subjects.sort()
train_subjects_1.sort()

num_train_subjects_1 = len(train_subjects_1)
num_predict_subjects    = len(predict_subjects)

num_vars = len(predict_data_dict[predict_subjects[0]])


# Create a matrix with the features of the first acquisitions of the training set
train_data_1 = np.zeros((num_train_subjects_1, num_vars))
for i in range(num_train_subjects_1):
    train_data_1[i,:] = train_data_dict[train_subjects_1[i]]  

# Create a matrix with the features of the predict set
predict_data = np.zeros((num_predict_subjects, num_vars))
for i in range(num_predict_subjects):
    predict_data[i,:] = predict_data_dict[predict_subjects[i]]  


# Calculate the mean and std of the total population
data_total  = np.concatenate((predict_data, train_data_1))

means_total = np.zeros(num_vars)
stds_total  = np.zeros(num_vars)

for i in range(num_vars):
    means_total[i] = np.nanmean(data_total[:,i])
    stds_total[i]  = np.nanstd( data_total[:,i])

np.savetxt('NORM/means.txt',   means_total)
np.savetxt('NORM/stds.txt',    stds_total)

