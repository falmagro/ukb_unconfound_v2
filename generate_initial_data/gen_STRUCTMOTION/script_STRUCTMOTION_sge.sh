#!/bin/bash

for elem in `cat $CONFDATA/subj.txt` ; do
    echo "./script_STRUCTMOTION_subject.sh $elem"
done > jobs.txt

rm   -rf logs
mkdir -p logs
rm   -rf PREDICT_DATA
mkdir -p PREDICT_DATA

jid1=`fsl_sub -l logs -q short.qc -t jobs.txt`
jid2=`fsl_sub -j $jid1 -l logs2 -q short.qc ./script_STRUCTMOTION_clean.sh`

