#!/bin/env python

import pickle
import numpy as np 
from file_to_dict import file_to_dict
from sklearn import linear_model as LM
from sklearn.metrics import r2_score
from sklearn.metrics import mean_squared_error
from sklearn.grid_search import GridSearchCV
from sklearn.cross_validation import train_test_split

# Generate dictionaries with the data
train_scores_dict = file_to_dict('TRAIN_DATA/train_class.txt')
train_data_dict   = file_to_dict('TRAIN_DATA/train_data.txt')

# Get the lists of subjects 
train_subjects   = list(train_scores_dict.keys())
train_subjects.sort()

num_train_subjects   = len(train_subjects)
num_vars = len(train_data_dict[train_subjects[0]])

# Create a matrix with the scores of the training set
train_scores = np.zeros(num_train_subjects)
for i in range(num_train_subjects):
    train_scores[i]=train_scores_dict[train_subjects[i]][0]

# Create a matrix with the features of the training set
train_data = np.zeros((num_train_subjects, num_vars))
for i in range(num_train_subjects):
    train_data[i,:] = train_data_dict[train_subjects[i]]  

means_total = np.loadtxt('final_results/means.txt')
stds_total  = np.loadtxt('final_results/stds.txt')


# Normalise the training set (also remove NaNs)
train_data_norm = np.zeros(train_data.shape)
for i in range(num_vars):
    train_data[np.isnan(train_data[:,i]),i] = means_total[i] 
    train_data_norm[:,i] = (train_data[:,i] - means_total[i]) / stds_total[i]


#Shuffling (Not using sklearn because we shuffle 3 things)
new_random_order = np.random.permutation(len(train_subjects))

train_subjects  = [train_subjects[x] for x in new_random_order]
train_scores    = [train_scores[x] for x in new_random_order]
train_data_norm = train_data_norm[new_random_order,:]

data_train, data_test, scores_train, scores_test = train_test_split( \
            train_data_norm, train_scores, test_size=0.20, stratify=train_scores)

models = [LM.LinearRegression, LM.BayesianRidge,  \
          LM.ElasticNetCV, LM.LarsCV, LM.Lasso, LM.LassoCV, \
          LM.LassoLars, LM.LassoLarsCV, LM.LassoLarsIC, LM.ElasticNet, \
          LM.OrthogonalMatchingPursuit,  LM.OrthogonalMatchingPursuitCV, \
          LM.PassiveAggressiveRegressor, LM.Ridge,  LM.RidgeCV, \
          LM.SGDRegressor, LM.TheilSenRegressor]

min_mse=1

for model in models:
    try:
        mod=model()
        clf = GridSearchCV(mod,{},cv=10)
        clf.fit(data_train,np.array(scores_train))
        pr=clf.predict(data_train)
        mse = mean_squared_error(np.array(scores_train,),pr)
        if min_mse > mse:
            min_mse = mse
            final_model = clf
            final_model_name = str(model)
        print('Model ' + str(model) + ' has this mse: ' + str(mse))
    except:
        print('Model ' + str(model) + ' did not work')

print()
print('Best model is: ' + final_model_name)

print()
print()

pr=final_model.predict(data_test)
mse = mean_squared_error(np.array(scores_test,),pr)
print('Final model ' + str(final_model_name) + \
      ' has this mse for the hold-out set: ' + str(mse))

print('Weights of the best model: ' + str(final_model.best_estimator_.coef_))

#Saving the model, prediction for the 24k, means, stds and weights.
with open('MODEL/model.p','wb') as f:
    pickle.dump(final_model,f)
f.close()

np.savetxt('MODEL/weights.txt', final_model.best_estimator_.coef_)

