#!/bin/bash

#set -x

# TODO: Adapt these lines to your system
export MODULEPATH
module add fsl
#

subjName="$1"
dire="$subjDir/$subjName"
T1d="$dire/T1/"

if [ -d "$T1d/unusable" ] ; then
    T1d="$T1d/unusable"
fi

com="smoothest"

origDir=`pwd`

res1=""
if [ -f $T1d/T1.nii.gz ] ; then
    res1=`$com -z $T1d/T1.nii.gz -m $T1d/T1_brain_mask.nii.gz | tail -n 1 | awk '{print \$2,\$3,\$4}'`
fi
if [ "$res1" == "" ] ; then
    res1="NaN NaN NaN"
fi

if [ -f $dire/IDP_files/IDPs.txt ] ; then
    res2="`cat $dire/IDP_files/IDPs.txt |awk '{print $7}'`"
    if [ "$res2" == "NaN" ] ; then
        cd $subjDir
        res2=`$origDir/scripts/bb_IDP_all_align_to_T1 $name`
        cd $origDir
    fi
    if [ "$res2" == "" ] ; then
        res2="NaN"
    fi
else
    res2="NaN"
fi

res3=""
for fil in "T1_orig_QC_CNR_upper.txt" "T1_orig_QC_CNR_lower.txt" "T1_notNorm_QC_CNR_upper.txt" "T1_notNorm_QC_CNR_lower.txt" "T1_QC_CNR_eyes.txt" ; do
    if [ -f $T1d/QC_T1_files/$fil ] ; then
        res3="$res3 `cat $T1d/QC_T1_files/$fil`"
    else
        res3="$res3 NaN"
    fi
done

res4=`cat $CONFDATA/ID_QOALAT.txt | grep "^$subjName" | awk '{print $2}'`
if [ "$res4" == "" ] ; then
    res4="NaN"
fi


# Adapt this to the format in which we will have the FreeSurfer Data
res5="NaN"
lf="$SUBJECTS_DIR/$subjName/surf/lh.orig.nofix"
rf="$SUBJECTS_DIR/$subjName/surf/rh.orig.nofix"
if [ ! -f $lf ] ; then
    res5="NaN"
elif [ ! -f $rf ] ; then
    res5="NaN"
else
    res51=`$(dirname $(which recon-all))/mris_euler_number $lf  2>&1 | head -n 1 | awk '{print $13}'` 
    res52=`$(dirname $(which recon-all))/mris_euler_number $rf  2>&1 | head -n 1 | awk '{print $13}'`
    res5=`echo " ( $res51 + $res52 ) / 2" | bc`
fi

echo "$subjName $res1 $res2 $res3 $res4 $res5"

module remove fsl
