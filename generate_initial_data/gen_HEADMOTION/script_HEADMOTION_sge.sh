#!/bin/bash

for elem in `cat $CONFDATA/subj.txt` ; do
    echo "./script_HEADMOTION_subject.py $elem"
done > jobs_HEADMOTION.txt

jid=`fsl_sub -l logs -t jobs_HEADMOTION.txt`

fsl_sub -j $jid -l logs ././script_HEADMOTION_gather.sh

