#!/bin/bash

# Note: Needs a directory with DICOMs

if [ ! -f $CONFDATA/ID_FLIPPEDSWI.txt ] ; then
    ../gen_FLIPPEDSWI/script_FLIPPEDSWI_serial.sh
fi

for elem in `cat $CONFDATA/subj.txt` ; do 
    dat=`cat $subjDir/$elem/DICOM/T1.txt | grep "Acquisition Date" | awk '{print $6}' | sed "s|'||g" `
    tim=`cat $subjDir/$elem/DICOM/T1.txt | grep "Acquisition Time" | awk '{print $6}' | sed "s|'||g" `
    site=`cat $CONFDATA/IDPs.txt | grep "^$elem" | awk '{print $893}'`
    SWI=`cat $CONFDATA/ID_FLIPPEDSWI.txt | grep "$elem" | awk '{print $2}'`
    echo "$elem $dat $tim NaN $site $SWI" ; # 4th column is unimportant. They are not used
done > $CONFDATA/initial_workspace.txt

