#!/bin/env python

# This code generates:
#   - ID_BATCH.txt  
#   - ID_SITE.txt
#   - ID_REFAMP.txt
#   - ID_SCANRAMP.txt
#   - ID_SCANMISC.txt
#   - ID_SCANCOLDHEAD.txt
#   - ID_SCANHEADCOIL.txt
#   - ID_SERVICEPACK.txt
#   - ID_PROTOCOL.txt
#   - ID_CMRR.txt
#   - ID_REPT1.txt

import os
import pickle
import pydicom
import os.path
import datetime
import collections
import numpy as np

outputDir = os.environ['CONFDATA']+'/'

print('Before loading: ' + str(datetime.datetime.now()))
with open('BB.pickle', 'rb') as handle:
    BB=pickle.load(handle)

print('After loading: ' + str(datetime.datetime.now()))


#Generating batch file
ID_Batch=collections.OrderedDict()
for subject in sorted(BB['list_subjects']):
    ID_Batch[subject]=0
    if 'batch' in BB['subj'][subject].keys():
        ID_Batch[subject] = BB['subj'][subject]['batch']
    else:
        ID_Batch[subject] = 'NaN'
with open(outputDir + 'ID_BATCH.txt', 'w') as f:
    for subject in ID_Batch.keys():
        f.write(subject + ' ' + ID_Batch[subject] + '\n')
f.close()


#Generating site file
ID_Site=collections.OrderedDict()
for subject in sorted(BB['list_subjects']):
    ID_Site[subject] = BB['subj'][subject]['site']

with open(outputDir + 'ID_SITE.txt', 'w') as f:
    for subject in ID_Site.keys():
        f.write(subject + ' ' + str(ID_Site[subject]) + '\n')
f.close()


#Generating REFAMP file
ID_RefAmp=collections.OrderedDict()
for subject in sorted(BB['list_subjects']):
    ID_RefAmp[subject]=np.nan
    if 'ReferenceAmplitude' in BB['subj'][subject].keys():
        ID_RefAmp[subject]=BB['subj'][subject]['ReferenceAmplitude']


with open(outputDir + 'ID_REFAMP.txt', 'w') as f:
    for subject in ID_RefAmp.keys():
        f.write(subject + ' ' + str(ID_RefAmp[subject]) + '\n')
f.close()


#Ramp Date for the different sites
RampDatesSite1=[]
RampDatesSite1.append(datetime.datetime.strptime("20170722",'%Y%m%d'))
RampDatesSite1.append(datetime.datetime.strptime("20180103",'%Y%m%d'))
RampDatesSite1.append(datetime.datetime.strptime("20180224",'%Y%m%d'))
RampDatesSite1.append(datetime.datetime.strptime("20190829",'%Y%m%d'))

ID_Ramp=collections.OrderedDict()

for subject in sorted(BB['list_subjects']):
    ID_Ramp[subject]=[]
    if BB['subj'][subject]['site'] == 0:
        if BB['subj'][subject]['AcqTimestamp'] < RampDatesSite1[0]:
            ID_Ramp[subject]=1
        elif (BB['subj'][subject]['AcqTimestamp'] >= RampDatesSite1[0] and 
             BB['subj'][subject]['AcqTimestamp'] < RampDatesSite1[1]):
            ID_Ramp[subject]=2
        elif (BB['subj'][subject]['AcqTimestamp'] >= RampDatesSite1[1] and 
             BB['subj'][subject]['AcqTimestamp'] < RampDatesSite1[2]):
            ID_Ramp[subject]=3
        elif (BB['subj'][subject]['AcqTimestamp'] >= RampDatesSite1[2] and 
             BB['subj'][subject]['AcqTimestamp'] < RampDatesSite1[3]):
            ID_Ramp[subject]=4
        else:
            ID_Ramp[subject]=5
    else:
        ID_Ramp[subject]=0

with open(outputDir + 'ID_SCANRAMP.txt', 'w') as f:
    for subject in ID_Ramp.keys():
        f.write(subject + ' '+ str(ID_Ramp[subject]) + '\n')
f.close()


# Misc Change Dates for Site 1
MiscDatesSite1=[]
MiscDatesSite1.append(datetime.datetime.strptime("20160416",'%Y%m%d'))
MiscDatesSite1.append(datetime.datetime.strptime("20181207 180000",'%Y%m%d %H%M%S'))
MiscDatesSite1.append(datetime.datetime.strptime("20190705",'%Y%m%d'))

MiscDatesSite2=[]
MiscDatesSite2.append(datetime.datetime.strptime("20170405",'%Y%m%d'))
MiscDatesSite2.append(datetime.datetime.strptime("20171125",'%Y%m%d'))
MiscDatesSite2.append(datetime.datetime.strptime("20181228",'%Y%m%d'))

ID_Misc=collections.OrderedDict()
for subject in sorted(BB['list_subjects']):
    if BB['subj'][subject]['site'] == 0:
        if BB['subj'][subject]['AcqTimestamp'] < MiscDatesSite1[0]:
            ID_Misc[subject]=1
        elif (BB['subj'][subject]['AcqTimestamp'] >= MiscDatesSite1[0] and 
             BB['subj'][subject]['AcqTimestamp'] < MiscDatesSite1[1]):
            ID_Misc[subject]=2
        elif (BB['subj'][subject]['AcqTimestamp'] >= MiscDatesSite1[1] and 
             BB['subj'][subject]['AcqTimestamp'] < MiscDatesSite1[2]):
            ID_Misc[subject]=3
        elif BB['subj'][subject]['AcqTimestamp'] >= MiscDatesSite1[2]:
            ID_Misc[subject]=4
        else:
            ID_Misc[subject]=0 # Should never happen
    elif BB['subj'][subject]['site'] == 1:
        if BB['subj'][subject]['AcqTimestamp'] < MiscDatesSite2[0]:
            ID_Misc[subject]=5
        elif (BB['subj'][subject]['AcqTimestamp'] >= MiscDatesSite2[0] and 
             BB['subj'][subject]['AcqTimestamp'] < MiscDatesSite2[1]):
            ID_Misc[subject]=6
        elif (BB['subj'][subject]['AcqTimestamp'] >= MiscDatesSite2[1] and 
             BB['subj'][subject]['AcqTimestamp'] < MiscDatesSite2[2]):
            ID_Misc[subject]=7
        elif BB['subj'][subject]['AcqTimestamp'] >= MiscDatesSite2[2]:
            ID_Misc[subject]=8
        else:
            ID_Misc[subject]=0 # Should never happen
    else:
        ID_Misc[subject]=0

with open(outputDir + 'ID_SCANMISC.txt', 'w') as f:
    for subject in ID_Misc.keys():
        f.write(subject + ' '+ str(ID_Misc[subject]) + '\n')
f.close()


# ColdHead Change Dates for Site 1
ColdHeadDatesSite1=[]
ColdHeadDatesSite1.append(datetime.datetime.strptime("20190829",'%Y%m%d'))

ColdHeadDatesSite2=[]
ColdHeadDatesSite2.append(datetime.datetime.strptime("20191025",'%Y%m%d'))
ColdHeadDatesSite2.append(datetime.datetime.strptime("20191103",'%Y%m%d'))


ID_ColdHead=collections.OrderedDict()
for subject in sorted(BB['list_subjects']):
    if BB['subj'][subject]['site'] == 0:
        if BB['subj'][subject]['AcqTimestamp'] < ColdHeadDatesSite1[0]:
            ID_ColdHead[subject]=1
        elif BB['subj'][subject]['AcqTimestamp'] >= ColdHeadDatesSite1[0]:
            ID_ColdHead[subject]=2
        else:
            ID_ColdHead[subject]=0 # Should never happen
    elif BB['subj'][subject]['site'] == 1:
        if BB['subj'][subject]['AcqTimestamp'] < ColdHeadDatesSite2[0]:
            ID_ColdHead[subject]=3
        elif (BB['subj'][subject]['AcqTimestamp'] >= ColdHeadDatesSite2[0] and 
             BB['subj'][subject]['AcqTimestamp'] < ColdHeadDatesSite2[1]):
            ID_ColdHead[subject]=4
        elif BB['subj'][subject]['AcqTimestamp'] >= ColdHeadDatesSite2[1]:
            ID_ColdHead[subject]=5
        else:
            ID_ColdHead[subject]=0 # Should never happen
    else:
        ID_ColdHead[subject]=0

with open(outputDir + 'ID_SCANCOLDHEAD.txt', 'w') as f:
    for subject in ID_ColdHead.keys():
        f.write(subject + ' '+ str(ID_ColdHead[subject]) + '\n')
f.close()





# HeadCoil Change Dates for Site 1
HeadCoilDatesSite1=[]
HeadCoilDatesSite1.append(datetime.datetime.strptime("20140619",'%Y%m%d'))
HeadCoilDatesSite1.append(datetime.datetime.strptime("20161126",'%Y%m%d'))
HeadCoilDatesSite1.append(datetime.datetime.strptime("20190111",'%Y%m%d'))

HeadCoilDatesSite2=[]
HeadCoilDatesSite2.append(datetime.datetime.strptime("20180220",'%Y%m%d'))

ID_HeadCoil=collections.OrderedDict()
for subject in sorted(BB['list_subjects']):
    if BB['subj'][subject]['site'] == 0:
        if BB['subj'][subject]['AcqTimestamp'] < HeadCoilDatesSite1[0]:
            ID_HeadCoil[subject]=1
        elif (BB['subj'][subject]['AcqTimestamp'] >= HeadCoilDatesSite1[0] and 
             BB['subj'][subject]['AcqTimestamp'] < HeadCoilDatesSite1[1]):
            ID_HeadCoil[subject]=2
        elif (BB['subj'][subject]['AcqTimestamp'] >= HeadCoilDatesSite1[1] and 
             BB['subj'][subject]['AcqTimestamp'] < HeadCoilDatesSite1[2]):
            ID_HeadCoil[subject]=3
        elif BB['subj'][subject]['AcqTimestamp'] >= HeadCoilDatesSite1[2]:
            ID_HeadCoil[subject]=4
        else:
            ID_HeadCoil[subject]=0 # Should never happen
    elif BB['subj'][subject]['site'] == 1:
        if BB['subj'][subject]['AcqTimestamp'] < HeadCoilDatesSite2[0]:
            ID_HeadCoil[subject]=5
        elif BB['subj'][subject]['AcqTimestamp'] >= HeadCoilDatesSite2[0]:
            ID_HeadCoil[subject]=6
        else:
            ID_HeadCoil[subject]=0 # Should never happen
    else:
        ID_HeadCoil[subject]=0

with open(outputDir + 'ID_SCANHEADCOIL.txt', 'w') as f:
    for subject in ID_HeadCoil.keys():
        f.write(subject + ' '+ str(ID_HeadCoil[subject]) + '\n')
f.close()



#SP Date for Site 1
SPDate=datetime.datetime.strptime("20170131",'%Y%m%d')

ID_SP=collections.OrderedDict()
for subject in sorted(BB['list_subjects']):
    ID_SP[subject]=[]
    if BB['subj'][subject]['site'] == 0:
        if BB['subj'][subject]['AcqTimestamp'] < SPDate:
            ID_SP[subject].append(-1)
        else:
            ID_SP[subject].append(1)
    else:
        ID_SP[subject].append(1)


with open(outputDir + 'ID_SERVICEPACK.txt', 'w') as f:
    for subject in ID_SP.keys():
        f.write(subject + ' ' + ' '.join([str(x) for x in ID_SP[subject]]) + '\n')
f.close()


#Generating Protocol file
ID_Protocol=collections.OrderedDict()
for subject in sorted(BB['list_subjects']):
    ID_Protocol[subject]=0
    if 'StudyDescription' in BB['subj'][subject].keys():
        ID_Protocol[subject] = BB['subj'][subject]['StudyDescription']

with open(outputDir + 'ID_PROTOCOL.txt', 'w') as f:
    for subject in ID_Protocol.keys():
        f.write(subject + ' ' + str(ID_Protocol[subject]) + '\n')
f.close()


#Generating CMRR file
ID_CMRR=collections.OrderedDict()
for subject in sorted(BB['list_subjects']):
    ID_CMRR[subject]=0
    if 'CMRR' in BB['subj'][subject].keys():
        ID_CMRR[subject] = BB['subj'][subject]['CMRR']

with open(outputDir + 'ID_CMRR.txt', 'w') as f:
    for subject in ID_CMRR.keys():
        f.write(subject + ' ' + str(ID_CMRR[subject]) + '\n')
f.close()


#Generating REPT1 file
ID_REPT1=collections.OrderedDict()
for subject in sorted(BB['list_subjects']):
    ID_REPT1[subject]=0
    if 'StudyID' in BB['subj'][subject].keys():
        ID_REPT1[subject] = BB['subj'][subject]['StudyID']
    else:
        ID_REPT1[subject] = "NaN"

with open(outputDir + 'ID_REPT1.txt', 'w') as f:
    for subject in ID_REPT1.keys():
        f.write(subject + ' ' + str(ID_REPT1[subject]) + '\n')
f.close()


print('After saving: ' + str(datetime.datetime.now()))

