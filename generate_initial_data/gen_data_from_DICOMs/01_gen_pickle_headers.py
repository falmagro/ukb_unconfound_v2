#!/bin/env python

import os
import sys
import shutil
import pickle
import pydicom
import os.path
import argparse
import numpy as np

sDir=os.environ['subjDir']   
confDataDir=os.environ['CONFDATA']
tags={}       

final={}

textFile = open(confDataDir+'/subj.txt','r')
subjects = [x.replace('\n','',) for x in textFile.readlines()]
     
for subject in subjects:
    
    print(subject)
    final[subject]={}

    for modality in ["T1", "T2", "dMRI", "rfMRI", "tfMRI", "SWI"]:

        dcmFile=sDir + '/' + subject + '/DICOM/' + modality + '.dcm'

        if os.path.isfile(dcmFile):
            final[subject][modality]={}
            final[subject][modality]['tags']=[]
            dic=pydicom.dcmread(dcmFile, defer_size='10KB')

            for k in dic.keys():
                # This code has been adapted from dcmpandas library
                # Skip images
                if (k.group,k.elem) == (0x7fe0,0x0010):
                    continue
                v = dic[k]
                key = (v.name
                       .replace(' ','')
                       .replace('\'','')
                       .replace('/','')
                       .replace('[','')
                       .replace(']','')
                       .replace('(','')
                       .replace(')','')
                       )
                if type(v.value) == bytes:
                    value = v.value.decode('latin1')
                else:
                    value = str(v.value)

                if (k.group,k.elem) in [(0x20,0x1041), # Slice location
                                        (0x18,0x50)    # Slice thickness
                                        ]:
                    value = float(value)
                elif (k.group,k.elem) in [(0x28,0x30), # Pixel spacing
                                        ]:
                    value = [float(f) for f in v.value]

                elif not (('\\' in value) or ('[' in value)):
                    if v.VR in ['IS','SL','US']:
                        value = int(value)

                if key in final[subject][modality].keys():
                    if isinstance(final[subject][modality][key],list):
                        final[subject][modality][key].append(value)
                    else:
                        newList=[final[subject][modality][key],value]
                        final[subject][modality][key]=newList
                else:
                    final[subject][modality][key]=value

                if key not in tags.keys():
                    tags[key]=(k.group,k.element,'(%04x_%04x)'%(k.group,
                                                        k.elem), v.VR)

with open('all_header.pickle', 'wb') as handle:
    pickle.dump(final, handle) 

#for key in tags.keys():
#    print(key + ': ' + str(tags[key]))

