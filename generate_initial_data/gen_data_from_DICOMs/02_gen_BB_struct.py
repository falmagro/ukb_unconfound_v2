#!/bin/env python

import os
import sys
import string
import shutil
import pickle
import logging
import pydicom
import os.path
import argparse
import datetime
import collections
import numpy as np

def is_number(s):
    try:
        float(s)
        return True
    except ValueError:
        return False

startingTime = str(datetime.datetime.now())
startingTime = startingTime.replace(' ','_').replace(':','-').split('.')[0]

print('Logs are being created in ' + startingTime + '.log')
logging.basicConfig(format='%(asctime)s; %(levelname)s; %(message)s',
                    filename=startingTime + '.log', level=logging.DEBUG)


BB={}
BBDIR=os.environ['BB_BIN_DIR']
textFile = open(os.environ['CONFDATA'] + '/subj.txt','r')
subjects = [x.replace('\n','',) for x in textFile.readlines()]

BB['list_subjects']=subjects

BB['subj']={}

for subject in subjects:
    BB['subj'][subject]={}

BB['list_subjects_by_batch']={}

logging.info('Generating Batch info per subject. ')

batches=['1', '2', '3', '4','5','6']
BB['list_batches']=batches
for batch in batches:
    textFile = open('./s' + str(batch) + '.txt','r')
    subjects = [x.replace('\n','',) for x in textFile.readlines()]
    BB['list_subjects_by_batch'][batch] = subjects

    for subject in subjects:
        if subject in list(BB['subj']):
            BB['subj'][subject]['batch']=batch
        else:
            logging.warning('Subject ' + subject + ' is listed in the batches but was not found') 

logging.info('Generating DICOM hdr info per subject. ')

fi='./all_header.pickle'
logging.info('Loading: ' + fi)
with open(fi, 'rb') as f:
    p=pickle.load(f)
    for subject in p.keys():
        BB['subj'][subject]['dcm']=p[subject]


logging.info('Generating DICOM header fields list. ')
BB['list_dcm_fields']=[]
for subject in BB['list_subjects']:
    for mod in BB['subj'][subject]['dcm'].keys():
        for ke in BB['subj'][subject]['dcm'][mod].keys():
            if ke not in BB['list_dcm_fields']:
                BB['list_dcm_fields'].append(ke)

logging.info('Generating DICOM modalities list: ' )
BB['list_modalities']=['T1', 'T2', 'SWI', 'dMRI', 'rfMRI', 'tfMRI']
for subject in  BB['list_subjects']:
    BB['subj'][subject]['Modalities']=[]
    for mo in ['T1', 'T2', 'SWI', 'dMRI', 'rfMRI', 'tfMRI']:
        if mo in BB['subj'][subject]['dcm'].keys():
            BB['subj'][subject]['Modalities'].append(mo)


logging.info('Generating Acquisition timestamps per subject. ')
for subject in BB['list_subjects']:
    ad=BB['subj'][subject]['dcm']['T1']['AcquisitionDate']
    at=BB['subj'][subject]['dcm']['T1']['AcquisitionTime']
    BB['subj'][subject]['AcqTimestamp'] = datetime.datetime.strptime(ad + ' ' +
                                             at + ' UTC','%Y%m%d %H%M%S.%f %Z')

logging.info('Generating an ordered list of timestamps and a dictionary. ')
orderedTimestamps=collections.OrderedDict()
timestampsDict = {}
timestamps = [] 

for subject in BB['list_subjects']:
    timestampsDict[BB['subj'][subject]['AcqTimestamp']] = subject       

for key in sorted(timestampsDict.keys()):
    orderedTimestamps[key]=timestampsDict[key]
    timestamps.append(key)

BB['timestampsDict']=orderedTimestamps
BB['timestamps']=timestamps


logging.info('Generating Site info per subject. ')

# Easy to extend when new sites are added
with open(BBDIR + '/bb_data/sites_r.txt', 'r') as f:
    BB['list_sites'] =  p=[x.strip() for x in f.readlines()]

BB['list_subjects_by_site']=[]
for i in range(len(BB['list_sites'])):
    BB['list_subjects_by_site'].append([])

for subject in BB['list_subjects']:
    ia=BB['subj'][subject]['dcm']['T1']['InstitutionAddress']
    try:
        ind = BB['list_sites'].index(ia)
    except:
        logging.warning('Subject ' + subject + ' had no Site information')
        ind=-1
    BB['subj'][subject]['site']=ind
    if ind>-1:
        BB['list_subjects_by_site'][ind].append(subject)
        

logging.info('Checking inconsistencies in sites info. ')   
for subject in BB['list_subjects']:
    val=BB['subj'][subject]['dcm']['T1']['InstitutionAddress']
    # Print any inconsistencies in Institution Address across modalities
    for ke in BB['subj'][subject]['dcm'].keys():
        if not val == BB['subj'][subject]['dcm'][ke]['InstitutionAddress']:
            logging.warning('Subject ' + su + ' has inconsistencies in ' + 
                            'the site info')


#CMRR
logging.info('Generating CMRR dict info per subject. ')
for subject in BB['list_subjects']:
    # For each modality for the subject
    for mod in BB['subj'][subject]['dcm'].keys():
        if 'CSASeriesHeaderInfo' in BB['subj'][subject]['dcm'][mod].keys():
            CSA=BB['subj'][subject]['dcm'][mod]['CSASeriesHeaderInfo']
            CSA2=CSA.replace('\x00','').replace('\t',' ').split('\n')
            ind=-1
            for st in CSA2:
                if '### ASCCONV END ###' in st:
                    ind=CSA2.index(st)
            if ind >-1:
                CSA3=CSA2[ind+1:]
                CSA4=[]
                for st in CSA3:
                    CSA4.append(''.join([b for b in st if b in string.printable]))
                CSA5=""
                for st in CSA4:
                    if "by eja" in st:
                        CSA5=st
                if not CSA5 == "":
                    CSA6=CSA5.split('||')
                    dictCMRR={}
                    for st in CSA6:
                        if "by eja" in st and ':' in st:
                            tr=st.replace(' by eja','').split(':')
                            dictCMRR['CMRR_' + tr[0]]=tr[1].strip().replace('/', 
                                    '-').replace(' ', '-').replace(';','')
                    if len(dictCMRR.keys()) >0:
                        if 'CMRR_dict' not in BB['subj'][subject].keys():
                            BB['subj'][subject]['CMRR_dict']={}
                        BB['subj'][subject]['CMRR_dict'][mod]=dictCMRR


  

logging.info('Correcting CMRR inconsistencies. ')
list_no_CMRR=[]

#Get a list of subjects without CMRR data
for sub in BB['list_subjects']:
    if 'CMRR_dict' not in BB['subj'][sub]:
        list_no_CMRR.append(sub)
    else:
        # For each different modality
        for ke in BB['subj'][sub]['CMRR_dict'].keys():
            if 'CMRR_ICE_Linux' in BB['subj'][sub]['CMRR_dict'][ke].keys():
                break;
            list_no_CMRR.append(sub)
list_no_CMRR=list(set(list_no_CMRR))

# For each subject without CMRR data, check the   
# CMRR code of the subjects before and after
cont=0
for l in list_no_CMRR:
    ind=BB['timestamps'].index(BB['subj'][l]['AcqTimestamp'])
    
    nextInd = ind + 1
    nextSubjID=BB['timestampsDict'][BB['timestamps'][nextInd]]
    nextSubj=BB['subj'][nextSubjID]

    while not(nextSubj['site'] == BB['subj'][l]['site']) or ('CMRR_dict' not in nextSubj):
        nextInd = nextInd + 1
        nextSubjID=BB['timestampsDict'][BB['timestamps'][nextInd]]
        nextSubj=BB['subj'][nextSubjID]
        logging.warning('Subject ' +  nextSubjID + ' cannot follow ' + l)

    prevInd = ind - 1
    prevSubjID=BB['timestampsDict'][BB['timestamps'][prevInd]]
    prevSubj=BB['subj'][prevSubjID]

    while not(prevSubj['site'] == BB['subj'][l]['site']) or 'CMRR_dict' not in prevSubj:
        prevInd = prevInd - 1
        prevSubjID=BB['timestampsDict'][BB['timestamps'][prevInd]]
        prevSubj=BB['subj'][prevSubjID]
        logging.warning('Subject ' +  prevSubjID + ' cannot precede ' + l)


    if ('CMRR_dict' in nextSubj.keys()) and ('CMRR_dict' in prevSubj.keys()):
        # Get the modality with CMRR code for the next and previous subjects
        for nextMod in nextSubj['CMRR_dict'].keys():
            if 'CMRR_ICE_Linux' in nextSubj['CMRR_dict'][nextMod].keys():
                break
        for prevMod in prevSubj['CMRR_dict'].keys():
            if 'CMRR_ICE_Linux' in prevSubj['CMRR_dict'][prevMod].keys():
                break
        if (('CMRR_ICE_Linux' in nextSubj['CMRR_dict'][nextMod].keys()) and
           ('CMRR_ICE_Linux' in prevSubj['CMRR_dict'][prevMod].keys())):
            BB['subj'][l]['CMRR_dict']={}
            
            # If CMRR code of the previous and following subjects are
            # the same, the CMRR code of this subject will be the same
            if nextSubj['CMRR_dict'][nextMod]['CMRR_ICE_Linux'] == prevSubj['CMRR_dict'][prevMod]['CMRR_ICE_Linux']:
                BB['subj'][l]['CMRR_dict']['T1']={}
                BB['subj'][l]['CMRR_dict']['T1']['CMRR_ICE_Linux'] = nextSubj['CMRR_dict'][nextMod]['CMRR_ICE_Linux']
                cont=cont+1
            # If they differ, take the CMRR code of the next subject for this subject.
            else:
                BB['subj'][l]['CMRR_dict']['T1']={}
                BB['subj'][l]['CMRR_dict']['T1']['CMRR_ICE_Linux'] = nextSubj['CMRR_dict'][nextMod]['CMRR_ICE_Linux']
                cont=cont+1
        else:
            logging.error(l+ ' ' + str(ind) + ' PREV and NEXT CMRR do not have CMRR_ICE_Linux string')

    else:
        if not ('CMRR_dict' in nextSubj.keys()):
            logging.error(l + ' ' + str(ind)+ ' PREV subject does not have CMRR_dict')  
        elif not ('CMRR_dict' in prevSubj.keys()):
            logging.error(l + ' ' + str(ind) + ' NEXT subject does not have CMRR_dict') 
        else:
            logging.error(l + ' ' + str(ind)+ ' Unknown error')  
logging.info('   ' + str(cont) + ' CMRR inconsistencies corrected') 

logging.info('Generating CMRR info per subject. ')
for sub in BB['list_subjects']:
    if (('T1' in BB['subj'][sub]['CMRR_dict'].keys()) and 
        ('CMRR_ICE_Linux' in BB['subj'][sub]['CMRR_dict']['T1'].keys())):
        BB['subj'][sub]['CMRR']=BB['subj'][sub]['CMRR_dict']['T1']['CMRR_ICE_Linux']
    else:
        boo=False
        for mod in  BB['subj'][sub]['CMRR_dict'].keys():
            if 'CMRR_ICE_Linux' in BB['subj'][sub]['CMRR_dict'][mod].keys():
                BB['subj'][sub]['CMRR']=BB['subj'][sub]['CMRR_dict'][mod]['CMRR_ICE_Linux']
                boo=True
                break
        if not boo:
            logging.error('There was a problem generating CMRR data for' + sub)         

logging.info('Generating CMRR values list. ')
lis = []
for sub in BB['list_subjects']:
    lis.append(BB['subj'][sub]['CMRR'])
lis = list(set(lis))
BB['list_CMRR'] = lis

#Order the values chronologically
ti = []
for elem in BB['list_CMRR']:
    d=elem.replace('--','-').split('-')
    ti.append(datetime.datetime.strptime(d[6]+'-'+d[4]+'-'+d[5],'%Y-%b-%d'))

BB['list_CMRR'] = list(zip(*sorted(zip(ti,BB['list_CMRR']))))[1]

#Rewrite the CMRR field to be an index:
for subject in BB['list_subjects']:
    BB['subj'][subject]['CMRR']=BB['list_CMRR'].index(BB['subj'][subject]['CMRR']) 


# Reference Amplitude
logging.info('Generating CSA dict info per subject. ')
for subject in BB['list_subjects']:
    for mod in BB['subj'][subject]['dcm'].keys():
        if 'CSASeriesHeaderInfo' in BB['subj'][subject]['dcm'][mod].keys():
            dic={}
            CSA=BB['subj'][subject]['dcm'][mod]['CSASeriesHeaderInfo']
            CSA2=CSA.replace('\x00','').replace('\t',' ').split('\n')
            p=[x.replace('"','') for x in CSA2 if len(x.split(' = ')) == 2]
            for pair in p:
                [key,value]=pair.split(' = ')
                value=value.strip()
                if is_number(value):
                    value = float(value)
                dic[key.strip()]=value
            BB['subj'][subject]['dcm'][mod]['CSASeriesHeaderInfo']=dic


for subject in BB['list_subjects']:
    if 'CSASeriesHeaderInfo' in BB['subj'][subject]['dcm']['T1'].keys():
        RA=BB['subj'][subject]['dcm']['T1']['CSASeriesHeaderInfo']['sTXSPEC.asNucleusInfo[0].flReferenceAmplitude']
        if type(RA) == type(''):
            RA = RA.strip()
            if is_number(RA):
                RA=float(RA)
            else:
                logging.error('Problem with RefAmp in subject ' + subject + 
                          '. DICOM header does not follow the standard format') 

        BB['subj'][subject]['ReferenceAmplitude'] = RA

# Table Position
logging.info('Generating Table Position info per subject. ')
for subject in BB['list_subjects']:
    TP=np.nan
    if 'ImaAbsTablePosition' in BB['subj'][subject]['dcm']['T1'].keys():
        TP=eval(BB['subj'][subject]['dcm']['T1']['ImaAbsTablePosition'])
        TP=TP[2]
    else:
        keys=BB['subj'][subject]['dcm'].keys()
        if len(keys) > 0:
           key=next(iter(keys))
           if 'ImaAbsTablePosition' in BB['subj'][subject]['dcm'][key].keys():
              TP=eval(BB['subj'][subject]['dcm'][key]['ImaAbsTablePosition'])
              TP=TP[2]
    if type(TP) == type(''):
        if is_number(TP):
            TP=float(TP)
        
    BB['subj'][subject]['TablePosition'] = TP



#Protocol
DICOM_fields = ["StudyDescription", "StudyID"]

logging.info('Correcting inconsistencies in the DCM information for PROTOCOL.' )
#Things to correct: there is ONE subject without these fields in the DCM
li=[]
for dm in DICOM_fields:
    for subject in BB['list_subjects']:
        for mod in BB['subj'][subject]['dcm'].keys():
            if dm not in BB['subj'][subject]['dcm'][mod].keys():
                li.append(subject)
                logging.warning('Subject ' + subject + ' does not have ' + dm)
li=list(set(li))

# Correction: If the fields are the same in the previous and next
# subjects, use those values for this subject
for l in li:
    ind=BB['timestamps'].index(BB['subj'][l]['AcqTimestamp'])
    prevSubjID=BB['timestampsDict'][BB['timestamps'][ind-1]]
    prevSubj=BB['subj'][prevSubjID]
    nextSubjID=BB['timestampsDict'][BB['timestamps'][ind+1]]
    nextSubj=BB['subj'][nextSubjID]
    for dm in DICOM_fields:
        if prevSubj['dcm']['T1'][dm] == nextSubj['dcm']['T1'][dm]:
            for mod in BB['subj'][l]['dcm'].keys():
                BB['subj'][l]['dcm'][mod][dm] = prevSubj['dcm']['T1'][dm]
        else:
            logging.error('Next and Prev subjects to ' + subject + 
                         ' do not have consistent StudyID that can be imputed')

# Find the subject with discrepancies in their DICOM fields
li=[]
for dm in DICOM_fields:
    for subject in BB['list_subjects']:
        p=[BB['subj'][subject]['dcm'][mod][dm] for mod in 
                                            BB['subj'][subject]['dcm'].keys()]
        if not all(x == p[0] for x in p):
            li.append(subject)
li=list(set(li))

# The subject with inconsistent values only has inconsistencies in T1.
for l in li:
    for dm in DICOM_fields:
        BB['subj'][l]['dcm']['T1'][dm]=BB['subj'][l]['dcm']['rfMRI'][dm]


logging.info('Generating Study info per subject. ' )
for sub in BB['list_subjects']:
    for dm in DICOM_fields:
        BB['subj'][sub][dm]=BB['subj'][sub]['dcm']['T1'][dm]     

#List of protocols found so far:
validStudies = ['head^UK Biobank_v3.0', 'head^UK Biobank_v4.0', 
                'head^UK Biobank_v5.0', 'head^UK Biobank_v6.0',
                'UKBiobank_Head^head_v6.0', 'head^UK', 
                'UK_Biobank^Head', 'head^UK Biobank']


# Correcting inconsistencies in the Study Description'
studyRules={  'Head^UK Biobank_V.3.0'        : 'head^UK Biobank_v3.0',
              'head^UK Biobank_V3.0'         : 'head^UK Biobank_v3.0',
              'head^UK Biobank_v4.0_0001'    : 'head^UK Biobank_v4.0',
              'Head^UK Biobank_v5.0'         : 'head^UK Biobank_v5.0',
              'head UK Biobank_v6.0'         : 'head^UK Biobank_v6.0',
              'head UK Biobank_v6.1'         : 'head^UK Biobank_v6.0',
              'head^Brain Dot Engine'        : 'head^UK Biobank_v6.0',
              'SequenceRegion^UserSequences' : 'head^UK Biobank_v6.0',             
              'UKBiobank^head_v6.0'          : 'UKBiobank_Head^head_v6.0',
              'UKBiobank_Head head_v6.4'     : 'UKBiobank_Head^head_v6.0'}

for sub in BB['list_subjects']:
    if BB['subj'][sub]['StudyDescription'] in studyRules.keys():
        BB['subj'][sub]['StudyDescription'] = studyRules[BB['subj'][sub]['StudyDescription']]

for sub in BB['list_subjects']:
    if BB['subj'][sub]['StudyDescription'] not in validStudies:
        logging.error('Subject ' + sub + ' has an unknown (new?) StudyDescription:' + str(BB['subj'][sub]['StudyDescription']) )


lis=[]
for sub in BB['list_subjects']:
    lis.append(BB['subj'][sub]['StudyDescription'])


logging.info('Generating Study values list: ')
lis = []
for sub in BB['list_subjects']:
    lis.append(BB['subj'][sub]['StudyDescription'])
lis = sorted(list(set(lis)))
BB['list_studies'] = lis

#Storing the study description as an index of the list of studies
for subject in BB['list_subjects']:
    BB['subj'][subject]['StudyDescription']=BB['list_studies'].index(BB['subj'][subject]['StudyDescription']) 


#Saving whole dataset
#logging.info('Saving whole dataset: ' )
#with open('../dicom_headers/BB_full.pickle', 'wb') as handle:
#    pickle.dump(BB, handle)

#Delete dicom headers data
for subject in BB['list_subjects']:
    del BB['subj'][subject]['dcm']


#Saving without DICOM headers
logging.info('Saving reduced dataset: ' )
with open('BB.pickle', 'wb') as handle:
    pickle.dump(BB, handle)

logging.info('Processing finished: ' )

