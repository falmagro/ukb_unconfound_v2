function func_01_05_gen_nonlin_conf(num_IDP)
%
%   Authors: Fidel Alfaro-Almagro, Stephen M. Smith, Tom Nichols, & Paul McCarthy
%
%   Copyright 2017 University of Oxford
%
%   Licensed under the Apache License, Version 2.0 (the "License");
%   you may not use this file except in compliance with the License.
%   You may obtain a copy of the License at
%
%    http://www.apache.org/licenses/LICENSE-2.0
%
%   Unless required by applicable law or agreed to in writing, software
%   distributed under the License is distributed on an "AS IS" BASIS,
%   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%   See the License for the specific language governing permissions and
%   limitations under the License.
%
    addpath(genpath('common_matlab/'));
    
    load('workspaces/ws_01/nonlin_confounds_generated.mat');
    load('workspaces/ws_01/IDPs_i_nonlin_deconf.mat');
    
    num_conf_nonlin=length(ind_nonlin);
    
    veu = zeros(num_conf_nonlin,1);

    j={};
    
    % Get for each site, the indices of the subject for that 
    % site that do not have NaN value for the IDP num_IDP
    for i=1:length(indSite)
        j{i} = intersect(find(~isnan(IDP_i_deconf(:,num_IDP))),indSite{i});
    end

    for i = 1:num_conf_nonlin
        % Snippet to figure out the Site of the non-linear confound 
        numSite = -1;
        for numSites = 1:length(indSite)
            if ~isempty(strfind(names_nonlin{i}, ...
                                strcat('Site_', num2str(numSites))))
                numSite = numSites;
            end
        end
            
        confstmp_all = nets_demean(conf(j{numSite},ind_nonlin(i)));
        BETAs_all    = (pinv(confstmp_all) * IDP_i_deconf(j{numSite},num_IDP));
        tmp_all      = confstmp_all * BETAs_all;
        veu(i)       = 100 * ((nanstd(tmp_all) ./ ...
                               nanstd(IDP_i_deconf(j{numSite},num_IDP))).^2);
    end
    
    fNam = strcat('workspaces/ws_01/veu_nonlin/ws_',num2str(num_IDP)); 
    save(fNam, '-v7.3', 'veu');
end
