function func_01_12_gen_ct_conf(num_IDP)
%
%   Authors: Fidel Alfaro-Almagro, Stephen M. Smith, Tom Nichols, & Paul McCarthy
%
%   Copyright 2017 University of Oxford
%
%   Licensed under the Apache License, Version 2.0 (the "License");
%   you may not use this file except in compliance with the License.
%   You may obtain a copy of the License at
%
%    http://www.apache.org/licenses/LICENSE-2.0
%
%   Unless required by applicable law or agreed to in writing, software
%   distributed under the License is distributed on an "AS IS" BASIS,
%   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%   See the License for the specific language governing permissions and
%   limitations under the License.
%
    addpath(genpath('common_matlab/'));

    load('workspaces/ws_01/ct_confounds_generated.mat');
    load('workspaces/ws_01/IDPs_i_ct_deconf.mat');
    
    cont=1;
    veu = zeros(num_ct,1);

    j={};
    
    % Get for each site, the indices of the subject for that 
    % site that do not have NaN value for the IDP num_IDP
    for i=1:length(indSite)
        j{i} = intersect(find(~isnan(IDP_i_deconf(:,num_IDP))),indSite{i});
    end

    for i=1:num_div
        qName = strcat('workspaces/ws_01/ct_q', num2str(i),'.mat');
        load(qName)
        
        crossed_terms=tmp;
        
        for k = 1:size(crossed_terms,2)

            numSite = -1;
            for numSites = 1:length(indSite)
                if ~isempty(strfind(names_ct{cont}, ...
                                    strcat('Site_', num2str(numSites))))
                    numSite = numSites;
                end
            end
                
            confstmp = nets_demean(crossed_terms(j{numSite},k));
            BETAs    = (pinv(confstmp) * IDP_i_deconf(j{numSite},num_IDP));
            tmp      = confstmp * BETAs;
            veu(cont)   = 100 * ((nanstd(tmp) ./ nanstd(IDP_i_deconf(j{numSite},...
                                                                 num_IDP))).^2);
            cont=cont+1;
        end

        clear crossed_terms;
        clear tmp;
    end
    
    fNam = strcat('workspaces/ws_01/veu_ct/ws_',num2str(num_IDP)); 
    save(fNam, '-v7.3', 'veu');
    datetime('now')
end




