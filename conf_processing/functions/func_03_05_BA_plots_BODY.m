function func_03_05_BA_plots_BODY(num_BODY_vars)
%
%   Authors: Fidel Alfaro-Almagro, Stephen M. Smith, Tom Nichols, & Paul McCarthy
%
%   Copyright 2017 University of Oxford
%
%   Licensed under the Apache License, Version 2.0 (the "License");
%   you may not use this file except in compliance with the License.
%   You may obtain a copy of the License at
%
%    http://www.apache.org/licenses/LICENSE-2.0
%
%   Unless required by applicable law or agreed to in writing, software
%   distributed under the License is distributed on an "AS IS" BASIS,
%   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%   See the License for the specific language governing permissions and
%   limitations under the License.
%
    addpath(genpath('common_matlab/'));
    
    load('workspaces/ws_03/ws_03_BODY_vars_i.mat');
    load('workspaces/ws_03/ws_03_conf.mat');

    BODY_vars_i_deconf = nets_unconfound2(BODY_vars_i(:,num_BODY_vars), conf);

    save(strcat('workspaces/ws_03/BODY_vars_i_deconf_05/ws_BODY_vars_i_deconf_',...
         num2str(num_BODY_vars)), '-v7.3', 'BODY_vars_i_deconf');
end
