function func_02_04_ve_gpu(num_IDP)
%
%   Authors: Fidel Alfaro-Almagro, Stephen M. Smith, Tom Nichols, & Paul McCarthy
%
%   Copyright 2017 University of Oxford
%
%   Licensed under the Apache License, Version 2.0 (the "License");
%   you may not use this file except in compliance with the License.
%   You may obtain a copy of the License at
%
%    http://www.apache.org/licenses/LICENSE-2.0
%
%   Unless required by applicable law or agreed to in writing, software
%   distributed under the License is distributed on an "AS IS" BASIS,
%   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%   See the License for the specific language governing permissions and
%   limitations under the License.
%
    addpath(genpath('common_matlab/'));

    make_dir('workspaces/ws_02/ve/')
    
    load('workspaces/ws_02/confounds_generated.mat');

    conf = gpuArray(conf);
    subset_IDPs_i = gpuArray(subset_IDPs_i);

    nCONFg=length(ind_conf_groups);
    
    % Rounding errors may produce negative UVEs.
    % This thresholding makes up for that error 
    % at the expense of windsoring really low VEs.
    veLowerThreshold = 1e-08;
    
    ve_i                     = {};
    veu_i                    = {};
    ve_RandGroups_i          = {};
    veu_RandGroups_i         = {};
    veRand_i                 = {};
    veuRand_i                = {};
    ve_i{nCONFg}             = gpuArray(zeros(nCONFg,1));
    veu_i{nCONFg}            = gpuArray(zeros(nCONFg,1));
    veRand_i{nCONFg}         = gpuArray(zeros(nCONFg,1));
    veuRand_i{nCONFg}        = gpuArray(zeros(nCONFg,1));
    ve_RandGroups_i{nCONFg}  = gpuArray(zeros(nCONFg,1));
    veu_RandGroups_i{nCONFg} = gpuArray(zeros(nCONFg,1)); 
    ve_Rand_SIMPLE{nCONFg}   = gpuArray(zeros(nCONFg,1));
    veu_Rand_SIMPLE{nCONFg}  = gpuArray(zeros(nCONFg,1));
    ve_all                   = gpuArray(zeros(nCONFg,1));

    j=~isnan(subset_IDPs_i(:,num_IDP));
                     
    for I = 1:nCONFg

        confstmp_all = nets_demean(conf(j,:));
        BETAs_all    = (pinv(confstmp_all)*subset_IDPs_i(j,num_IDP));
        tmp_all      = confstmp_all * BETAs_all;
        ve_all(I)    = 100 * ((nanstd(tmp_all) ./ ...
                               nanstd(subset_IDPs_i(j,num_IDP))).^2);
                          
        for k = 1:num_groups(I)
            if (I ~= 3) || (mod(k,10) ==  0)
                my_log('', ['Loop: ' num2str(I) ' / ' num2str(k)]);
            end
            
            indK       = ind_conf_groups{I}{k};
            conftmp    = nets_demean(conf(j,indK));
            BETAs      = pinv(conftmp) * subset_IDPs_i(j,num_IDP);
            tmp        = conftmp * BETAs;
            ve_i{I}(k) = 100 * ((nanstd(tmp) ./ ...
                                  nanstd(subset_IDPs_i(j,num_IDP))).^2);

            indExcluK     = [ind_conf_groups{I}{1:k-1} ...
                             ind_conf_groups{I}{k+1:num_groups(I)}];
            conftmp_exclu = nets_demean(conf(j,indExcluK));
            BETAs_exclu   = pinv(conftmp_exclu) * subset_IDPs_i(j,num_IDP);
            tmp_exclu     = conftmp_exclu*BETAs_exclu;
            ve_exclu      = 100 * ((nanstd(tmp_exclu) ./ ...
                                    nanstd(subset_IDPs_i(j,num_IDP))).^2);  

            veu_i{I}(k) = ve_all(I) - ve_exclu;

            if ve_i{I}(k) < veLowerThreshold
                ve_i{I}(k) = veLowerThreshold;
            end
            if veu_i{I}(k) < veLowerThreshold
                veu_i{I}(k) = veLowerThreshold;
            end
            
                        
            conftmp    = nets_demean(conf_Group_Rand{end}(j,indK));
            BETAs      = pinv(conftmp) * subset_IDPs_i(j,num_IDP);
            tmp        = conftmp * BETAs;
            ve_RandGroups_i{I}(k) = 100 * ((nanstd(tmp) ./ ...
                                  nanstd(subset_IDPs_i(j,num_IDP))).^2);
            
            conftmp_exclu = nets_demean([conf(j,:) ...
                                         conf_Group_Rand{end}(j,indK)]);
            BETAs_exclu   = pinv(conftmp_exclu) * subset_IDPs_i(j,num_IDP);
            tmp_exclu     = conftmp_exclu*BETAs_exclu;
            ve_exclu      = 100 * ((nanstd(tmp_exclu) ./ ...
                                    nanstd(subset_IDPs_i(j,num_IDP))).^2);           
            
            veu_RandGroups_i{I}(k) = ve_exclu - (ve_all(I));
            
            if ve_RandGroups_i{I}(k) < veLowerThreshold
                ve_RandGroups_i{I}(k) = veLowerThreshold;
            end            
            if veu_RandGroups_i{I}(k) < veLowerThreshold
                veu_RandGroups_i{I}(k) = veLowerThreshold;
            end
        end

        for k = 1:num_groups_Rand
            conftmp        = nets_demean(conf_Group_Rand{k}(j,:));
            BETAs          = pinv(conftmp) * subset_IDPs_i(j,num_IDP);
            tmp            = conftmp * BETAs;
            veRand_i{I}(k) = 100 * ((nanstd(tmp) ./ ...
                                   nanstd(subset_IDPs_i(j,num_IDP))).^2);

            ve_excluRand  = ve_all(I);
                                
            confstmp_all = nets_demean([conf(j,:) conf_Group_Rand{k}(j,:)]);
            BETAs_all    = (pinv(confstmp_all)*subset_IDPs_i(j,num_IDP));
            tmp_all      = confstmp_all * BETAs_all;

            ve_allRand   = 100 * ((nanstd(tmp_all) ./ ...
                               nanstd(subset_IDPs_i(j,num_IDP))).^2);
                                
            veuRand_i{I}(k) = ve_allRand - ve_excluRand;

            if veRand_i{I}(k) < veLowerThreshold
                veRand_i{I}(k) = veLowerThreshold;
            end
            if veuRand_i{I}(k) < veLowerThreshold
                veuRand_i{I}(k) = veLowerThreshold;
            end
        end

        conftmp = nets_demean(confRSIM(j,:));
        BETAs   = pinv(conftmp) * subset_IDPs_i(j,num_IDP);
        tmp     = conftmp * BETAs;
        ve_Rand_SIMPLE{I} = 100 * ((nanstd(tmp) ./ nanstd(subset_IDPs_i(j,num_IDP))).^2);

        conftmp_exclu = nets_demean([conf(j,:) confRSIM(j,:)]);
        BETAs_exclu   = pinv(conftmp_exclu) * subset_IDPs_i(j,num_IDP);
        tmp_exclu     = conftmp_exclu*BETAs_exclu;
        ve_exclu      = 100 * ((nanstd(tmp_exclu) ./ ...
                                nanstd(subset_IDPs_i(j,num_IDP))).^2);           

        veu_Rand_SIMPLE{I} = ve_exclu - (ve_all(I));
        
        if veu_Rand_SIMPLE{I} < veLowerThreshold
            veu_Rand_SIMPLE{I} = veLowerThreshold;
        end
            
        
        conftmp = nets_demean(conf_Simple(j,:));
        BETAs   = pinv(conftmp) * subset_IDPs_i(j,num_IDP);
        tmp     = conftmp * BETAs;
        ve_Simple{I} = 100 * ((nanstd(tmp) ./ nanstd(subset_IDPs_i(j,num_IDP))).^2);

        conftmp = nets_demean(conf_PCA(j,:));
        BETAs   = pinv(conftmp) * subset_IDPs_i(j,num_IDP);
        tmp     = conftmp * BETAs;
        ve_PCA{I}  = 100 * ((nanstd(tmp) ./ nanstd(subset_IDPs_i(j,num_IDP))).^2);
        
        
        if ve_Simple{I} < veLowerThreshold
            ve_Simple{I} = veLowerThreshold;
        end
        if ve_PCA{I} < veLowerThreshold
            ve_PCA{I} = veLowerThreshold;
        end
        if ve_all(I) < veLowerThreshold
            ve_all(I) = veLowerThreshold;
        end
        
    end
        
    finalValues.ve_i             = gather(ve_i);
    finalValues.veu_i            = gather(veu_i);
    finalValues.veRand_i         = gather(veRand_i);
    finalValues.veuRand_i        = gather(veuRand_i);
    finalValues.ve_all           = gather(ve_all);
    finalValues.ve_Simple        = gather(ve_Simple);
    finalValues.ve_PCA           = gather(ve_PCA);
    finalValues.ve_RandGroups_i  = gather(ve_RandGroups_i);
    finalValues.veu_RandGroups_i = gather(veu_RandGroups_i);
    finalValues.ve_Rand_SIMPLE   = gather(ve_Rand_SIMPLE);
    finalValues.veu_Rand_SIMPLE  = gather(veu_Rand_SIMPLE);
    
    fNam = strcat('workspaces/ws_02/ve/ws_',num2str(num_IDP)); 
    save(fNam, '-v7.3', 'finalValues');
end
