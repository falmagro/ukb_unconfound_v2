%
%   Authors: Fidel Alfaro-Almagro, Stephen M. Smith, Tom Nichols, & Paul McCarthy
%
%   Copyright 2017 University of Oxford
%
%   Licensed under the Apache License, Version 2.0 (the "License");
%   you may not use this file except in compliance with the License.
%   You may obtain a copy of the License at
%
%    http://www.apache.org/licenses/LICENSE-2.0
%
%   Unless required by applicable law or agreed to in writing, software
%   distributed under the License is distributed on an "AS IS" BASIS,
%   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%   See the License for the specific language governing permissions and
%   limitations under the License.
%
clear;
warning('off', 'all');
addpath('scripts');
addpath('functions');
addpath(genpath('common_matlab/'));

tic
toc
script_01_00_gen_init_vars;                                 toc
script_01_01_gen_raw_conf_gpu;                              toc
script_01_02_gen_nonlin_conf_gpu;                           toc
script_01_03_gen_jobs;                                      toc
run_in_cluster('scripts/script_01_04_gen_nonlin_conf.sh');  toc
script_01_06_gen_nonlin_conf;                               toc 
[s, c] = system('scripts/script_01_07_gen_nonlin_conf.sh'); toc
script_01_08_gen_nonlin_conf;                               toc
script_01_09_gen_ct_conf_gpu;                               toc
script_01_10_gen_jobs;                                      toc
run_in_cluster('scripts/script_01_11_gen_ct_conf.sh');      toc
script_01_13_gen_ct_conf;                                   toc
[s, c] = system('scripts/script_01_14_gen_ct_conf.sh');     toc
script_01_15_gen_ct_conf;                                   toc
script_01_16_gen_date_time_conf_gpu;                        toc
script_01_17_clean_workspace;                               toc
script_02_01_ve_gpu;                                        toc
script_02_02_gen_jobs;                                      toc
run_in_cluster('scripts/script_02_03_ve.sh');               toc
script_02_05_ve;                                            toc
script_02_06_ve;                                            toc
script_02_07_ve;                                            toc
script_03_01_BA_plots;                                      toc
script_03_02_generate_jobs;                                 toc
run_in_cluster('scripts/script_03_03_BA_plots.sh');         toc
script_03_06_BA_plots;                                      toc
script_03_07_generate_jobs;                                 toc
run_in_cluster('scripts/script_03_08_BA_plots.sh');         toc
script_04_01_general_unconfound;                            toc
script_04_03_generate_TC_figs;                              toc
script_04_04_generate_NAT_figs;                             toc
script_04_05_generate_SITE_figs;                            toc
[s, c] = system('scripts/script_05_01_gen_figures.sh');     toc

