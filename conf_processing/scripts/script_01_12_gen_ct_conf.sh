#!/bin/bash

origDir=`pwd`

/opt/fmrib/MATLAB/R2017a/bin/matlab -nojvm -nodisplay -nosplash -r "addpath('$origDir/functions/');func_01_12_gen_ct_conf($1);exit"

