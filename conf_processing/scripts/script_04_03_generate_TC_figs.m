%
%   Authors: Fidel Alfaro-Almagro, Stephen M. Smith, Tom Nichols, & Paul McCarthy
%
%   Copyright 2017 University of Oxford
%
%   Licensed under the Apache License, Version 2.0 (the "License");
%   you may not use this file except in compliance with the License.
%   You may obtain a copy of the License at
%
%    http://www.apache.org/licenses/LICENSE-2.0
%
%   Unless required by applicable law or agreed to in writing, software
%   distributed under the License is distributed on an "AS IS" BASIS,
%   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%   See the License for the specific language governing permissions and
%   limitations under the License.
%
clear;

addpath(genpath('common_matlab/'));
addpath('functions/');

my_log('', 'Before load');
load('workspaces/ws_01/final_workspace');
fm = (split(mfilename('fullpath'),'/')); 
fm = fm{end};
my_log(fm, 'After load');

% histogram of scan times
make_dir('figs/TC/')

% Getting the indices of the first temporal confound per site
indi =[];
for i=1:length(ind_site)
    stri = strcat('Site_', num2str(i), '__01');
    for j = ind_conf_groups{27}
        if strfind(conf_names{j}, stri)
            indi = [indi j];
        end
    end
end

darkred = [0.635 0.078 0.184];
% top PCA components
for i=length(ind_site):-1:1
    f = figure('visible','off','PaperOrientation', 'landscape','Units', ...
               'points','Position', [0,0,900,675]);
    xlabel('Time of day in [0, 1] range');
    set(gcf,'PaperPositionMode', 'auto', 'PaperOrientation', 'portrait');
    set(gca, 'FontSize',20);
    comp_name = strrep(conf_names{indi(i)},'__', '_');
    comp_name = strrep(comp_name,'_', ' - ');
    yyaxis left;
    histogram(scan_time(ind_site{i}),500,...
              'FaceColor', darkred,...
              'EdgeColor', darkred); 
    ylabel('Number of subjects at a certain time');
    yyaxis right;
    dscatter(scan_time(ind_site{i}),conf(ind_site{i},indi(i)));
    ylabel('First Temporal Component');
    ax = gca;
    title(strcat('Histogram of scanning time of day vs component ', ...
                  {' '}, comp_name));
    set(gca, 'LooseInset', get(gca, 'TightInset'));
    ax.YAxis(1).Color = darkred;
    ax.YAxis(2).Color = 'b';
    print('-dpng','-r70',['figs/TC/' conf_names{indi(i)} '.png']);

end

IDPs_unconf=nets_unconfound2(IDPs_i,conf(:,1:(indi(1)-1)));
IDPs_unconf=IDPs_unconf(ind_site{1},:);
t=my_nancorr(IDPs_unconf,scan_time(ind_site{1}));
ind_max=find(t==max(t));

f = figure('visible','off', 'PaperOrientation', 'landscape','Units', ...
           'points','Position', [0,0,900,675]);
hold on;
set(gcf,'PaperPositionMode', 'auto', 'PaperOrientation', 'portrait');
set(gca, 'FontSize',20);
corre = plot(t);  
r = plot(ind_max,t(ind_max), '*r');
legend(r, strcat('IDP: ', {' '}, IDP_names{ind_max}));
hold off
xlabel('IDPs');
ylabel('PCC');
title(strcat('PCC for correlations between the IDPs and', ...
              {' '}, comp_name));
set(gca, 'LooseInset', get(gca, 'TightInset'));
print('-dpng','-r70',['figs/TC/correlations.png']);

[~,i]=sort(scan_time(ind_site{1}));
IDPs_unconf = IDPs_unconf(i,ind_max); 
IDPs_unconf_s = smooth(IDPs_unconf,1000);
times =scan_time(ind_site{1}); 
times=times(i);


f = figure('visible','off','PaperOrientation', 'landscape','Units', ...
           'points','Position', [0,0,900,675]);
hold on;
set(gcf,'PaperPositionMode', 'auto', 'PaperOrientation', 'portrait');
set(gca, 'FontSize',20);
yyaxis left;
dscatter(times, IDPs_unconf_s); % smoothed IDP
ylabel('Smoothed IDP');
yyaxis right;
dscatter(times, IDPs_unconf_s); % smoothed IDP
ylabel('Smoothed IDP');
ax = gca;
xlabel('Time of day in [0, 1] range');
title(strcat('Smoothed IDP: ', {' '}, IDP_names{ind_max}));
set(gca, 'LooseInset', get(gca, 'TightInset'));
ax.YAxis(1).Color = 'k';
ax.YAxis(2).Color = 'k';
print('-dpng','-r70',['figs/TC/smoothed_IDP.png']);


f = figure('visible','off', 'PaperOrientation', 'landscape','Units', ...
           'points','Position', [0,0,900,675]);
hold on;
set(gcf,'PaperPositionMode', 'auto', 'PaperOrientation', 'portrait');
set(gca, 'FontSize',20);
yyaxis left;
dscatter(times, IDPs_unconf); % smoothed IDP
ylabel('Unsmoothed IDP');
yyaxis right;
dscatter(times, IDPs_unconf); % smoothed IDP
ylabel('Unsmoothed IDP');
ax = gca;
xlabel('Time of day in [0, 1] range');
title(strcat('Unsmoothed IDP: ', {' '}, IDP_names{ind_max}));
set(gca, 'LooseInset', get(gca, 'TightInset'));
ax.YAxis(1).Color = 'k';
ax.YAxis(2).Color = 'k';
print('-dpng','-r70',['figs/TC/unsmoothed_IDP.png']);

