#!/bin/env python
#
# Authors: Fidel Alfaro-Almagro, Stephen M. Smith, Tom Nichols, & Paul McCarthy
#
# Copyright 2017 University of Oxford
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

import plotly
import random
import matplotlib
import numpy as np
import plotly.plotly as py
import sys,argparse,os.path
import plotly.graph_objs as go
from scipy.interpolate import interpn

class MyParser(argparse.ArgumentParser):
    def error(self, message):
        sys.stderr.write('error: %s\n' % message)
        self.print_help()
        sys.exit(2)

class Usage(Exception):
    def __init__(self, msg):
        self.msg = msg

def main():

    parser = MyParser(description='Bland-Altman Plot generation tool')
    parser.add_argument("conf_group", help='Conf group to generate plot for')
    parser.add_argument("non_IDP_group", help='BODY or COGN')
    parser.add_argument("inputDir",  nargs='?', default='./', \
                                        help='Output Directory')
    parser.add_argument("outputDir", nargs='?', default='./', \
                                        help='Input Directory')

    argsa = parser.parse_args()

    conf_group    = argsa.conf_group
    non_IDP_group = argsa.non_IDP_group
    inputDir      = argsa.inputDir
    outputDir     = argsa.outputDir

    X =  np.loadtxt(inputDir + '/' + non_IDP_group + '_X_'  + conf_group + '.txt');
    Y =  np.loadtxt(inputDir + '/' + non_IDP_group + '_Y_'  + conf_group + '.txt');
    A =  np.loadtxt(inputDir + '/' + non_IDP_group + '_A_'  + conf_group + '.txt');
    B =  np.loadtxt(inputDir + '/' + non_IDP_group + '_B_'  + conf_group + '.txt');
    RA = np.loadtxt(inputDir + '/' + non_IDP_group + '_RA_' + conf_group + '.txt');
    RB = np.loadtxt(inputDir + '/' + non_IDP_group + '_RB_' + conf_group + '.txt');
    N =  np.loadtxt(inputDir + '/' + non_IDP_group + '_N_'  + conf_group + '.txt');

    f = open(inputDir + '/' + non_IDP_group + '_labels_'+ conf_group + '.txt', 'r')
    L = f.readlines()
    f.close()

    rangeX=(np.nanpercentile(X,2.5),np.nanpercentile(X,97.5))
    rangeY=(np.nanpercentile(Y,2.5),np.nanpercentile(Y,97.5))

    sizeX=rangeX[1] - rangeX[0]
    sizeY=rangeY[1] - rangeY[0]

    # Only show points outside outside a central ellipsoid with 95% of the data
    # Center: (0, 0) - X axis = rangeX - Y axis = rangeY
    # Ellipsoid equation: ((X^2) / (sizeX^2)) + ((Y^2) / (sizeY^2)) <= 1
    equation = [((X[i]**2) / sizeX) + ((Y[i]**2) / sizeY) for i in range(len(X))]
    newInd = [i for i in range(len(X)) if (equation[i] > 1)]

    newY =  Y[newInd]
    newX =  X[newInd]
    newA =  A[newInd]
    newB =  B[newInd]
    newRA = RA[newInd]
    newRB = RB[newInd]
    newN =  N[newInd]

    newL = [('-log P A: ' + str(A[i]) + '<BR>-log P B: ' + str(B[i])  + '<BR>' + \
          'Rho A: ' + str(RA[i]) + '<BR>Rho B: ' + str(RB[i])  + '<BR>' + \
          'N: ' + str(N[i]) + '<BR>' + L[i].replace('  -  ', '<BR>')) for i in newInd]

    plotly.offline.plot({
        "data": [go.Scattergl(x=newX, y=newY, mode='markers', text=newL, \
                          marker = dict(line = dict(width = 1),\
                                        color= 'blue',\
                                        size = 5))],
        "layout": go.Layout(title=go.layout.Title(\
                            text='Bland-Altman plot for ' + conf_group + '<BR>' + \
                                 '-log P values for correlation between ' + \
                                 non_IDP_group + ' vars and IDPs<BR>' + \
                                 'B = After removing all confounds from IDPs and ' +\
                                 non_IDP_group + ' vars<BR>' +\
                                 'A = After removing all confounds but ' + conf_group,\
                            y=0.97),
                            xaxis=go.layout.XAxis(
                                title=go.layout.xaxis.Title(
                                    text='(A + B) / 2',
                                    font=dict(
                                        size=18
                                    )
                                )
                            ),
                            yaxis=go.layout.YAxis(
                                title=go.layout.yaxis.Title(
                                    text='B - A',
                                    font=dict(
                                        size=18
                                    )
                                )
                            ),
                            hovermode = 'closest')},\
        auto_open=False,\
        filename = outputDir + '/BA_' + non_IDP_group + '_' + conf_group + '.html')

if __name__ == "__main__":
    main()
