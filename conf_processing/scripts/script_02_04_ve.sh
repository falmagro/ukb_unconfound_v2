#!/bin/bash

origDir=`pwd`

/opt/fmrib/MATLAB/R2017a/bin/matlab -nojvm -nodisplay -nosplash -r "addpath('$origDir/functions/');func_02_04_ve($1); exit"

