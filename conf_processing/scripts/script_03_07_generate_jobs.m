%
%   Authors: Fidel Alfaro-Almagro, Stephen M. Smith, Tom Nichols, & Paul McCarthy
%
%   Copyright 2017 University of Oxford
%
%   Licensed under the Apache License, Version 2.0 (the "License");
%   you may not use this file except in compliance with the License.
%   You may obtain a copy of the License at
%
%    http://www.apache.org/licenses/LICENSE-2.0
%
%   Unless required by applicable law or agreed to in writing, software
%   distributed under the License is distributed on an "AS IS" BASIS,
%   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%   See the License for the specific language governing permissions and
%   limitations under the License.
%
clear;

addpath(genpath('common_matlab/'));
addpath('functions/');

rm_file('jobs/jobs_03_09_BA_plots.txt')

load('workspaces/ws_03/correlations.mat');

numConfGroups = size(ind_conf_groups{1},2);

% For each Site
for i = 1:numConfGroups
    fileID = fopen('jobs/jobs_03_09_BA_plots.txt', 'a');
    fprintf(fileID, './scripts/script_03_09_BA_plots.sh %s\n', num2str(i));
    fclose(fileID);    
end

clear;
