#!/bin/bash

origDir=`pwd`

/opt/fmrib/MATLAB/R2017a/bin/matlab -nodisplay -nosplash -r "addpath('$origDir/functions/');func_03_09_BA_plots($1); exit"

