#!/bin/bash

origDir=`pwd`

/opt/fmrib/MATLAB/R2017a/bin/matlab -nojvm -nodisplay -nosplash -r "addpath('$origDir/functions/');func_03_04_BA_plots($1); exit"

