#!/bin/env python
#
# Authors: Fidel Alfaro-Almagro, Stephen M. Smith, Tom Nichols, & Paul McCarthy
#
# Copyright 2017 University of Oxford
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

import plotly
import numpy as np
import plotly.plotly as py
import sys,argparse,os.path

class MyParser(argparse.ArgumentParser):
    def error(self, message):
        sys.stderr.write('error: %s\n' % message)
        self.print_help()
        sys.exit(2)

class Usage(Exception):
    def __init__(self, msg):
        self.msg = msg

def main():

    parser = MyParser(description='Correlation matrix generation tool')
    parser.add_argument("dataFile", help='Data to generate violin plots')
    parser.add_argument("randomFile", help='Random confounds for the violin plots')
    parser.add_argument("namesConfFile", help='Names for the Conf Categories')
    parser.add_argument("namesIDPsFile", help='Names for the IDPs')
    parser.add_argument("outputFile", nargs='?', default='./corr_matrix', \
                                        help='Output file')
    parser.add_argument('--apply_log', default=False, action='store_true', \
                        help='Apply log_10 to the data')

    argsa = parser.parse_args()

    dataFile   = argsa.dataFile
    randomFile = argsa.randomFile
    namesConfFile  = argsa.namesConfFile
    namesIDPsFile  = argsa.namesIDPsFile
    outputFile = argsa.outputFile
    apply_log  = argsa.apply_log

    if not outputFile.endswith('.html'):
        outputFile = outputFile + '.html'

    dataD = np.loadtxt(dataFile)
    randD = np.loadtxt(randomFile)

    if dataD.ndim == 1:
        dataD = dataD.reshape(dataD.shape[0],1)
    if randD.ndim == 1:
        randD = randD.reshape(randD.shape[0],1)
    
    with open(namesConfFile, 'r') as f:
        namesConfNum = f.readlines()
    namesConf = [x.strip() for x in namesConfNum]
    numConf = [x.strip().split(' ')[1] for x in namesConfNum]
    with open(namesIDPsFile, 'r') as f:
        namesIDPs = f.readlines()
    namesIDPs = [x.strip() for x in namesIDPs]
    randNamesConf = ['RAND ' + '(' + namesConf[i] + ')' for i in \
                                                            range(len(numConf))]

    colorbar_title = '% of Unique Variance Explained'
    yrange = [0.0001,100]
    if apply_log:
        data_no_log = np.array(dataD)
        rand_no_log = np.array(randD)
        randD = np.log10(randD)
        dataD = np.log10(dataD)
        colorbar_title = 'Log10 of % of Unique Variance Explained'
        yrange=[-4, 2]

    final_data1=[]
    final_data2=[]


    for i in range(dataD.shape[0]):
        text = [str.format('{:2.3f}', data_no_log[i,j]) + '% of ' + \
                namesIDPs[j]+ ' explained by ' + namesConf[i] + \
                ' confounds.' for j in range(len(namesIDPs))]
        conf = {
            "type": 'violin',
            "x0": namesConf[i],
            "y": dataD[i,:],
            "offsetgroup": str((i*2)),
            "scalegroup": namesConf[i],
            "text" : text,
            "name": namesConf[i],

            "box": {
                "visible" : True,
                "fillcolor": '#FFFFFF',
                "line": {
                    "color": '#000000',
                    "width": 1
                }
            },
            "points": 'all',
            "pointpos": 0,
            "jitter": 0,
            "scalemode": 'count',
            "meanline": {
                "visible": True
            },
            "marker": {
                "size" : 0.1,
                "line": {
                    "width": 0
                }
            },
            "showlegend": False
        }
        final_data1.append(conf)
        randomC = {
            "type": 'violin',
            "x0":  randNamesConf[i],
            "y": randD[i,:],
            "offsetgroup":  str((i*2) +1),
            "scalegroup":  randNamesConf[i],
            "text" : text,
            "name":  randNamesConf[i],
            "box": {
                "visible" : True,
                "fillcolor": '#FFFFFF',
                "line": {
                    "color": '#000000',
                    "width": 1
                }
            },
            "points": False,
            "scalemode": 'count',
            "meanline": {
                "visible": True
            },
            "line": {
                "color": '#AAAAAA'
            },
            "marker": {
                "size" : 0.001,
            },
            "showlegend": False

        }
        final_data1.append(randomC)
        topNum= {
            "type": 'violin',
            "x0": 'C ' + numConf[i],
            "y": [np.random.rand(),np.random.rand()],
            "showlegend": False
        }
        final_data2.append(topNum)
        final_data2.append(topNum)
    fig = {
        "data": final_data1,
        "layout" : {
            "title": dataFile.replace('_', ' ').replace('.txt','').split('/')[-1],
            "margin": {
                "b" : 200
            },
            "xaxis": {
                "showgrid":True,
                "showline":True,
                "tickangle": 270
            },
            "yaxis": {
                "title" : colorbar_title,
                "range": yrange,
                "zeroline": False,
            },
            "hovermode": "closest"
        }
    }
    plotly.offline.plot(fig, filename=outputFile)

if __name__ == "__main__":
    main()
