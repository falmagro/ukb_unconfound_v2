#!/bin/bash

if [ ! "$1" == "" ] ; then
    waitjob=" -j $1 "
else
    waitjob=""
fi

rm   -rf logs/logs_03_09_BA_plots/
mkdir -p logs/logs_03_09_BA_plots/

rm TXT_DATA/BA/*.txt
rm HTML/BA/*.txt

rm -rf   workspaces/ws_03/BA_09/
mkdir -p workspaces/ws_03/BA_09/

res=`fsl_sub -l logs/logs_03_09_BA_plots/ $waitjob -s openmp,3 -t jobs/jobs_03_09_BA_plots.txt`

echo $res 

