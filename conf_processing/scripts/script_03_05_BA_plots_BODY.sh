#!/bin/bash

origDir=`pwd`

/opt/fmrib/MATLAB/R2017a/bin/matlab -nojvm -nodisplay -nosplash -r "addpath('$origDir/functions/');func_03_05_BA_plots_BODY($1); exit"

