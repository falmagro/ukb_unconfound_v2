%
%   Authors: Fidel Alfaro-Almagro, Stephen M. Smith, Tom Nichols, & Paul McCarthy
%
%   Copyright 2017 University of Oxford
%
%   Licensed under the Apache License, Version 2.0 (the "License");
%   you may not use this file except in compliance with the License.
%   You may obtain a copy of the License at
%
%    http://www.apache.org/licenses/LICENSE-2.0
%
%   Unless required by applicable law or agreed to in writing, software
%   distributed under the License is distributed on an "AS IS" BASIS,
%   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%   See the License for the specific language governing permissions and
%   limitations under the License.
%
clear;

addpath(genpath('common_matlab/'));
addpath('functions/');

rm_file('jobs/jobs_02_04_ve_short.txt');
rm_file('jobs/jobs_02_04_ve_long.txt');
make_dir('jobs');
make_dir('workspaces/ws_02/ve/');

load('workspaces/ws_02/confounds_generated.mat');

num_IDPs = length(IDPnames);

num1=floor(num_IDPs * 0.3); % Splitting variables to use 2 SGE queues

% For each IDP
for i = 1:num1
    fileID = fopen(['jobs/jobs_02_04_ve_short.txt'], 'a');
    fprintf(fileID, './scripts/script_02_04_ve.sh %s\n', num2str(i));
    fclose(fileID);    
end

% For each IDP
for i = num1+1:num_IDPs
    fileID = fopen(['jobs/jobs_02_04_ve_long.txt'], 'a');
    fprintf(fileID, './scripts/script_02_04_ve.sh %s\n', num2str(i));
    fclose(fileID);    
end

clear;
