#!/bin/bash

if [ ! "$1" == "" ] ; then
    waitjob=" -j $1 "
else
    waitjob=""
fi

res=""

rm    -rf logs/logs_01_05_gen_nonlin_conf/
mkdir -p  logs/logs_01_05_gen_nonlin_conf/

rm     -f workspaces/ws_01/veu_nonlin/*

res=`      fsl_sub -l logs/logs_01_05_gen_nonlin_conf/ $waitjob -q     short.q -t jobs/jobs_01_05_gen_nonlin_conf_long.txt`
res="$res,`fsl_sub -l logs/logs_01_05_gen_nonlin_conf/ $waitjob -q veryshort.q -t jobs/jobs_01_05_gen_nonlin_conf_short.txt`"

echo $res 
