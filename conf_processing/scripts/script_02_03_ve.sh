#!/bin/bash

if [ ! "$1" == "" ] ; then
    waitjob=" -j $1 "
else
    waitjob=""
fi

res=""

rm    -rf logs/logs_02_04_ve
mkdir  -p logs/logs_02_04_ve

res=`      fsl_sub -l logs/logs_02_04_ve/ $waitjob -q short.q -t jobs/jobs_02_04_ve_long.txt`
res="$res,`fsl_sub -l logs/logs_02_04_ve/ $waitjob -q short.q -t jobs/jobs_02_04_ve_short.txt`"

echo $res 
