%
%   Authors: Fidel Alfaro-Almagro, Stephen M. Smith, Tom Nichols, & Paul McCarthy
%
%   Copyright 2017 University of Oxford
%
%   Licensed under the Apache License, Version 2.0 (the "License");
%   you may not use this file except in compliance with the License.
%   You may obtain a copy of the License at
%
%    http://www.apache.org/licenses/LICENSE-2.0
%
%   Unless required by applicable law or agreed to in writing, software
%   distributed under the License is distributed on an "AS IS" BASIS,
%   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%   See the License for the specific language governing permissions and
%   limitations under the License.
%
clear;

addpath(genpath('common_matlab/'));
addpath('functions/');

my_log('', 'Before load');
load('workspaces/ws_01/ws_01_04.mat');
fm = (split(mfilename('fullpath'),'/')); 
fm = fm{end};
my_log(fm, 'After initial load');

fid = fopen('tables/list_nonlin.txt');
tmp = textscan(fid, '%s', 'Delimiter', '\n');

final=[];

for i=1:length(tmp{1})
   for j=1:length(names_nonlin)
       if strcmp(tmp{1}{i}, names_nonlin{j})
           final=[final;j];
       end
   end
end

final=sort(final);
num_nonlin = length(final);
conf_nonlin  = conf_nonlin(:, final);
names_nonlin = (names_nonlin(final))';


fileID = fopen('tables/summary.txt', 'a');
fprintf(fileID, 'Number of NONLINs after thresholding: %s\n', num2str(num_nonlin));
fclose(fileID);

% Saving workspace
save('workspaces/ws_01/conf_nonlin.mat',  'conf_nonlin',  '-v7.3');
save('workspaces/ws_01/names_nonlin.mat', 'names_nonlin', '-v7.3');
my_log(fm, 'End');
