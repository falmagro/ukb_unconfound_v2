%
%   Authors: Fidel Alfaro-Almagro, Stephen M. Smith, Tom Nichols, & Paul McCarthy
%
%   Copyright 2017 University of Oxford
%
%   Licensed under the Apache License, Version 2.0 (the "License");
%   you may not use this file except in compliance with the License.
%   You may obtain a copy of the License at
%
%    http://www.apache.org/licenses/LICENSE-2.0
%
%   Unless required by applicable law or agreed to in writing, software
%   distributed under the License is distributed on an "AS IS" BASIS,
%   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%   See the License for the specific language governing permissions and
%   limitations under the License.
%
clear;

load('workspaces/ws_01/ws_01_16.mat')
load('workspaces/ws_01/IDPs.mat');

dataName = '41k';
dataDir = strcat('../data/', dataName, '_data/');

conf       = [confAge confSex confAgeSex confHeadSize confSite confBatch ...
              confCMRR confProtocol confServicePack confScanRamp ...
              confScanColdHead confScanHeadCoil confScanMisc ...
              confFlippedSWI confFST2 confNewEddy confScaling confTE ...
              confStructHeadMotion confDVARS confHeadMotion confHeadMotionST ...
              confTablePos confEddyQC conf_nonlin conf_ct confAcqTime ...
              confAcqDate];
          
conf_Group = {confAge,confSex,confAgeSex,confHeadSize,confSite,confBatch,...
              confCMRR,confProtocol,confServicePack,confScanRamp,...
              confScanColdHead,confScanHeadCoil,confScanMisc,...
              confFlippedSWI,confFST2,confNewEddy,confScaling,confTE,...
              confStructHeadMotion,confDVARS,confHeadMotion,confHeadMotionST,...
              confTablePos,confEddyQC,conf_nonlin,conf_ct,confAcqTime,...
              confAcqDate};        
          
names = [namesAge;namesSex;namesAgeSex;namesHeadSize;namesSite;namesBatch;...
         namesCMRR;namesProtocol;namesServicePack;namesScanRamp;...
         namesScanColdHead;namesScanHeadCoil;namesScanMisc;...
         namesFlippedSWI;namesFST2;namesNewEddy;namesScaling;namesTE;...
         namesStructHeadMotion;namesDVARS;namesHeadMotion;namesHeadMotionST;...
         namesTablePos;namesEddyQC;names_nonlin;names_ct;namesAcqTime;...
         namesAcqDate];        

group_Names = {};
group_Names{1} = {'AGE', 'SEX', 'AGE_SEX', 'HEAD_SIZE', 'SITE', 'BATCH', ...
                  'CMRR', 'PROTOCOL', 'SERVICE_PACK', 'SCAN_RAMP', ...
                  'SCAN_COLD_HEAD', 'SCAN_HEAD_COIL', 'SCAN_MISC', ...
                  'FLIPPED_SWI', 'FS_T2', 'NEW_EDDY', 'SCALING', 'TE', ...
                  'STRUCT_MOTION', 'DVARS', 'HEAD_MOTION', 'HEAD_MOTION_ST', ...
                  'TABLE_POS', 'EDDY_QC', 'NON_LIN' 'CROSSED_TERMS', ...
                  'ACQ_TIME', 'ACQ_DATE'}; 

           
num_conf      = {};
num_groups    = [];
num_conf{1}   = size(conf,2);
num_groups(1) = length(conf_Group);
ind_no_nonlin = 1:size(conf,2);

%This will allow for multiple confound groupings.
ind_conf_groups = {};

previous = 0;
for i = 1:num_groups(1)
    num_elements=size(conf_Group{i},2);
    ind_conf_groups{i} = [previous + 1 : previous + num_elements];
    previous = previous + num_elements;
end

save('tmp_vars', 'GEN_names', 'COGN_names', 'BODY_names', ...
                 'GEN_vars',  'COGN_vars',  'BODY_vars', '-v7.3');

clear i j k sigma cont previous
clear princ_components_cell
clear princ_components_cell_st
clear total_princ_components_cell
clear total_princ_components_cell_st

clear loaded_smoothed_subset_IDPs
clear loaded_smoothed_subset_IDPs_st
clear subset_IDPs_i_deconf
clear subset_IDPs_i_deconf_st
clear subset_IDPs_i_st
clear conf_Group

clear namesServicePack
clear namesSite
clear namesAge
clear namesSex
clear namesAgeSex
clear namesCoil
clear namesHeadSize
clear namesQuench
clear namesFlippedSWI
clear namesYTranslation
clear namesStructHeadMotion
clear namesFST2
clear namesProtocol
clear namesBatch
clear namesCMRR
clear namesEddyQC
clear namesTablePos
clear namesHeadMotionST
clear namesDVARS
clear namesAcqTime
clear namesHeadMotion
clear namesAcqDate
clear namesScanRamp;
clear namesScanColdHead;
clear namesScanHeadCoil;
clear namesScanMisc;

clear confAcqDateCell
clear confAcqTimeCell
clear confAge
clear confAgeSex
clear confCoil
clear confFlippedSWI
clear confFST2
clear confHeadSize
clear confQuench
clear confSex
clear confStructHeadMotion
clear confYTranslation
clear confProtocol
clear confBatchclear confCMRR
clear confEddyQC
clear confTablePos
clear confServicePack
clear confAcqTimeLinear
clear conf_Non_Temporal
clear confAcqDate
clear confHeadMotionST
clear confDVARS
clear confHeadMotion
clear confAcqTime
clear confBatch
clear confSite
clear confScanRamp;
clear confScanColdHead;
clear confScanHeadCoil;
clear confScanMisc;

clear GEN_names
clear COGN_names
clear BODY_names

clear GEN_vars
clear COGN_vars
clear BODY_vars

clear indNotZero
clear dataName
clear FSnames
clear siteValues
clear siteDATA
clear namesDir
clear fm
clear finalFile 
clear numDateComponents
clear numTempComponents

clear num_elements
clear indSite_st
clear ind_no_nonlin
clear new_index_sortedTime
clear index_sortedTime
clear index_sortedDate
clear siteDATA_st
clear scan_date
clear scan_date_cont
clear scan_time
clear sortedTime

clear unsorted_index_sortedTime;

num_sites = numSites; clear numSites;
num_subjects = N; clear N;
num_conf = num_conf{1};
num_conf_groups = num_groups ; clear num_groups;

IDPs = subset_IDPs ; clear subset_IDPs;
IDPs_i = subset_IDPs_i ; clear subset_IDPs_i;
IDP_names = IDPnames ; clear IDPnames;

ind_site = indSite ; clear indSite;

site=zeros(size(ind_site));
for i = 1:length(ind_site)
   site(ind_site{i})=i; 
end


subject_IDs = ALL_IDs; clear ALL_IDs;
conf_names = names ; clear names;
conf_groups_names = group_Names ; clear group_Names;

scan_date = sortedDate ; clear sortedDate;
scan_time = day_fraction ; clear day_fraction;

QC_IDPs = var_QC_IDPs ; clear var_QC_IDPs;

conf_YTRANS        = nets_load_match(strcat(dataDir, 'ID_YTRANSLATION.txt'), subject_IDs);
conf_NEWEDDY       = nets_load_match(strcat(dataDir, 'ID_NEWEDDY.txt'),      subject_IDs);
conf_SCALING_T1    = nets_load_match(strcat(dataDir, 'ID_T1SCALING.txt'),    subject_IDs);
conf_SCALING_T2    = nets_load_match(strcat(dataDir, 'ID_T2SCALING.txt'),    subject_IDs);
conf_SCALING_SWI   = nets_load_match(strcat(dataDir, 'ID_SWISCALING.txt'),   subject_IDs);
conf_SCALING_dMRI  = nets_load_match(strcat(dataDir, 'ID_dMRISCALING.txt'),  subject_IDs);
conf_SCALING_rfMRI = nets_load_match(strcat(dataDir, 'ID_rfMRISCALING.txt'), subject_IDs);
conf_SCALING_tfMRI = nets_load_match(strcat(dataDir, 'ID_tfMRISCALING.txt'), subject_IDs);
conf_TE_rfMRI      = nets_load_match(strcat(dataDir, 'ID_TERFMRI.txt'),      subject_IDs);
conf_TE_tfMRI      = nets_load_match(strcat(dataDir, 'ID_TETFMRI.txt'),      subject_IDs);
conf_FST2          = nets_load_match(strcat(dataDir, 'ID_FST2.txt'),         subject_IDs); 
head_size_scaling  = nets_load_match(strcat(dataDir, 'ID_HEADSIZE.txt'),     subject_IDs); 
TABLEPOS           = nets_load_match(strcat(dataDir, 'ID_TABLEPOS.txt'),     subject_IDs);

conf_TablePos_COG_X     = TABLEPOS(:,1);
conf_TablePos_COG_Y     = TABLEPOS(:,2);
conf_TablePos_COG_Z     = TABLEPOS(:,3);
conf_TablePos_COG_Table = TABLEPOS(:,4);

repeated_IDs = [];
for i = 1:num_subjects
    if subject_IDs(i) > 30000000
        repeated_IDs = [repeated_IDs; i];
    end
end

clear TABLEPOS;
clear a b i en ans na cad1 cad2 blue cmdout comm dataDir;
clear IDP_group_dir IDP_group_files IDP_name; 

clear S1 S2 V V1;
clear avg_VE cOrder;
clear confAgeSex_nonlin;
clear confAge_nonlin;
clear confDVARS_nonlin;
clear confEDDYQC_nonlin;
clear confHEADMOTIONST_nonlin;
clear confHEADMOTION_nonlin;
clear confHEADSIZE_nonlin;
clear confNewEddy;
clear confSTRUCTHEADMOTION_nonlin;
clear confTABLEPOS_nonlin;
clear confScaling;

clear confTE;
clear confTE_nonlin;
clear conf_PCA;
clear conf_Simple;
clear conf_ct;
clear conf_ind;
clear conf_no_crossed;
clear conf_nonlin;
clear correctOrder;
clear darkblue;

clear exclude_ind_ct;
clear fid fileID fileIDPs fileNLs fileName;
clear final final_ct_conf_list final_file_name final_ind;
clear group_Names_Simple;
clear h h1 hleg hline iH il ile le n;
clear indNotNaN indPairs ind_IDP_groups ind_conf;
clear ind_ct ind_no_ct ind_nonlin indi indiMat;
clear inds_for_UVE inds_for_avg;

clear namF name_fil names2;
clear namesAgeSex_nonlin;
clear namesAge_nonlin;
clear namesDVARS_nonlin;
clear namesEDDYQC_nonlin;
clear namesHEADMOTIONST_nonlin;
clear namesHEADMOTION_nonlin;
clear namesHEADSIZE_nonlin;
clear namesSTRUCTHEADMOTION_nonlin;
clear namesTABLEPOS_nonlin;
clear namesNewEddy;
clear namesScaling;
clear namesTE;
clear namesTE_nonlin;
clear names_Simple;
clear names_ct;
clear names_nonlin;

clear HeadSizeScaling;
clear newIndices new_ind new_indi new_maxV new_names;
clear numV numVals num_IDP_groups num_conf_Simple num_ct num_div 
clear num_IDPs num_no_ct num_no_nonlin num_nonlin;
clear plotNames nbins yt ytl limVE;
clear red status teal temp tmp;
clear thr_for_UVE thr_for_avg;
clear veu clear total_veu;
clear dataDir dataName num_conf num_sites num_subjects;

save('workspace_for_steve', '-v7.3');

dataName = '41k';
dataDir = strcat('../data/', dataName, '_data/');

clear age sex site;
clear conf_YTRANS;
clear conf_NEWEDDY;
clear conf_SCALING_T1;
clear conf_SCALING_T2;
clear conf_SCALING_SWI;
clear conf_SCALING_dMRI;
clear conf_SCALING_rfMRI;
clear conf_SCALING_tfMRI;
clear conf_TE_rfMRI;
clear conf_TE_tfMRI;
clear conf_FST2;
clear TABLEPOS;
clear IDPs;

clear head_size_scaling;

clear conf_TablePos_COG_X;
clear conf_TablePos_COG_Y;
clear conf_TablePos_COG_Z;
clear conf_TablePos_COG_Table;

load('tmp_vars.mat');
rm_file('tmp_vars.mat');

save('workspaces/ws_01/final_workspace', '-v7.3');
