%
%   Authors: Fidel Alfaro-Almagro, Stephen M. Smith, Tom Nichols, & Paul McCarthy
%
%   Copyright 2017 University of Oxford
%
%   Licensed under the Apache License, Version 2.0 (the "License");
%   you may not use this file except in compliance with the License.
%   You may obtain a copy of the License at
%
%    http://www.apache.org/licenses/LICENSE-2.0
%
%   Unless required by applicable law or agreed to in writing, software
%   distributed under the License is distributed on an "AS IS" BASIS,
%   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%   See the License for the specific language governing permissions and
%   limitations under the License.
%
clear;

addpath(genpath('common_matlab/'));
addpath('functions/');

my_log('', 'Before load');

load('workspaces/ws_00/clean_workspace.mat');
rm_make_dir('workspaces/ws_01/');
finalFile = 'workspaces/ws_01/raw_confounds_generated.mat';
rm_file(finalFile);

fm = (split(mfilename('fullpath'),'/')); 
fm = fm{end};
my_log(fm, 'After load');

namesDir = '../data/NAMES_confounds/'; 

[sortedDate, index_sortedDate] = sort(scan_date);

age           = age(index_sortedDate);
sex           = sex(index_sortedDate);
day_fraction  = day_fraction(index_sortedDate);
ALL_IDs       = ALL_IDs(index_sortedDate,:);
subset_IDPs   = subset_IDPs(index_sortedDate,:);
subset_IDPs_i = subset_IDPs_i(index_sortedDate,:);
GEN_vars      = GEN_vars(index_sortedDate,:);
BODY_vars     = BODY_vars(index_sortedDate,:);
COGN_vars     = COGN_vars(index_sortedDate,:);
var_QC_IDPs   = var_QC_IDPs(index_sortedDate,:);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% GENERATE SITE CONFOUND AND INDEX SUBJECTS BY SITE %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

siteDATA = getValues(strcat(dataDir, 'ID_SITE.txt'), ALL_IDs);
siteValues = my_unique(siteDATA);
indSite=cell(length(siteValues),1);
for j = 1:length(siteValues)
    valueSite=siteValues(j);
    indSite{j}=find(all(siteDATA==siteValues(j,:),2));
end
clear valueSite j;

namesSite = {};
confSite = zeros(N,length(indSite)-1);

% Subjects from Site 1 will have -1 in all site confounds
confSite(indSite{1},:) = -1;

% Subjects for the other sites will have -1 in their corresponding column
% Value by default is 0.
for i=2:length(indSite)
    confSite(indSite{i},i-1) = 1;
    namesSite=[namesSite ; strcat('Site_1_vs_', num2str(i))];
end
confSite = nets_normalise(confSite); 
confSite(isnan(confSite)) = 0;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% GENERATE CATEGORICAL CONFOUNDS %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
[confSex,          namesSex]          = duplicateCategorical('SEX',          ALL_IDs, indSite, dataName);
[confBatch,        namesBatch]        = duplicateCategorical('BATCH',        ALL_IDs, indSite, dataName);
[confCMRR,         namesCMRR]         = duplicateCategorical('CMRR',         ALL_IDs, indSite, dataName);
[confProtocol,     namesProtocol]     = duplicateCategorical('PROTOCOL',     ALL_IDs, indSite, dataName);
[confServicePack,  namesServicePack]  = duplicateCategorical('SERVICEPACK',  ALL_IDs, indSite, dataName);
[confScanRamp,     namesScanRamp]     = duplicateCategorical('SCANRAMP',     ALL_IDs, indSite, dataName);
[confScanMisc,     namesScanMisc]     = duplicateCategorical('SCANMISC',     ALL_IDs, indSite, dataName);
[confScanColdHead, namesScanColdHead] = duplicateCategorical('SCANCOLDHEAD', ALL_IDs, indSite, dataName);
[confScanHeadCoil, namesScanHeadCoil] = duplicateCategorical('SCANHEADCOIL', ALL_IDs, indSite, dataName);
[confFlippedSWI,   namesFlippedSWI]   = duplicateCategorical('FLIPPEDSWI',   ALL_IDs, indSite, dataName);
[confFST2,         namesFST2]         = duplicateCategorical('FST2',         ALL_IDs, indSite, dataName);
[confNewEddy,      namesNewEddy]      = duplicateCategorical('NEWEDDY',      ALL_IDs, indSite, dataName);
[confScaling,      namesScaling]      = duplicateCategorical('SCALING',      ALL_IDs, indSite, dataName);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% GENERATE CONTINUOUS CONFOUNDS %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
[confHeadMotion,      namesHeadMotion]       = duplicateDemedianNormBySite ('HEADMOTION',       ALL_IDs, indSite, dataName);
[confHeadMotionST,    namesHeadMotionST]     = duplicateDemedianNormBySite ('HEADMOTIONST',     ALL_IDs, indSite, dataName);
[confHeadSize,        namesHeadSize]         = duplicateDemedianNormBySite ('HEADSIZE',         ALL_IDs, indSite, dataName);
[confTablePos,        namesTablePos]         = duplicateDemedianNormBySite ('TABLEPOS',         ALL_IDs, indSite, dataName);
[confDVARS,           namesDVARS]            = duplicateDemedianNormBySite ('DVARS',            ALL_IDs, indSite, dataName);
[confEddyQC,          namesEddyQC]           = duplicateDemedianNormBySite ('EDDYQC',           ALL_IDs, indSite, dataName);
[confStructHeadMotion,namesStructHeadMotion] = duplicateDemedianNormBySite ('STRUCTHEADMOTION', ALL_IDs, indSite, dataName);
[confAge,             namesAge]              = duplicateDemedianNormBySite ('AGE',              ALL_IDs, indSite, dataName);
[confTE,              namesTE]               = duplicateDemedianNormBySite ('TE',               ALL_IDs, indSite, dataName);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% GENERATE AGE-RELATED CONFOUNDS %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
confAgeSex = zeros(size(confAge));

for i = 1:size(confAge,2)
   indNotZero = find(confAge(:,i) ~= 0);
   confAgeSex(indNotZero,i) = nets_normalise( confAge(indNotZero,i) .* confSex(indNotZero,i));
end

namesAgeSex = {};
for j = 1:length(indSite)
    namesAgeSex = [namesAgeSex ; strcat('AgeSex_Site_', num2str(j))]; 
end

save(finalFile, '-v7.3');
my_log(fm, 'End');
