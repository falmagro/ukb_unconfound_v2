%
%   Authors: Fidel Alfaro-Almagro, Stephen M. Smith, Tom Nichols, & Paul McCarthy
%
%   Copyright 2017 University of Oxford
%
%   Licensed under the Apache License, Version 2.0 (the "License");
%   you may not use this file except in compliance with the License.
%   You may obtain a copy of the License at
%
%    http://www.apache.org/licenses/LICENSE-2.0
%
%   Unless required by applicable law or agreed to in writing, software
%   distributed under the License is distributed on an "AS IS" BASIS,
%   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%   See the License for the specific language governing permissions and
%   limitations under the License.
%
clear;
get(0,'Factory'); set(0,'defaultfigurecolor',[1 1 1]);

addpath(genpath('common_matlab/'));
addpath('functions/')

dataName   = '41k';
dataDir    = strcat('../data/', dataName, '_data/');
finalFile = ('workspaces/ws_00/clean_workspace.mat');

rm_file(finalFile);

fm = (split(mfilename('fullpath'),'/')); 
fm = fm{end};
my_log(fm, 'Start processing');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%% input and sort IDPs
ALL_IDs = load(strcat(dataDir, 'subj.txt'));

%%% load main IDPs
ALL_IDPs = nets_load_match(strcat(dataDir, 'IDPs.txt'), ALL_IDs);

indNotNaN = ~isnan(ALL_IDPs(:,17)); % throw away subjects with no T1
ALL_IDPs = ALL_IDPs(indNotNaN,:); 

ALL_IDs = ALL_IDs(indNotNaN);
N = size(ALL_IDs,1);

% Just keep the elements from FMRIB_info that have IDPs
FMRIB_info = nets_load_match(strcat(dataDir, 'initial_workspace.txt'), ALL_IDs);

% Get the acquition time 
timeStampH = floor(FMRIB_info(:,2)/10000); 
timeStampm = floor((FMRIB_info(:,2)-timeStampH*10000)/100); 
timeStampS = (FMRIB_info(:,2)-timeStampH*10000-timeStampm*100);

% Convert time of day to "decimal" hours
FMRIB_info(:,2) = timeStampH+timeStampm/60+timeStampS/3600; 

% Get the fraction of the day when the subject was acquired 
day_fraction = ((timeStampH+timeStampm/60+timeStampS/3600 ) -7) / 13;

% Get acquisition date
timeStampY = floor(FMRIB_info(:,1)/10000); 
timeStampM = floor((FMRIB_info(:,1)-timeStampY*10000)/100); 
timeStampD = (FMRIB_info(:,1)-timeStampY*10000-timeStampM*100);

% convert scan date to "decimal" years
FMRIB_info(:,1)=timeStampY + (( datenum(timeStampY,timeStampM,timeStampD) ...
                - datenum(timeStampY,1,1) ) ) ./  yeardays(timeStampY) ;   

% Calculate the discrete and continuous scan date.
scan_date=FMRIB_info(:,1);
scan_date_cont=timeStampY + (( datenum(timeStampY,timeStampM,timeStampD) ...
                - datenum(timeStampY,1,1) ) +day_fraction) ./ yeardays(timeStampY) ; 

clear timeStampS timeStampm timeStampH timeStampD timeStampM timeStampY;
            
% Calculate resting IDPs
NODEamps25  = nets_load_match(strcat(dataDir, 'rfMRI_d25_NodeAmplitudes_v1.txt'),  ALL_IDs);
NODEamps100 = nets_load_match(strcat(dataDir, 'rfMRI_d100_NodeAmplitudes_v1.txt'), ALL_IDs);
NET25       = nets_load_match(strcat(dataDir, 'rfMRI_d25_partialcorr_v1.txt'),     ALL_IDs);
NET100      = nets_load_match(strcat(dataDir, 'rfMRI_d100_partialcorr_v1.txt'),    ALL_IDs);

% Calculate FS IDPs
FS = nets_load_match(strcat(dataDir, 'FS_IDPs.txt'), ALL_IDs);



%%% get the list of IDP sub-modalities
%%%
%%% The need of  {' '} and the char casting is an 
%%% objective proof of bad language design. -F
system(char(strcat('cat ', {' '},  dataDir, ...
              'IDPinfo.txt | awk ''{print $1}'' > temp.txt')));
po = fopen(sprintf('temp.txt'),'r'); 
system('rm temp.txt');

IDPnames=textscan(po,'%s');  
fclose(po);  
IDPnames=IDPnames{1};

tmp = {};
for i=2:length(IDPnames)
    tmp{i-1}=IDPnames{i};
end
IDPnames=tmp';

j=length(IDPnames)+1;
for i=1:size(NODEamps25,2)
    IDPnames{j}=sprintf('rfMRI amplitudes (ICA25 node %d)',i);   
    j=j+1;  
end

for i=1:size(NODEamps100,2)
    IDPnames{j}=sprintf('rfMRI amplitudes (ICA100 node %d)',i);  
    j=j+1;  
end

for i=1:size(NET25,2)
    IDPnames{j}=sprintf('rfMRI connectivity (ICA25 edge %d)',i);        
    j=j+1;  
end

for i=1:size(NET100,2)
    IDPnames{j}=sprintf('rfMRI connectivity (ICA100 edge %d)',i);       
    j=j+1;  
end

po = fopen(strcat(dataDir, 'FS_names.txt'),'r'); 
FSnames=textscan(po,'%s');  
fclose(po);  
FSnames=FSnames{1};

FS_use_T2=FS(:,1);
FS=FS(:,2:end);
FSnames=FSnames(2:end);

IDPnames=[IDPnames ; FSnames];


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Joining all the IDPs together and i-normal them %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Remove QC IDPs and IDPs used as confounds
var_QC_IDPs=ALL_IDPs(:,1:16);
QC_IDP_names = IDPnames(1:16);
ind_IDPs_to_exclude = [1:17 888:892];
ind_IDPs_to_include = setdiff([1:size(ALL_IDPs,2)],ind_IDPs_to_exclude);
subset_IDPs = [ALL_IDPs(:,ind_IDPs_to_include) ...
               NODEamps25 NODEamps100 NET25 NET100 FS];
           
ind_names_to_include = [ind_IDPs_to_include 893:length(IDPnames)];
IDPnames = IDPnames(ind_names_to_include); 

my_log(fm, 'Before inormal');
if check_GPU()
    subset_IDPs_i = gather(nets_inormal(gpuArray(subset_IDPs)));
else
    subset_IDPs_i = nets_inormal(subset_IDPs);
end
    
my_log(fm, 'After inormal - Before unconfound for ACQ Time');

%%%%%%%%%%%%
% Non-IDPs %
%%%%%%%%%%%%

GEN_vars  = nets_load_match(strcat(dataDir, 'ID_OTHER_GENERAL.txt'),  ALL_IDs);
BODY_vars = nets_load_match(strcat(dataDir, 'ID_BODY.txt'), ALL_IDs);
COGN_vars = nets_load_match(strcat(dataDir, 'ID_COGN.txt'), ALL_IDs);

GEN_names  = textscan(fopen(strcat(dataDir, 'OTHER_GENERAL_names.txt'),  'r'), '%s');  
BODY_names = textscan(fopen(strcat(dataDir, 'BODY_names.txt'), 'r'), '%s');  
COGN_names = textscan(fopen(strcat(dataDir, 'COGN_names.txt'), 'r'), '%s');  

GEN_names  = GEN_names{1};
BODY_names = BODY_names{1};
COGN_names = COGN_names{1};


% Calculate the age of each subject.
scan_date(isnan(scan_date))=max(scan_date)+0.1; % sometimes needed with missing dates
yobI = GEN_vars(:,2); 
mobI = GEN_vars(:,3); 
birth_date = yobI+(mobI-0.5)/12;
age = scan_date - birth_date;

% Generate the sex of each subject
sex = GEN_vars(:,1); 


%Save Age, Sex and HeadSize in TXT files
fileID_AGE_name  = strcat(dataDir, 'ID_AGE.txt');
fileID_SEX_name  = strcat(dataDir, 'ID_SEX.txt');
fileID_HDS_name  = strcat(dataDir, 'ID_HEADSIZE.txt');
fileID_TOD_name  = strcat(dataDir, 'ID_TOD.txt');
fileID_FST2_name = strcat(dataDir, 'ID_FST2.txt');

rm_file(fileID_AGE_name);
rm_file(fileID_SEX_name);
rm_file(fileID_HDS_name);
rm_file(fileID_TOD_name);
rm_file(fileID_FST2_name);

fileID_AGE  = fopen(fileID_AGE_name,  'a');
fileID_SEX  = fopen(fileID_SEX_name,  'a');
fileID_HDS  = fopen(fileID_HDS_name,  'a');
fileID_TOD  = fopen(fileID_TOD_name,  'a');
fileID_FST2 = fopen(fileID_FST2_name, 'a');

for i = 1:size(ALL_IDPs,1)
    fprintf(fileID_AGE, '%i %2.4f\n',  ALL_IDs(i), age(i));
    fprintf(fileID_SEX, '%i %i\n',     ALL_IDs(i), sex(i));
    if isnan(ALL_IDPs(i,18))
        fprintf(fileID_HDS, '%i NaN\n', ALL_IDs(i));
    else
        fprintf(fileID_HDS, '%i %1.10f\n', ALL_IDs(i), ALL_IDPs(i,17));
    end
    fprintf(fileID_TOD, '%i %1.8f\n',  ALL_IDs(i), day_fraction(i));
    fprintf(fileID_FST2, '%i %i\n',  ALL_IDs(i), FS_use_T2(i));
end

fclose(fileID_AGE);  clear fileID_AGE_name  fileID_AGE;
fclose(fileID_SEX);  clear fileID_SEX_name  fileID_SEX;
fclose(fileID_HDS);  clear fileID_HDS_name  fileID_HDS;
fclose(fileID_TOD);  clear fileID_TOD_name  fileID_TOD;
fclose(fileID_FST2); clear fileID_FST2_name fileID_FST2;

make_dir('workspaces/ws_00/');
make_dir('figs/EDDYQC/');

ED = nets_load_match(strcat(dataDir, 'ID_EDDYQC.txt'),  ALL_IDs);
TA = nets_load_match(strcat(dataDir, 'ID_TABLEPOS.txt'),  ALL_IDs);

f = figure('visible', 'off','PaperOrientation', 'landscape',...
           'Position', [0,0,1000,1000]);
dscatter(TA(:,3), ED(:,1));
set(gcf,'PaperPositionMode', 'auto', 'PaperOrientation', 'portrait');
set(gca, 'FontSize',17);
set(gca, 'LooseInset', get(gca, 'TightInset'));
xlabel('Table Position - Z');
ylabel('Eddy Currents - X');
title({'Magnitude of X-Eddy currents vs Z Location of subject in scanner'});

print('-dpng','-r70',strcat('figs/EDDYQC/Eddy_QC_X__vs__TablePos_COG_Z.png'));

clear i j ans birth_date mobI yobI dosI po FMRIB_info clear FS_use_T2;
clear ind_IDPs_to_exclude ind_IDPs_to_include ind_names_to_include;
clear ALL_IDPs NODEamps25 NODEamps100 NET25 NET100 FS;
clear ED TA;

save(finalFile, '-v7.3');
my_log(fm, 'End');
