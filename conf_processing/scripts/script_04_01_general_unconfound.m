%
%   Authors: Fidel Alfaro-Almagro, Stephen M. Smith, Tom Nichols, & Paul McCarthy
%
%   Copyright 2017 University of Oxford
%
%   Licensed under the Apache License, Version 2.0 (the "License");
%   you may not use this file except in compliance with the License.
%   You may obtain a copy of the License at
%
%    http://www.apache.org/licenses/LICENSE-2.0
%
%   Unless required by applicable law or agreed to in writing, software
%   distributed under the License is distributed on an "AS IS" BASIS,
%   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%   See the License for the specific language governing permissions and
%   limitations under the License.
%
clear all;
addpath(genpath('common_matlab/'));
load 'workspaces/ws_02/confounds_generated.mat'

dataName = '41k';
dataDir = strcat('../data/', dataName, '_data/');

fm = (split(mfilename('fullpath'),'/')); 
fm = fm{end};
my_log(fm, 'After initial load');

BLOOD_vars                     = nets_load_match(strcat(dataDir, 'ID_BLOOD.txt'), ALL_IDs);
BODY_BONE_vars                 = nets_load_match(strcat(dataDir, 'ID_BODY_BONE.txt'), ALL_IDs);
BODY_CARDIAC_vars              = nets_load_match(strcat(dataDir, 'ID_BODY_CARDIAC.txt'), ALL_IDs);
BODY_vars                      = nets_load_match(strcat(dataDir, 'ID_BODY.txt'), ALL_IDs);
COGN_vars                      = nets_load_match(strcat(dataDir, 'ID_COGN.txt'), ALL_IDs);
EARLY_LIFE_vars                = nets_load_match(strcat(dataDir, 'ID_EARLY_LIFE.txt'), ALL_IDs);
HEARING_vars                   = nets_load_match(strcat(dataDir, 'ID_HEARING.txt'), ALL_IDs);
LIFESTYLE_ALCOHOL_vars         = nets_load_match(strcat(dataDir, 'ID_LIFESTYLE_ALCOHOL.txt'), ALL_IDs);
LIFESTYLE_EXERCISE_WORK_vars   = nets_load_match(strcat(dataDir, 'ID_LIFESTYLE_EXERCISE_WORK.txt'), ALL_IDs);
LIFESTYLE_FOOD_DRINK_vars      = nets_load_match(strcat(dataDir, 'ID_LIFESTYLE_FOOD_DRINK.txt'), ALL_IDs);
LIFESTYLE_GENERAL_vars         = nets_load_match(strcat(dataDir, 'ID_LIFESTYLE_GENERAL.txt'), ALL_IDs);
LIFESTYLE_TOBACCO_vars         = nets_load_match(strcat(dataDir, 'ID_LIFESTYLE_TOBACCO.txt'), ALL_IDs);
MENTAL_HEALTH_SELF_REPORT_vars = nets_load_match(strcat(dataDir, 'ID_MENTAL_HEALTH_SELF_REPORT.txt'), ALL_IDs);
PHYSICAL_ACTIVITY_vars         = nets_load_match(strcat(dataDir, 'ID_PHYSICAL_ACTIVITY.txt'), ALL_IDs);

BLOOD_names                     = textscan(fopen(strcat(dataDir, 'BLOOD_names.txt'), 'r'), '%s'); 
BODY_BONE_names                 = textscan(fopen(strcat(dataDir, 'BODY_BONE_names.txt'), 'r'), '%s'); 
BODY_CARDIAC_names              = textscan(fopen(strcat(dataDir, 'BODY_CARDIAC_names.txt'), 'r'), '%s'); 
BODY_names                      = textscan(fopen(strcat(dataDir, 'BODY_names.txt'), 'r'), '%s'); 
COGN_names                      = textscan(fopen(strcat(dataDir, 'COGN_names.txt'), 'r'), '%s'); 
EARLY_LIFE_names                = textscan(fopen(strcat(dataDir, 'EARLY_LIFE_names.txt'), 'r'), '%s'); 
HEARING_names                   = textscan(fopen(strcat(dataDir, 'HEARING_names.txt'), 'r'), '%s'); 
LIFESTYLE_ALCOHOL_names         = textscan(fopen(strcat(dataDir, 'LIFESTYLE_ALCOHOL_names.txt'), 'r'), '%s'); 
LIFESTYLE_EXERCISE_WORK_names   = textscan(fopen(strcat(dataDir, 'LIFESTYLE_EXERCISE_WORK_names.txt'), 'r'), '%s'); 
LIFESTYLE_FOOD_DRINK_names      = textscan(fopen(strcat(dataDir, 'LIFESTYLE_FOOD_DRINK_names.txt'), 'r'), '%s'); 
LIFESTYLE_GENERAL_names         = textscan(fopen(strcat(dataDir, 'LIFESTYLE_GENERAL_names.txt'), 'r'), '%s'); 
LIFESTYLE_TOBACCO_names         = textscan(fopen(strcat(dataDir, 'LIFESTYLE_TOBACCO_names.txt'), 'r'), '%s'); 
MENTAL_HEALTH_SELF_REPORT_names = textscan(fopen(strcat(dataDir, 'MENTAL_HEALTH_SELF_REPORT_names.txt'), 'r'), '%s'); 
PHYSICAL_ACTIVITY_names         = textscan(fopen(strcat(dataDir, 'PHYSICAL_ACTIVITY_names.txt'), 'r'), '%s'); 

BLOOD_names                     = BLOOD_names{1}; 
BODY_BONE_names                 = BODY_BONE_names{1}; 
BODY_CARDIAC_names              = BODY_CARDIAC_names{1}; 
BODY_names                      = BODY_names{1}; 
COGN_names                      = COGN_names{1}; 
EARLY_LIFE_names                = EARLY_LIFE_names{1}; 
HEARING_names                   = HEARING_names{1}; 
LIFESTYLE_ALCOHOL_names         = LIFESTYLE_ALCOHOL_names{1}; 
LIFESTYLE_EXERCISE_WORK_names   = LIFESTYLE_EXERCISE_WORK_names{1}; 
LIFESTYLE_FOOD_DRINK_names      = LIFESTYLE_FOOD_DRINK_names{1}; 
LIFESTYLE_GENERAL_names         = LIFESTYLE_GENERAL_names{1}; 
LIFESTYLE_TOBACCO_names         = LIFESTYLE_TOBACCO_names{1}; 
MENTAL_HEALTH_SELF_REPORT_names = MENTAL_HEALTH_SELF_REPORT_names{1}; 
PHYSICAL_ACTIVITY_names         = PHYSICAL_ACTIVITY_names{1}; 

nIDPs = [BLOOD_vars BODY_vars BODY_BONE_vars BODY_CARDIAC_vars COGN_vars ...
         EARLY_LIFE_vars HEARING_vars LIFESTYLE_ALCOHOL_vars ...
         LIFESTYLE_EXERCISE_WORK_vars LIFESTYLE_FOOD_DRINK_vars ...
         LIFESTYLE_GENERAL_vars LIFESTYLE_TOBACCO_vars ...
         MENTAL_HEALTH_SELF_REPORT_vars PHYSICAL_ACTIVITY_vars];
    
nIDPs_groups = {BLOOD_vars,BODY_vars,BODY_BONE_vars,BODY_CARDIAC_vars,...
                COGN_vars,EARLY_LIFE_vars,HEARING_vars,...
                LIFESTYLE_ALCOHOL_vars,LIFESTYLE_EXERCISE_WORK_vars,...
                LIFESTYLE_FOOD_DRINK_vars,LIFESTYLE_GENERAL_vars,...
                LIFESTYLE_TOBACCO_vars,MENTAL_HEALTH_SELF_REPORT_vars,...
                PHYSICAL_ACTIVITY_vars};    

nIDPs_names =  [BLOOD_names;BODY_names;BODY_BONE_names;BODY_CARDIAC_names;...
                COGN_names;EARLY_LIFE_names;HEARING_names;...
                LIFESTYLE_ALCOHOL_names;LIFESTYLE_EXERCISE_WORK_names;...
                LIFESTYLE_FOOD_DRINK_names;LIFESTYLE_GENERAL_names;...
                LIFESTYLE_TOBACCO_names;MENTAL_HEALTH_SELF_REPORT_names;...
                PHYSICAL_ACTIVITY_names];      

nIDPs_group_names = {'Blood assays  ', 'Physical - General (Body)  ', ...
                'Physical - Bones  ', 'Physical - Cardiac '...
                'Cognitive  ', 'Early life factors  ', 'Hearing test  ', ...
                'Lifestyle - Alcohol  ', 'Lifestyle - Exercise & work  ', ...
                'Lifestyle - Food & drink  ', 'Lifestyle - General  ', ...
                'Lifestyle - Tobacco  ', 'Mental health self report  ', ...
                'Physical activity test  '}; 
 
num_nIDPs_groups = length(nIDPs_groups);
num_nIDPs = size(nIDPs,2);
            
ind_nIDPs_groups    = {};

previous = 0;
for i = 1:num_nIDPs_groups
    num_elements=size(nIDPs_groups{i},2);
    ind_nIDPs_groups{i} = [previous + 1 : previous + num_elements];
    previous = previous + num_elements;
end

rm_make_dir('../data/nonIDPs/');

for i = 1:num_nIDPs_groups
   name_file = strrep(nIDPs_group_names{i}, ' ', '_');
   
   dlmwrite(strcat('../data/nonIDPs/', name_file, '.txt'), ...
             ind_nIDPs_groups{i}, 'delimiter', ' ');
end

IDPs  = subset_IDPs_i;   
nIDPs = nets_normalise(nIDPs);

clear subset_IDPs_i;
clear nIDPs_groups;
clear BLOOD_vars BLOOD_names;
clear BODY_vars BODY_names;
clear BODY_BONE_vars BODY_BONE_names;
clear BODY_CARDIAC_vars BODY_CARDIAC_names;
clear COGN_vars COGN_names;
clear EARLY_LIFE_vars EARLY_LIFE_names;
clear HEARING_vars HEARING_names;
clear LIFESTYLE_ALCOHOL_vars LIFESTYLE_ALCOHOL_names;
clear LIFESTYLE_EXERCISE_WORK_vars LIFESTYLE_EXERCISE_WORK_names;
clear LIFESTYLE_FOOD_DRINK_vars LIFESTYLE_FOOD_DRINK_names;
clear LIFESTYLE_GENERAL_vars LIFESTYLE_GENERAL_names;
clear LIFESTYLE_TOBACCO_vars LIFESTYLE_TOBACCO_names;
clear MENTAL_HEALTH_SELF_REPORT_vars MENTAL_HEALTH_SELF_REPORT_names;
clear PHYSICAL_ACTIVITY_vars PHYSICAL_ACTIVITY_names;

my_log(fm, 'After loading and formatting all data. Before unconfounding.');
            

% Unconfound each IDP / nIDP matrix with each confound set
IDPs_deconf         = nets_unconfound2(IDPs,  conf); 
nIDPs_deconf        = nets_unconfound2(nIDPs, conf);
IDPs_deconf_Simple  = nets_unconfound2(IDPs,  conf_Simple);
nIDPs_deconf_Simple = nets_unconfound2(nIDPs, conf_Simple);
IDPs_deconf_PCA     = nets_unconfound2(IDPs,  conf_PCA);
nIDPs_deconf_PCA    = nets_unconfound2(nIDPs, conf_PCA);
IDPs_deconf_PCA90   = nets_unconfound2(IDPs,  conf_PCA90);
nIDPs_deconf_PCA90  = nets_unconfound2(nIDPs, conf_PCA90);
IDPs_deconf_PCA99   = nets_unconfound2(IDPs,  conf_PCA99);
nIDPs_deconf_PCA99  = nets_unconfound2(nIDPs, conf_PCA99);

my_log(fm, 'After unconfounding. Before correlating.');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Find the Rho, P, and N of all correlations %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
[R, P, N]                                           = my_corr(IDPs, nIDPs);
[R_deconf, P_deconf, N_deconf]                      = my_corr(IDPs_deconf, nIDPs_deconf);
[R_deconf_Simple, P_deconf_Simple, N_deconf_Simple] = my_corr(IDPs_deconf_Simple, nIDPs_deconf_Simple);
[R_deconf_PCA, P_deconf_PCA, N_deconf_PCA]          = my_corr(IDPs_deconf_PCA, nIDPs_deconf_PCA);
[R_deconf_PCA90, P_deconf_PCA90, N_deconf_PCA90]    = my_corr(IDPs_deconf_PCA90, nIDPs_deconf_PCA90);
[R_deconf_PCA99, P_deconf_PCA99, N_deconf_PCA99]    = my_corr(IDPs_deconf_PCA99, nIDPs_deconf_PCA99);

my_log(fm, 'After correlating. Before saving.');

make_dir('figs/MNHT/');
make_dir('TXT_DATA/MNHT/');
make_dir('HTML/MNHT/');
make_dir('workspaces/ws_04/');

save('workspaces/ws_04/ws_04.mat','-v7.3');

dlmwrite(['TXT_DATA/MNHT/R.txt'], R, 'delimiter', ' ');
dlmwrite(['TXT_DATA/MNHT/P.txt'], P, 'delimiter', ' ');
dlmwrite(['TXT_DATA/MNHT/N.txt'], N, 'delimiter', ' ');
dlmwrite(['TXT_DATA/MNHT/R_deconf.txt'], R_deconf, 'delimiter', ' ');
dlmwrite(['TXT_DATA/MNHT/P_deconf.txt'], P_deconf, 'delimiter', ' ');
dlmwrite(['TXT_DATA/MNHT/N_deconf.txt'], N_deconf, 'delimiter', ' ');
dlmwrite(['TXT_DATA/MNHT/R_deconf_Simple.txt'], R_deconf_Simple, 'delimiter', ' ');
dlmwrite(['TXT_DATA/MNHT/P_deconf_Simple.txt'], P_deconf_Simple, 'delimiter', ' ');
dlmwrite(['TXT_DATA/MNHT/N_deconf_Simple.txt'], N_deconf_Simple, 'delimiter', ' ');
dlmwrite(['TXT_DATA/MNHT/R_deconf_PCA.txt'], R_deconf_PCA, 'delimiter', ' ');
dlmwrite(['TXT_DATA/MNHT/P_deconf_PCA.txt'], P_deconf_PCA, 'delimiter', ' ');
dlmwrite(['TXT_DATA/MNHT/N_deconf_PCA.txt'], N_deconf_PCA, 'delimiter', ' ');
dlmwrite(['TXT_DATA/MNHT/R_deconf_PCA90.txt'], R_deconf_PCA90, 'delimiter', ' ');
dlmwrite(['TXT_DATA/MNHT/P_deconf_PCA90.txt'], P_deconf_PCA90, 'delimiter', ' ');
dlmwrite(['TXT_DATA/MNHT/N_deconf_PCA90.txt'], N_deconf_PCA90, 'delimiter', ' ');
dlmwrite(['TXT_DATA/MNHT/R_deconf_PCA99.txt'], R_deconf_PCA99, 'delimiter', ' ');
dlmwrite(['TXT_DATA/MNHT/P_deconf_PCA99.txt'], P_deconf_PCA99, 'delimiter', ' ');
dlmwrite(['TXT_DATA/MNHT/N_deconf_PCA99.txt'], N_deconf_PCA99, 'delimiter', ' ');

fileName=['TXT_DATA/MNHT/IDPs_labels.txt'];
fileIDPs=fopen(fileName,'w');
fprintf(fileIDPs,'%s\n', IDPnames{:});
fclose(fileIDPs);

fileName=['TXT_DATA/MNHT/nIDPs_labels.txt'];
filenIDPs=fopen(fileName,'w');
fprintf(filenIDPs,'%s\n', nIDPs_names{:});
fclose(filenIDPs);

my_log(fm, 'After saving. Before generating Manhattan plots.')

my_manhattan_plot(P, ind_IDP_groups, ind_nIDPs_groups, ...
                  nIDPs_group_names, ' no unconfounding', ...
                  'figs/MNHT/P', ...
                  '-log_1_0(P)', 'non-IDPs Variables');
my_manhattan_plot(P_deconf, ind_IDP_groups, ind_nIDPs_groups, ...
                  nIDPs_group_names, ' full unconfounding', ...
                  'figs/MNHT/P_deconf', ...
                  '-log_1_0(P)', 'non-IDPs Variables');
my_manhattan_plot(P_deconf_Simple, ind_IDP_groups, ind_nIDPs_groups, ...
                  nIDPs_group_names, ' Simple unconfounding', ...
                  'figs/MNHT/P_deconf_simple', ...
                  '-log_1_0(P)', 'non-IDPs Variables');
my_manhattan_plot(P_deconf_PCA, ind_IDP_groups, ind_nIDPs_groups, ...
                  nIDPs_group_names, ' PCA unconfounding', ...
                  'figs/MNHT/P_deconf_PCA', ...
                  '-log_1_0(P)', 'non-IDPs Variables');
my_manhattan_plot(P_deconf_PCA90, ind_IDP_groups, ind_nIDPs_groups, ...
                  nIDPs_group_names, ' PCA90 unconfounding', ...
                  'figs/MNHT/P_deconf_PCA90', ...
                  '-log_1_0(P)', 'non-IDPs Variables');
my_manhattan_plot(P_deconf_PCA99, ind_IDP_groups, ind_nIDPs_groups, ...
                  nIDPs_group_names, ' PCA99 unconfounding', ...
                  'figs/MNHT/P_deconf_PCA99', ...
                  '-log_1_0(P)', 'non-IDPs Variables');


comm = ['./scripts/script_04_02_manhattan.py ', ...
               'R P N  ', ...
               'IDPs_labels nIDPs_labels ../data/GROUPS_IDPs_7_groups/',...
               ' ../data/nonIDPs/ TXT_DATA/MNHT/ HTML/MNHT/ ', ...
               'No_unconfounding 0.05'];
[status, cmdout] = system(comm);
 
comm = ['./scripts/script_04_02_manhattan.py ', ...
               'R_deconf P_deconf N_deconf  ', ...
               'IDPs_labels nIDPs_labels ../data/GROUPS_IDPs_7_groups/',...
               ' ../data/nonIDPs/ TXT_DATA/MNHT/ HTML/MNHT/ ', ...
               'Full_unconfounding 0.05'];
[status, cmdout] = system(comm);

comm = ['./scripts/script_04_02_manhattan.py ', ...
               'R_deconf_Simple P_deconf_Simple N_deconf_Simple ', ...
               'IDPs_labels nIDPs_labels ../data/GROUPS_IDPs_7_groups/',...
               ' ../data/nonIDPs/ TXT_DATA/MNHT/ HTML/MNHT/ ', ...
               'Simple_unconfounding 0.05'];
[status, cmdout] = system(comm);

comm = ['./scripts/script_04_02_manhattan.py ', ...
               'R_deconf_PCA P_deconf_PCA N_deconf_PCA ', ...
               'IDPs_labels nIDPs_labels ../data/GROUPS_IDPs_7_groups/',...
               ' ../data/nonIDPs/ TXT_DATA/MNHT/ HTML/MNHT/ ', ...
               'PCA_unconfounding 0.05'];
[status, cmdout] = system(comm);

comm = ['./scripts/script_04_02_manhattan.py ', ...
               'R_deconf_PCA90 P_deconf_PCA90 N_deconf_PCA90 ', ...
               'IDPs_labels nIDPs_labels ../data/GROUPS_IDPs_7_groups/',...
               ' ../data/nonIDPs/ TXT_DATA/MNHT/ HTML/MNHT/ ', ...
               'PCA90_unconfounding 0.05'];
[status, cmdout] = system(comm);

comm = ['./scripts/script_04_02_manhattan.py ', ...
               'R_deconf_PCA99 P_deconf_PCA99 N_deconf_PCA99 ', ...
               'IDPs_labels nIDPs_labels ../data/GROUPS_IDPs_7_groups/',...
               ' ../data/nonIDPs/ TXT_DATA/MNHT/ HTML/MNHT/ ', ...
               'PCA99_unconfounding 0.05'];
[status, cmdout] = system(comm);
              

my_BA_plot_no_python(P_deconf(:),P_deconf(:), P_deconf_Simple(:), ...
          R_deconf_Simple(:), N_deconf(:), IDPnames, nIDPs_names, ...
          'ALL_vs_SIMPLE', 'IDP', 'non-IDPs', size(P_deconf), ...
          'non-IDP', 'figs/MNHT/', 'TXT_DATA/MNHT/', 'HTML/MNHT/', ...
          ['After unconfounding with the full set of ' ...
            num2str(size(conf,2)) ' confounds.'], ...
          'After unconfounding with the SIMPLE set of confounds.', ...
          '-log_1_0 P values for correlations between non-IDPs and IDPs');

my_BA_plot_no_python(P_deconf(:),P_deconf(:), P(:), ...
          R(:), N_deconf(:), IDPnames, nIDPs_names, ...
          'ALL_vs_NONE', 'IDP', 'non-IDPs', size(P_deconf), ...
          'non-IDP', 'figs/MNHT/', 'TXT_DATA/MNHT/', 'HTML/MNHT/', ...
          ['After unconfounding with the full set of ' ...
            num2str(size(conf,2)) ' confounds.'], ...
          'After no unconfounding.', ...
          '-log_1_0 P values for correlations between non-IDPs and IDPs');

my_BA_plot_no_python(P_deconf(:),P_deconf(:), P_deconf_PCA99(:), ...
          R_deconf_PCA99(:), N_deconf(:), IDPnames, nIDPs_names, ...
          'ALL_vs_PCA99', 'IDP', 'non-IDPs', size(P_deconf), ...
          'non-IDP', 'figs/MNHT/', 'TXT_DATA/MNHT/', 'HTML/MNHT/', ...
          ['After unconfounding with the full set of ' ...
            num2str(size(conf,2)) ' confounds.'], ...
          ['After unconfounding with the PCA99 (' ...
          num2str(size(conf_PCA99,2)) ') set of confounds.'], ...
          '-log_1_0 P values for correlations between non-IDPs and IDPs');

my_BA_plot_no_python(P_deconf_Simple(:),P_deconf_Simple(:), P_deconf_PCA99(:), ...
          R_deconf_PCA99(:), N_deconf_Simple(:), IDPnames, nIDPs_names, ...
          'SIMPLE_vs_PCA99', 'IDP', 'non-IDPs', size(P_deconf_Simple), ...
          'non-IDP', 'figs/MNHT/', 'TXT_DATA/MNHT/', 'HTML/MNHT/', ...
          ['After unconfounding with the SIMPLE set of confounds.'], ...
          ['After unconfounding with the PCA99 (' ...
          num2str(size(conf_PCA99,2)) ') set of confounds.'], ...
          '-log_1_0 P values for correlations between non-IDPs and IDPs');