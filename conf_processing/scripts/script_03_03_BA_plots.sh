#!/bin/bash

if [ ! "$1" == "" ] ; then
    waitjob=" -j $1 "
else
    waitjob=""
fi

rm   -rf logs/logs_03_04_BA_plots/
mkdir -p logs/logs_03_04_BA_plots/
rm   -rf logs/logs_03_05_BA_plots/
mkdir -p logs/logs_03_05_BA_plots/

rm   -rf workspaces/ws_03/IDP_deconf_04/
mkdir -p workspaces/ws_03/IDP_deconf_04/
rm   -rf workspaces/ws_03/BODY_vars_i_deconf_05/
mkdir -p workspaces/ws_03/BODY_vars_i_deconf_05/
rm   -rf workspaces/ws_03/COGN_vars_i_deconf_05/
mkdir -p workspaces/ws_03/COGN_vars_i_deconf_05/

res=""

res=`      fsl_sub -l logs/logs_03_04_BA_plots/ $waitjob -q veryshort.q -t jobs/jobs_03_04_BA_plots_short.txt`
res="$res,`fsl_sub -l logs/logs_03_04_BA_plots/ $waitjob -q long.q      -t jobs/jobs_03_04_BA_plots_long.txt`"
res="$res,`fsl_sub -l logs/logs_03_05_BA_plots/ $waitjob -q veryshort.q -t jobs/jobs_03_05_BA_plots_BODY_short.txt`"
res="$res,`fsl_sub -l logs/logs_03_05_BA_plots/ $waitjob -q long.q      -t jobs/jobs_03_05_BA_plots_BODY_long.txt`"
res="$res,`fsl_sub -l logs/logs_03_05_BA_plots/ $waitjob -q veryshort.q -t jobs/jobs_03_05_BA_plots_COGN_short.txt`"
res="$res,`fsl_sub -l logs/logs_03_05_BA_plots/ $waitjob -q long.q      -t jobs/jobs_03_05_BA_plots_COGN_long.txt`"
echo $res 

