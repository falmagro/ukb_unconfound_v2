%
%   Authors: Fidel Alfaro-Almagro, Stephen M. Smith, Tom Nichols, & Paul McCarthy
%
%   Copyright 2017 University of Oxford
%
%   Licensed under the Apache License, Version 2.0 (the "License");
%   you may not use this file except in compliance with the License.
%   You may obtain a copy of the License at
%
%    http://www.apache.org/licenses/LICENSE-2.0
%
%   Unless required by applicable law or agreed to in writing, software
%   distributed under the License is distributed on an "AS IS" BASIS,
%   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%   See the License for the specific language governing permissions and
%   limitations under the License.
%
clear;

addpath(genpath('common_matlab/'));
addpath('functions/');

my_log('', 'Before load');
load('workspaces/ws_02/ve_04.mat');
fm = (split(mfilename('fullpath'),'/')); 
fm = fm{end};
my_log(fm, 'After load');

make_dir('TXT_DATA/GEN/');

% In this case, the confound groupings d 
I = 1;

ve=[];
ve=[ve;variance_explained_i_all{I}];
ve=[ve;variance_explained_Simple{I}];
ve=[ve;variance_explained_PCA{I}];
ve=[ve;variance_explained_PCA90{I}];
ve=[ve;variance_explained_PCA99{I}];

diff_ve=[];
diff_ve=[diff_ve;variance_explained_i_all{I} - variance_explained_Simple{I}];
diff_ve=[diff_ve;variance_explained_PCA{I}   - variance_explained_Simple{I}];
diff_ve=[diff_ve;variance_explained_PCA90{I} - variance_explained_Simple{I}];
diff_ve=[diff_ve;variance_explained_PCA99{I} - variance_explained_Simple{I}];

group_Names{I}{1} = 'ALL';
group_Names{I}{2} = 'SIMPLE';
group_Names{I}{3} = 'PCA-MIN';
group_Names{I}{4} = 'PCA-90%';
group_Names{I}{5} = 'PCA-99%';

group_diff_Names{I}{1} = 'ALL - SIMPLE';
group_diff_Names{I}{2} = '   PCA-MIN - SIMPLE';
group_diff_Names{I}{3} = '   PCA-90% - SIMPLE';
group_diff_Names{I}{4} = '   PCA-99% - SIMPLE';

Rand_ve=[];
Rand_ve=[Rand_ve;variance_explained_Rand_i{I}{end}'];
Rand_ve=[Rand_ve;variance_explained_Rand_SIMPLE{I}];
Rand_ve=[Rand_ve;variance_explained_Rand_PCA{I}];
Rand_ve=[Rand_ve;variance_explained_Rand_PCA90{I}];
Rand_ve=[Rand_ve;variance_explained_Rand_PCA99{I}];

num_conf_labels={};
num_conf_labels{1} = [num2str(size(conf,2))];
num_conf_labels{2} = [num2str(size(conf_Simple,2))];
num_conf_labels{3} = [num2str(size(conf_PCA,2))];
num_conf_labels{4} = [num2str(size(conf_PCA90,2))];
num_conf_labels{5} = [num2str(size(conf_PCA99,2))];

final_num_conf_labels = {};
for i=1:length(num_conf_labels)
    final_num_conf_labels = [final_num_conf_labels, num_conf_labels{i}, ...
                             num_conf_labels{i}];
end  

final_ve  = [];
final_group_names = {};

for i=1:size(Rand_ve,1)
    final_ve  = [final_ve;  ve(i,:) ; Rand_ve(i,:)];
    final_group_names = [final_group_names, ...
      strrep(group_Names{I}{i},'_', ' '), ['RAND ' num_conf_labels{i}]];
end

confGroupsFileName = ['TXT_DATA/GEN/GLOBAL_conf_groups.txt'];

fileID=fopen(confGroupsFileName,'w');

for i=1:length(num_conf_labels)
    fprintf(fileID,'%s %s\n',group_Names{I}{i}, ...
                             num2str(num_conf_labels{i}));
end

fclose(fileID);

x = {ve};
y = {Rand_ve};
z = {final_ve};

lab = {'Variance Explained'};

plots_dir=strcat('figs/VE_GLOBAL/');
rm_make_dir(plots_dir)
txt_dir=['TXT_DATA/VE_GLOBAL/'];
make_dir(txt_dir);
html_dir=['HTML/VE_GLOBAL/'];
make_dir(html_dir);

% Iterate over ve (variance explained) 
i=1;
% Iterate over IDP grouping (ALL or independent)
for IDP_set=1:length(num_IDP_groups)
    % Iterate over IDP groups in each IDP grouping
    for j=1:num_IDP_groups{IDP_set}

        name_IDP_group = strrep(ind_IDP_groups{IDP_set}(j).name,...
                                                      '.txt', '');
        IDP_inds = ind_IDP_groups{IDP_set}(j).ind;                

        nameOut =strcat(txt_dir, name_IDP_group, '_', ...
                        strrep(lab{i},' ','_'),'.txt');
        nameRand=strcat(txt_dir, '/RAND_', name_IDP_group, '_',...
                        strrep(lab{i},' ','_'),'.txt');
        dlmwrite(nameOut,  x{i}(:,IDP_inds), 'delimiter', ' ');
        dlmwrite(nameRand, y{i}(:,IDP_inds), 'delimiter', ' ');

        IDPNamesFilename = ['TXT_DATA/GEN/GLOBAL_names_' ...
                                        name_IDP_group '.txt'];
        fileID=fopen(IDPNamesFilename,'w');

        for k=IDP_inds
            fprintf(fileID,'%s\n',IDPnames{k});
        end

        fclose(fileID);

        f = figure('visible', 'off', 'PaperOrientation','landscape',...
                  'Units','points','Position', [0,0,2400,1000]);

        % Code taken from github.com/bastibe/Violinplot-Matlab
        splitViolinplot(log10(z{i}(:,IDP_inds)'),...
              final_group_names, 'BoxWidth', 0.06, 'BoxColor', ...
              [0.5 0.5 0.5], 'EdgeColor', [0.2 0.2 0.2]);
        ti = title(strcat(strrep(name_IDP_group,{'_'},{' '}),...
                   {' '}, lab{i}));

        ax1=gca;
        set(gcf,'PaperPositionMode', 'auto', 'PaperOrientation', ...
                                                        'portrait');
        set(gca, 'FontSize',17, 'yscale', 'linear');
        set(gca, 'XGrid', 'off');
        set(gca, 'YGrid', 'on');
        xtickangle(90);
        ylabel('Log_1_0 % of variance explained');

        print('-dpng','-r70',strcat(plots_dir, name_IDP_group, ...
                             '_', strrep(lab{i},' ','_'),'.png'));

        htmlFilename = [html_dir name_IDP_group '_' ...
                        strrep(lab{i}, ' ','_') '.html'] ;

        [s, c] = system(['scripts/script_02_07_ve.py ' nameOut ...
                         ' ' nameRand ' ' confGroupsFileName ...
                         ' ' IDPNamesFilename ' ' htmlFilename ...
                         ' --apply_log']);
                     
        f = figure('visible', 'off', 'PaperOrientation','landscape',...
                  'Units','points','Position', [0,0,1400,750]);

        % Code taken from github.com/bastibe/Violinplot-Matlab
        if strcmp(name_IDP_group, 'T2_FLAIR') %Special case
            splitViolinplot(diff_ve(:,IDP_inds)', group_diff_Names{i}, ...
                    'BoxWidth', 0.06, 'BoxColor', [0.5 0.5 0.5], ...
                    'EdgeColor', [0.2 0.2 0.2]);            
        else
            violinplot(diff_ve(:,IDP_inds)', group_diff_Names{i},  ...
                   'BoxWidth', 0.02, 'BoxColor', [0.5 0.5 0.5],  ...
                   'EdgeColor', [0.2 0.2 0.2]);
        end
        ti = title(strcat('Difference of VE of', {' '} , ...
                   strrep(name_IDP_group,{'_'},{' '}),...
                   'IDPs between SIMPLE and other unconfoundins'));
               
        ylim([-20, 20]);
        set(gcf,'PaperPositionMode', 'auto', 'PaperOrientation', ...
                                                        'portrait');
        set(gca, 'FontSize',17, 'yscale', 'linear');
        set(gca, 'XGrid', 'off');
        set(gca, 'YGrid', 'on');
        set(gca, 'GridColor', [0, 0, 0]);
        set(gca, 'GridAlpha', 0.6);
        
        xtickangle(90);
        ylabel('Difference of IDPs VE');

        set(gca, 'LooseInset', get(gca, 'TightInset'));
        print('-dpng','-r70',strcat( plots_dir, 'diff_', name_IDP_group, ...
                             '_', strrep(lab{i},' ','_'),'.png'));
    end
end

my_log(fm, 'End');
