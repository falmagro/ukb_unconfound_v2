%
%   Authors: Fidel Alfaro-Almagro, Stephen M. Smith, Tom Nichols, & Paul McCarthy
%
%   Copyright 2017 University of Oxford
%
%   Licensed under the Apache License, Version 2.0 (the "License");
%   you may not use this file except in compliance with the License.
%   You may obtain a copy of the License at
%
%    http://www.apache.org/licenses/LICENSE-2.0
%
%   Unless required by applicable law or agreed to in writing, software
%   distributed under the License is distributed on an "AS IS" BASIS,
%   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%   See the License for the specific language governing permissions and
%   limitations under the License.
%
clear all;
addpath(genpath('common_matlab/'));
load 'workspaces/ws_02/confounds_generated.mat'

fm = (split(mfilename('fullpath'),'/')); 
fm = fm{end};

make_dir('figs/NAT/');

% Restricting the confounds to check to the ones (in Site 1) 
% belonging to the groups AGE, HEAD SIZE and TABLE POSITION.
best_groups = [1 4 23 19 21];

indi = [];

for i = best_groups
   for j = ind_conf_groups{1}{i}
      if strfind(names{j}, '_Site_1')
        indi = [indi j];
      end
   end
end

num_IDPs = size(subset_IDPs_i,2);
subset_IDPs_i_deconf = nets_unconfound2(subset_IDPs_i, conf);

cont=0;
for k = indi
    cont = cont + 1;
    my_log(fm, ['Loop conf: ' num2str(cont) ' of ' num2str(length(indi))]);
    tmpVar=[];

    for j=1:num_IDPs

      IDP_deconf=subset_IDPs_i_deconf(indSite{1},j); 
      IDP=subset_IDPs_i(indSite{1},j); 

      A=nets_demean(conf(indSite{1},k));

      subj_no_nan = ~isnan(IDP_deconf);

      IDP_deconf=IDP_deconf(subj_no_nan);
      IDP=IDP(subj_no_nan);
      A = A(subj_no_nan);

      N = length(A); 

      AA = nets_demean(A.^2);
      J = IDP_deconf;
      X = [A AA];
      beta=pinv(X)*J;
      % First pass at estimating I, ignoring interaction term cAI
      I1=J-X*beta;    
      I1_rand = I1;
      beta_rand = beta;
      % Iterate 20 times, including interaction terms
      for i = 1:20    
        AI=nets_demean(A.*I1);
        X=[A AA AI];
        beta=pinv(X)*J;
        I2=J-X*beta;
        I1=(I1+I2)/2;  % only do fractional update so we don't oscillate
      end
      
      for i = 1:20    % Iterate 20 times, including interaction terms
        AI=nets_demean(A.*randn(size(I1_rand)));
        X=[A AA AI];
        beta_rand=pinv(X)*J;
        I2=J-X*beta_rand;
        % Only do fractional update so we don't oscillate
        I1_rand=(I1_rand+I2)/2;  
      end
      
      val1 = 100 * ((std(beta(1)*X(:,1)) / nanstd(IDP)).^2);
      val2 = 100 * ((std(beta(2)*X(:,2)) / nanstd(IDP)).^2);
      val3 = 100 * ((std(beta(3)*X(:,3)) / nanstd(IDP)).^2);
      val4 = 100 * ((std(beta_rand(1)*X(:,1)) / nanstd(IDP)).^2);
      val5 = 100 * ((std(beta_rand(2)*X(:,2)) / nanstd(IDP)).^2);
      val6 = 100 * ((std(beta_rand(3)*X(:,3)) / nanstd(IDP)).^2);
      
      AA = nets_unconfound2(A.^2, A);
      J = nets_demean(IDP);
      X = nets_demean([A AA]);
      beta=pinv(X)*J;
     
      val7 = 100 * ((std(beta(1)*X(:,1)) / nanstd(IDP)).^2);
      val8 = 100 * ((std(beta(2)*X(:,2)) / nanstd(IDP)).^2);
      
      tmpVar=[tmpVar; corr(I1,IDP_deconf) val1 val2 val3 val4 val5 val6 val7 val8];
      
    end
    f = figure('Visible', 'off', 'Position', [0,0,1000, 500]);
    
    % boxplot of how well estimated I correlates with true I
    s1 = subplot(1,4,1);
    bo = boxplot(tmpVar(:,1), 'labels', {'Correlations'});                 
    
    % scatterplot of estimated parameter a vs true a
    s2 = subplot(1,4,[2,3,4]);
    X_Axis =[-12:0.15:2];
    pl = plot(X_Axis, hist(log10([tmpVar(:,8) tmpVar(:,9) tmpVar(:,4) ...
        tmpVar(:,7)]), X_Axis), 'LineWidth', 2); 
    le = legend(pl, 'Linear term', 'Quadratic term', 'Non-additive term', ...
        'Random variable', 'Location', 'northwest');
    xlabel('% VE')
    le.FontSize = 13;
    xlim([-7,2])

    finalLabs={};
    for i=1: length(s2.XTickLabel)
        finalLabs=[finalLabs; num2str(10^(s2.XTick(i)))];
    end
    s2.XTickLabel = finalLabs;
    
    suptitle(['Confound: ' strrep(names{k},'_', ' ')]);
    set(gca, 'LooseInset', get(gca, 'TightInset'));
    
    print('-dpng','-r70',strcat('figs/NAT/', num2str(k), '_',names{k},...
                                '.png'));         
    % TODO: Improve this
    if cont > 9
        exit
    end
end
