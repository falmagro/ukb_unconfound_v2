%
%   Authors: Fidel Alfaro-Almagro, Stephen M. Smith, Tom Nichols, & Paul McCarthy
%
%   Copyright 2017 University of Oxford
%
%   Licensed under the Apache License, Version 2.0 (the "License");
%   you may not use this file except in compliance with the License.
%   You may obtain a copy of the License at
%
%    http://www.apache.org/licenses/LICENSE-2.0
%
%   Unless required by applicable law or agreed to in writing, software
%   distributed under the License is distributed on an "AS IS" BASIS,
%   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%   See the License for the specific language governing permissions and
%   limitations under the License.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
% Loads the raw confounds generated in the previous stage and          %
% then generates non-linear version of the non-categorical confounds.  %
% Generated confounds are stored in workspaces/confounds_generated.mat %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clear;

addpath(genpath('common_matlab/'));
addpath('functions/');

my_log('', 'Before load');

load('workspaces/ws_01/nonlin_confounds_generated.mat');
load('workspaces/ws_01/conf_nonlin.mat');
load('workspaces/ws_01/names_nonlin.mat');
finalFile = 'workspaces/ws_01/ct_confounds_generated.mat';

rm_file(finalFile);
rm_make_dir('workspaces/ws_01/veu_ct/');

fm = (split(mfilename('fullpath'),'/')); 
fm = fm{end};
my_log(fm, 'After load');

conf       = [confAge confSex confAgeSex confHeadSize confSite confBatch ...
              confCMRR confProtocol confServicePack confScanRamp ...
              confScanColdHead confScanHeadCoil confScanMisc ...
              confFlippedSWI confFST2 confNewEddy confScaling confTE ...
              confStructHeadMotion confDVARS confHeadMotion confHeadMotionST ...
              confTablePos confEddyQC conf_nonlin];
          
names = [namesAge;namesSex;namesAgeSex;namesHeadSize;namesSite;namesBatch;...
         namesCMRR;namesProtocol;namesServicePack;namesScanRamp;...
         namesScanColdHead;namesScanHeadCoil;namesScanMisc;...
         namesFlippedSWI;namesFST2;namesNewEddy;namesScaling;namesTE;...
         namesStructHeadMotion;namesDVARS;namesHeadMotion;namesHeadMotionST;...
         namesTablePos;namesEddyQC;names_nonlin]; 

conf_no_crossed = conf;

indPairs=nchoosek(1:size(conf_no_crossed,2),2);

num_ct=size(indPairs,1);

fileID = fopen('tables/summary.txt', 'a');
fprintf(fileID, 'Initial number of CTs: %s\n', num2str(num_ct));
fclose(fileID);

names_ct={};

for i=1:num_ct
   if mod(i,1000) == 0
        my_log(fm, ['Loop names: ' num2str(i) ' of ' num2str(num_ct)]);
   end
   names_ct=[names_ct;strcat(names{indPairs(i,1)},'__x__',names{indPairs(i,2)})];
end    


exclude_ind_ct = [];
for i=1:num_ct
    [a,b] = regexp(names_ct{i}, 'Site_[0-9]');
    
    % All crossed terms should have 2 sites. If they don't there is a
    % problem
    if size(a,2) ~= 2
        my_log(fm, ['Issue with CT ' names_ct{1}]);
        %break;
    end
    S1=names_ct{i}(a(1):b(1));
    S2=names_ct{i}(a(2):b(2));
    if ~strcmp(S1,S2)
        exclude_ind_ct = [exclude_ind_ct i];
    end
end

names_ct(exclude_ind_ct)   = [];
indPairs(exclude_ind_ct,:) = [];
num_ct=size(indPairs,1);

fileID = fopen('tables/summary.txt', 'a');
fprintf(fileID, 'Number of CTs removing mix of Sites: %s\n', num2str(num_ct));
fclose(fileID);


ct = zeros(N,num_ct);

for i=1:num_ct
    if mod(i,1000) == 0
        my_log(fm, ['Loop Values: ' num2str(i) ' of ' num2str(num_ct)]);
    end
    ct(:,i) = (conf_no_crossed(:,indPairs(i,1)) .* ...
                          conf_no_crossed(:,indPairs(i,2)));
end

my_log(fm, 'Before unconfounding');
conf_ct = nets_unconfound2(ct, conf_no_crossed);
my_log(fm, 'After unconfounding');

clear ct;
ind_no_ct = 1:size(conf,2);

conf       = [confAge confSex confAgeSex confHeadSize confSite confBatch ...
              confCMRR confProtocol confServicePack confScanRamp ...
              confScanColdHead confScanHeadCoil confScanMisc ...
              confFlippedSWI confFST2 confNewEddy confScaling confTE ...
              confStructHeadMotion confDVARS confHeadMotion confHeadMotionST ...
              confTablePos confEddyQC conf_nonlin conf_ct];
          
conf_Group = {confAge,confSex,confAgeSex,confHeadSize,confSite,confBatch,...
              confCMRR,confProtocol,confServicePack,confScanRamp,...
              confScanColdHead,confScanHeadCoil,confScanMisc,...
              confFlippedSWI,confFST2,confNewEddy,confScaling,confTE,...
              confStructHeadMotion,confDVARS,confHeadMotion,confHeadMotionST,...
              confTablePos,confEddyQC,conf_nonlin,conf_ct};        
          
names = [namesAge;namesSex;namesAgeSex;namesHeadSize;namesSite;namesBatch;...
         namesCMRR;namesProtocol;namesServicePack;namesScanRamp;...
         namesScanColdHead;namesScanHeadCoil;namesScanMisc;...
         namesFlippedSWI;namesFST2;namesNewEddy;namesScaling;namesTE;...
         namesStructHeadMotion;namesDVARS;namesHeadMotion;namesHeadMotionST;...
         namesTablePos;namesEddyQC;names_nonlin;names_ct];        

group_Names = {{}};
group_Names{1} = {'AGE', 'SEX', 'AGE_SEX', 'HEAD_SIZE', 'SITE', 'BATCH', ...
                  'CMRR', 'PROTOCOL', 'SERVICE_PACK', 'SCAN_RAMP', ...
                  'SCAN_COLD_HEAD', 'SCAN_HEAD_COIL', 'SCAN_MISC', ...
                  'FLIPPED_SWI', 'FS_T2', 'NEW_EDDY', 'SCALING', 'TE', ...
                  'STRUCT_MOTION', 'DVARS', 'HEAD_MOTION', 'HEAD_MOTION_ST', ...
                  'TABLE_POS', 'EDDY_QC', 'NON_LIN' 'CROSSED_TERMS'};   

num_conf      = {};
num_groups    = [];
num_conf{1}   = size(conf,2);
num_groups(1) = length(conf_Group);


%This will allow for multiple confound groupings.
ind_conf_groups    = {{}};

previous = 0;
for i = 1:num_groups(1)
    num_elements=size(conf_Group{i},2);
    ind_conf_groups{1}{i} = [previous + 1 : previous + num_elements];
    previous = previous + num_elements;
end

clear conf_Group;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Create the indices in the conf for each element in conf_Group %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% ind_conf_groups_Rand = {};
% previous = 0;
% for i = 1:num_groups_Rand
%     num_elements=size(conf_Group_Rand{i},2);
%     ind_conf_groups_Rand{i} = [previous + 1 : previous + num_elements];
%     previous = previous + num_elements;
% end

IDP_group_dir={};
IDP_group_dir{1} = '../data/GROUPS_IDPs_7_groups/';
IDP_group_dir{2} = '../data/GROUPS_IDPs_all/';

for i=1:length(IDP_group_dir)
    IDP_group_files{i}= dir(strcat(IDP_group_dir{i}, '*.txt'));
    num_IDP_groups{i} = length(IDP_group_files{i});
    ind_IDP_groups{i} = [];
end

for i=1:length(IDP_group_dir)
    for j = 1:num_IDP_groups{i}
        fileID = fopen(strcat(IDP_group_dir{i}, ...
                       IDP_group_files{i}(j).name),'r');    
        tmp=fscanf(fileID, '%i');
        ind_IDP_groups{i}(j).name = IDP_group_files{i}(j).name;
        ind_IDP_groups{i}(j).ind  = tmp;
    end
end

%%%%%%%%%%%%
% CLEANING %
%%%%%%%%%%%%
clear all_PCA;
clear ESM;
clear HE;
clear AbyB;
clear BbyA;
clear ans;
clear Princ_Components_st;
clear indSite_st;
clear cont;
clear max_VE;
clear numDateComponents;
clear previous;
clear denominator;
clear gausskernel;
clear headMotion;
clear i;
clear j;
clear k;
clear numerator;
clear princ_components_cell;
clear princ_components_cell_st;
clear total_princ_components_cell;
clear loaded_smoothed_subset_IDPs_st;
clear subset_IDPs_i_st;
clear subset_IDPs_i_deconf_st;
clear total_princ_components_cell_st;
clear numTempComponents;
clear unsorted_index_sortedTime;
clear unsorted_index_sortedDate;
clear new_index_sortedTime;
clear new_index_sortedDate;
clear index_sortedDate;
clear index_sortedTime;
clear sortedTime;
clear scan_date;
clear scan_date_cont;
clear sigma;
clear tmp;
clear i;
clear j;
clear I;

ind_ct   = size(ind_no_ct,2) + 1:size(conf,2);
ind_conf = 1:size(conf,2);

num_ct    = size(ind_ct,2);
num_no_ct = size(ind_no_ct,2);

my_log(fm, 'Before unconfounding');
IDP_i_deconf  = nets_unconfound2(subset_IDPs_i, conf(:,ind_no_ct));
my_log(fm, 'After unconfounding');

save('workspaces/ws_01/IDPs_i.mat', '-v7.3','subset_IDPs_i');
save('workspaces/ws_01/IDPs_i_ct_deconf.mat', '-v7.3', 'IDP_i_deconf');

clear IDP_i_deconf;
clear IDP_i;
clear Princ_Components;
clear subset_IDPs_i_deconf;

num_div = 10;
displ=round(num_ct/num_div);

for i = 1:num_div
    if i == num_div
        tmp = conf_ct(:,((i-1)*displ)+1:end);
    else
        tmp = conf_ct(:,((i-1)*displ)+1:displ*i);
    end
    
    name_fil = ['workspaces/ws_01/ct_q' num2str(i) '.mat'];
    save(char(name_fil),'-v7.3','tmp');
end

save('workspaces/ws_01/conf.mat','-v7.3','conf');
save('workspaces/ws_01/conf_all_ct.mat','-v7.3','conf_ct');

clear displ conf_ct_div conf_ct conf subset_IDPs_i tmp;

save(finalFile, '-v7.3');
my_log(fm, 'End');
