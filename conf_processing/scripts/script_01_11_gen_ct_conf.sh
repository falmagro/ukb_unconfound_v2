#!/bin/bash

if [ ! "$1" == "" ] ; then
    waitjob=" -j $1 "
else
    waitjob=""
fi

res=""

rm    -rf logs/logs_01_12_gen_ct_conf/
mkdir  -p logs/logs_01_12_gen_ct_conf/

rm     -f workspaces/ws_01/veu_ct/*

res=`      fsl_sub -l logs/logs_01_12_gen_ct_conf/ $waitjob -q     short.q -t jobs/jobs_01_12_gen_ct_conf_long.txt`
res="$res,`fsl_sub -l logs/logs_01_12_gen_ct_conf/ $waitjob -q veryshort.q -t jobs/jobs_01_12_gen_ct_conf_short.txt`"

echo $res 
