#!/bin/env python
#
# Authors: Fidel Alfaro-Almagro, Stephen M. Smith, Tom Nichols, & Paul McCarthy
#
# Copyright 2017 University of Oxford
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

import sys
import glob
import argparse
import datetime
import numpy as np
import plotly
import plotly.graph_objs as go

class MyParser(argparse.ArgumentParser):
    def error(self, message):
        sys.stderr.write('error: %s\n' % message)
        self.print_help()
        sys.exit(2)

class Usage(Exception):
    def __init__(self, msg):
        self.msg = msg

def main():

    parser = MyParser(description='UVE thresholding tool')
    parser.add_argument("uve_data", help='Data with the UVE (conf x IDPs)')
    parser.add_argument("thr_data", help='Data with the 2 thresholds')
    parser.add_argument("IDPs_labels", help='Labels for the IDPs')
    parser.add_argument("confs_labels", help='Labels for the confs')
    parser.add_argument("IDP_groups_Dir", default='./', \
                                     help='Directory with the IDP groups')
    parser.add_argument("inputDir", nargs='?', default='./', \
                                     help='Input Directory')
    parser.add_argument("outputDir", nargs='?', default='./', \
                                     help='Output Directory')
    parser.add_argument("title", nargs='?', default='./', \
                                     help='Name of the type of confounds')
    argsa = parser.parse_args()

    uve_data       = argsa.uve_data
    thr_data       = argsa.thr_data
    IDPs_labels    = argsa.IDPs_labels
    confs_labels   = argsa.confs_labels
    IDP_groups_Dir = argsa.IDP_groups_Dir
    inputDir       = argsa.inputDir
    outputDir      = argsa.outputDir
    plot_title     = argsa.title

    print('Before loading data - ' + str(datetime.datetime.now()))

    uve = np.loadtxt(inputDir + '/' + uve_data + '.txt')
    thr = np.loadtxt(inputDir + '/' + thr_data + '.txt')
    f = open(inputDir + '/' + IDPs_labels + '.txt', 'r')
    IDPs_labels = [x.strip() for x in f.readlines()]
    f.close()
    f = open(inputDir + '/' + confs_labels + '.txt', 'r')
    confs_labels = [x.strip() for x in f.readlines()]
    f.close()

    print('After loading data. Before creating data to plot - ' + \
          str(datetime.datetime.now()))

    num_confs = uve.shape[0]
    num_IDPs  = uve.shape[1]

    IDP_groups = glob.glob(IDP_groups_Dir + '/*.txt')
    IDP_groups = [x.replace(IDP_groups_Dir, '') for x in IDP_groups]
    IDP_groups = [x.replace('.txt', '').replace('/', '') for x in IDP_groups]
    IDP_groups.sort()

    IDP_inds = {}
    for IDP_group in IDP_groups:
        vals = np.loadtxt(IDP_groups_Dir + '/' + IDP_group + '.txt')
        # Ugly, but I need to do this because np.loadtxt does
        # not return an array if a file has just one element
        if vals.size == 1:
            vals = [vals]
        IDP_inds[IDP_group] = [int(x) - 1 for x in vals]

    uve_means   = np.nanmean(uve, axis=1)
    uve_max     = np.nanmax(uve, axis=1)
    uve_max_ind = np.nanargmax(uve, axis=1)

    finalT_mean = []
    for i in range(num_confs):
        finalT_mean.append('Confound:' + confs_labels[i])

    finalT_max = []
    for i in range(num_confs):
        finalT_max.append('Confound:' + confs_labels[i] + '<BR>IDP: ' +
                           IDPs_labels[uve_max_ind[i]])

    data_mnht_X = {}
    data_mnht_Y = {}
    data_mnht_T = {}

    for IDP_group in IDP_groups:
        data_mnht_X[IDP_group] = []
        data_mnht_Y[IDP_group] = []
        data_mnht_T[IDP_group] = []
        for j in IDP_inds[IDP_group]:
            uves_for_IDP_group = uve[:,j]
            sorted_inds = np.argsort(uves_for_IDP_group)
            min_num = np.min([200, len(sorted_inds)])
            final_sorted_inds = sorted_inds[-min_num:len(sorted_inds)]            

            for i in final_sorted_inds:

                data_mnht_Y[IDP_group].append(uve[i,j])
                data_mnht_X[IDP_group].append(j)
                data_mnht_T[IDP_group].append('Confound:' + confs_labels[i] + \
                                              '<BR>IDP: ' + IDPs_labels[j] + \
                                              '<BR>% UVE: ' + str(uve[i,j]))

    # Creating the data to plot
    data_mean = []
    data_mean.append(go.Scattergl(x=np.arange(0,num_confs),
                                  y=uve_means,
                                  text=finalT_mean,
                                  mode='lines', name='Mean',
                                  line=dict(width=2, color='blue'))
                         )
    data_mean.append(go.Scattergl(x=[0,num_confs], y=[thr[0], thr[0]],
                                  mode='lines', name='Threshold',
                                  line=dict(width=2, color='red')
                                  )
                     )

    data_max = []
    data_max.append(go.Scattergl(x=np.arange(0,num_confs),
                                  y=uve_max,
                                  text=finalT_max,
                                  mode='lines', name='Max',
                                  line=dict(width=2, color='blue'))
                         )
    data_max.append(go.Scattergl(x=[0,num_confs], y=[thr[1], thr[1]],
                                  mode='lines', name='Threshold',
                                  line=dict(width=2, color='red')
                                  )
                     )

    data_mnht = []
    for IDP_group in IDP_groups:
        data_mnht.append(go.Scattergl(x=data_mnht_X[IDP_group],
                                      y=data_mnht_Y[IDP_group],
                                      text=data_mnht_T[IDP_group],
                                      mode='markers', name=IDP_group,
                                      marker=dict(
                                        line=dict(width=0),
                                        color=IDP_groups.index(IDP_group),
                                        size=4)
                                      )
                         ) 

    data_mnht.append(go.Scattergl(x=[0,num_IDPs], y=[thr[1], thr[1]],
                                  mode='lines', name='Threshold',
                                  line=dict(width=2, color='black ')
                                  )
                     )



    title_mean = 'Mean (across IDPs) % of Unique Variance Explained per ' + \
                 plot_title + ' confound - Threshold: ' + str(thr[0])
    title_max = 'Max (across IDPs) % of Unique Variance Explained per ' + \
                 plot_title + ' confound - Threshold: ' + str(thr[1])
    title_mnht = '% of UVE of each IDP (' + str(num_IDPs) + ') by each ' + \
                 plot_title + ' confound (' + str(num_confs) + \
                 ') - Threshold: ' + str(thr[1])


    print('After creating data to plot. Before plotting - ' +
           str(datetime.datetime.now()))

    # Actual plotting
    plotly.offline.plot({
        "data": data_mean,
        "layout": go.Layout(title=go.layout.Title(
                            text=title_mean.replace('_', ' '),
                            y=0.97),
                            xaxis=go.layout.XAxis(
                                title=go.layout.xaxis.Title(
                                    text='Confounds',
                                    font=dict(size=20)
                                ),
                                ticks='outside', automargin=True
                            ),
                            yaxis=go.layout.YAxis(
                                title=go.layout.yaxis.Title(
                                    text='Mean (across IDPs) UVE by each confound', 
                                    font=dict(size=20)
                                ),
                                # The range expands sligtly the real margins.
                                ticks='outside'
                            ),
                            hovermode='closest'
                    )
                },
                auto_open=False,
                filename=outputDir + '/MEAN_' + plot_title.upper() + \
                         '.html')

    plotly.offline.plot({
        "data": data_max,
        "layout": go.Layout(title=go.layout.Title(
                            text=title_max.replace('_', ' '),
                            y=0.97),
                            xaxis=go.layout.XAxis(
                                title=go.layout.xaxis.Title(
                                    text='Confounds',
                                    font=dict(size=20)
                                ),
                                ticks='outside', automargin=True
                            ),
                            yaxis=go.layout.YAxis(
                                title=go.layout.yaxis.Title(
                                    text='Max (across IDPs) UVE by each confound', 
                                    font=dict(size=20)
                                ),
                                # The range expands sligtly the real margins.
                                ticks='outside'
                            ),
                            hovermode='closest'
                    )
                },
                auto_open=False,
                filename=outputDir + '/MAX_' + plot_title.upper() + \
                         '.html')

    # Actual plotting
    plotly.offline.plot({
        "data": data_mnht,
        "layout": go.Layout(title=go.layout.Title(
                            text=title_mnht.replace('_', ' '),
                            y=0.97),
                            xaxis=go.layout.XAxis(
                                title=go.layout.xaxis.Title(
                                    text='IDPs',
                                    font=dict(size=20)
                                ),
                                range=[0, num_IDPs]
                            ),
                            yaxis=go.layout.YAxis(
                                title=go.layout.yaxis.Title(
                                    text='% Unique Variance Explained', font=dict(size=20)
                                ),
                                ticks='outside'
                            ),
                            hovermode='closest',
                    )
                },
                auto_open=False,
                filename=outputDir + '/MNHT_' + plot_title.upper() + '.html')



    print('After plotting. End - ' + str(datetime.datetime.now()))

if __name__ == "__main__":
    main()

