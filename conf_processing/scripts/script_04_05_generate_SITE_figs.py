#!/bin/env python
#
# Authors: Fidel Alfaro-Almagro, Stephen M. Smith, Tom Nichols, & Paul McCarthy
#
# Copyright 2017 University of Oxford
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

import plotly
import numpy as np
import plotly.plotly as py
import sys,argparse,os.path

class MyParser(argparse.ArgumentParser):
    def error(self, message):
        sys.stderr.write('error: %s\n' % message)
        self.print_help()
        sys.exit(2)

class Usage(Exception):
    def __init__(self, msg):
        self.msg = msg

def main():

    parser = MyParser(description='Correlation matrix generation tool')
    parser.add_argument("dataFile", help='Data to generate violin plots')
    parser.add_argument("namesIDPsFile", help='Names for the IDPs')
    parser.add_argument("outputFile", nargs='?', default='./corr_matrix', \
                                        help='Output file')

    argsa = parser.parse_args()

    dataFile      = argsa.dataFile
    namesIDPsFile = argsa.namesIDPsFile
    outputFile    = argsa.outputFile

    if not outputFile.endswith('.html'):
        outputFile = outputFile + '.html'

    dataD = np.loadtxt(dataFile)

    if dataD.ndim == 1:
        dataD = dataD.reshape(dataD.shape[0],1)
    
    namesConf = ['% VE by SITE', '% UVE by SITE']
    with open(namesIDPsFile, 'r') as f:
        namesIDPs = f.readlines()
    namesIDPs = [x.strip() for x in namesIDPs]

    colorbar_title = 'Log10 of % of Variance Explained'
    yrange=[-4, 2]

    final_data1=[]

    for i in range(dataD.shape[1]):
        text = [str.format('{:2.3f}', np.power(10,dataD[j,i])) + '% of ' + \
                namesIDPs[j] + ' explained by Site confounds.' \
                for j in range(len(namesIDPs))]
        conf = {
            "type": 'violin',
            "x0": namesConf[i],
            "y": dataD[:,i],
            "offsetgroup": str((i*2)),
            "scalegroup": namesConf[i],
            "text" : text,
            "name": namesConf[i],

            "box": {
                "visible" : True,
                "fillcolor": '#FFFFFF',
                "line": {
                    "color": '#000000',
                    "width": 1
                }
            },
            "points": 'all',
            "pointpos": 0,
            "jitter": 0,
            "scalemode": 'count',
            "meanline": {
                "visible": True
            },
            "marker": {
                "size" : 0.1,
                "line": {
                    "width": 0
                }
            },
            "showlegend": False
        }
        final_data1.append(conf)
    fig = {
        "data": final_data1,
        "layout" : {
            "title": "% VE and % UVE for all IDPs for SITE",
            "margin": {
                "b" : 200
            },
            "xaxis": {
                "showgrid":True,
                "showline":True,
                "tickangle": 270
            },
            "yaxis": {
                "title" : colorbar_title,

                "zeroline": False,
            },
            "hovermode": "closest"
        }
    }
    plotly.offline.plot(fig, filename=outputFile)

if __name__ == "__main__":
    main()
