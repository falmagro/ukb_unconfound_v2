function gpuExists = check_GPU();
    try
        gpuArray(1);
        gpuExists=true;
    catch
        gpuExists=false;
    end
end
