function [finalConfs, finalNames] = duplicateDemedianNormBySite(conf_name, ...
                                   ALL_IDs, subjectIndicesBySite, dataName)
    
    namesFileName  = strcat('../data/NAMES_confounds/', conf_name, '.txt');
    baseNames      = getNames(namesFileName);
    finalNames     = [];

    valuesFileName = strcat('../data/', dataName ,'_data/ID_', conf_name,...
                                                                   '.txt');
    values         = getValues(valuesFileName, ALL_IDs);
    
    numVars  = size(values,2);
    numSites = length(subjectIndicesBySite);
    
    % Generate the names
    for i = 1 : numSites
        for j = 1:length(baseNames)
            newName    = strcat(baseNames(j), '_Site_', num2str(i));
            finalNames = [finalNames ; newName];
        end
    end
    
    % Demedian globally each column
    for i = 1 : numVars
        values(:,i) = values(:,i) - nanmedian(values(:,i));
        madV = mad(values(:,i),1) * 1.48;
        if madV < eps
            madV = nanstd(values(:,i));
        end
        values(:,i) = values(:,i) / madV;
    end
   
    % Split the colums: 1 per site. 0-padding by default.
    finalConfs = zeros(length(ALL_IDs),numSites*numVars);

    % For each Site
    for i = 1:numSites
        % For each variable (column) that is received in "values"
        for j = 1:numVars
            V = values(subjectIndicesBySite{i},j);
            
            medTP = nanmedian(V);
            
            V(find(V > 8)) =  NaN;
            V(find(V <-8)) =  NaN;

            V(isnan(V)) = medTP;

            V = nets_normalise(V);
            finalConfs(subjectIndicesBySite{i}, (numVars*(i-1) + j)) = V;
        end 
    end
end
