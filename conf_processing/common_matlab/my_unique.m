% Taken from www.mathworsk.com
% The default "unique" function treats every NaN as a different number.
% This function treats all NaNs as if they were the same number.
function y = my_unique(x)
  y = unique(x);
  if any(isnan(y))
    y(isnan(y)) = []; % remove all nans
    y(end+1) = NaN; % add the unique one.
  end
end
