function [finalConfs, finalNames] = getData(conf_name, ALL_IDs, dataName)

    namesFileName = strcat('../data/NAMES_confounds/', conf_name, '.txt');
    finalNames = getNames(namesFileName);

    file_to_load=strcat('../data/',dataName,'_data/ID_', conf_name, '.txt');

    finalConfs = getValues(file_to_load, ALL_IDs);