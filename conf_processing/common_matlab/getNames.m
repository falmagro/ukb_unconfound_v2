function names = getNames(fileName)

    fid = fopen(fileName);
    tmp = textscan(fid, '%s', 'Delimiter', '\n');
    names = tmp{:};
end

