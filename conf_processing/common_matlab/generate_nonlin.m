function [conf_nonlin, names_nonlin] = generate_nonlin(conf_group, ...
                                        conf_group_names, all_raw_conf, indSite)

    conf_squared = zeros(size(conf_group));
    conf_inormal = zeros(size(conf_group));
    conf_inormal_squared = zeros(size(conf_group));

    for i=1:size(conf_group, 2)
        numSite = -1;
        for numSites = 1:length(indSite)
            if ~isempty(strfind(conf_group_names{i}, ...
                                strcat('Site_', num2str(numSites))))
                numSite = numSites;
            end
        end
        j=indSite{numSite};

        conf_squared(j,i) = nets_normalise(conf_group(j,i).^2);
        conf_squared(isnan(conf_squared)) = 0;
    
        conf_inormal(j,i)         = nets_inormal(conf_group(j,i));
        conf_inormal_squared(j,i) = nets_inormal(conf_squared(j,i));
    
        conf_squared(j,i) = nets_unconfound2(conf_squared(j,i), ...
                                                        all_raw_conf(j,:));
        conf_inormal(j,i) = nets_unconfound2(conf_inormal(j,i), ...
                                                        all_raw_conf(j,:));
        conf_inormal_squared(j,i) = nets_unconfound2(conf_inormal_squared(j,i),...
                                                       all_raw_conf(j,:));
    end

    conf_nonlin = [conf_squared conf_inormal conf_inormal_squared];

    names_nonlin = {};     
    
    suffix = {'_squared', '_inormal', '_squared_inormal'};
    for i = 1:length(suffix)
        for j = 1 : length(conf_group_names)
            newName      = [conf_group_names{j}, suffix{i}];
            names_nonlin = [names_nonlin ; newName];
        end
    end        
