function my_BA_plot(P_S1, R_S1, P_S2, R_S2, N, ...
                    cols_names, rows_names, group_name, ...
                    cols_label, rows_label, shape_matrix, ...
                    general_name, PNG_dir, TXT_dir, HTML_dir,...
                    S1_text, S2_text, general_text)
                   
    label = strrep(group_name, '_', ' ');

    X = (P_S1 + P_S2) / 2;
    Y = P_S1 - P_S2;
    
    BA = [X, Y];

    Info = {};
    IDP_indices=zeros(size(P_S1));
    COGN_indices=zeros(size(P_S1));

    for i = 1:size(P_S1,1)
       [IDP_ind, COGN_ind]= ind2sub(shape_matrix, i);
       IDP_indices(i) = IDP_ind;
       COGN_indices(i) = COGN_ind;
       Info{i}=[cols_label ': '  cols_names{IDP_ind} '  -  ' rows_label ': ' rows_names{COGN_ind}];
    end

    dlmwrite([TXT_dir '/' general_name '_B_' group_name '.txt'], P_S1);
    dlmwrite([TXT_dir '/' general_name '_A_'  group_name '.txt'], P_S2);
    dlmwrite([TXT_dir '/' general_name '_X_'  group_name '.txt'], X);
    dlmwrite([TXT_dir '/' general_name '_Y_'  group_name '.txt'], Y);
    dlmwrite([TXT_dir '/' general_name '_RB_' group_name '.txt'], R_S1);
    dlmwrite([TXT_dir '/' general_name '_RA_' group_name '.txt'], R_S2);
    dlmwrite([TXT_dir '/' general_name '_N_'  group_name '.txt'], N);
    fileName=[TXT_dir '/' general_name '_labels_' group_name '.txt'];
    filePH=fopen(fileName,'w');
    fprintf(filePH,'%s\n', Info{:});
    fclose(filePH);

    f = figure('visible','off','PaperOrientation', 'landscape','Units', ...
               'points','Position', [0,0,1000,700]);
    dscatter(BA(:,1), BA(:,2)); hline = refline([0 0]);hline.Color = 'r'; 
    set(gcf,'PaperPositionMode', 'auto', 'PaperOrientation', 'portrait');
    set(gca, 'FontSize',20);
    xlabel('Average of B & A', 'FontSize', 30);
    ylabel('B - A', 'FontSize', 30); 
    title({['Bland-Altman plot for ' label],  general_text, ...
           ['B = ' S1_text], ['A = ' S2_text]}, 'FontSize', 20);
    print('-dpng','-r70',[PNG_dir '/BA_' group_name ...
                          '_' general_name '.png']);
                      
    command = ['./scripts/script_03_12_BA_plots.py ' group_name ...
               ' ' general_name ' '  TXT_dir ' ' HTML_dir '\n']    
           
    [s, c] = system(command);
           
end
