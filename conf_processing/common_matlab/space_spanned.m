function [AbyB,BbyA] = space_spanned(A,B);

grot=B*(pinv(B)*A);   AbyB=100*sum(grot(:).*grot(:))/sum(A(:).*A(:));

grot=A*(pinv(A)*B);   BbyA=100*sum(grot(:).*grot(:))/sum(B(:).*B(:));

