function confR = generate_RANDOM_confounds(N, nMax, nSim, nPCA90, nPCA99)

    % Random confounds (gaussian distribution)   
    confR.confR001=normrnd(0,1,N,1);
    confR.confR010=normrnd(0,1,N,10);
    confR.confR100=normrnd(0,1,N,100);
    confR.confRMAX=normrnd(0,1,N,nMax);
    confR.confRSIM=normrnd(0,1,N,nSim);
    confR.confRICA=normrnd(0,1,N,nSim);
    confR.confRPCA=normrnd(0,1,N,nSim);
    confR.confRPCA90=normrnd(0,1,N,nPCA90);
    confR.confRPCA99=normrnd(0,1,N,nPCA99);
    
    confR.namesR001={'Random001-001'};

    confR.namesR010={};
    for i = 1:10
        confR.namesR010{i,1}=strcat('Random010-', sprintf('%03d',i));     
    end

    confR.namesR100={};
    for i = 1:100
        confR.namesR100{i,1}=strcat('Random100-', sprintf('%03d',i));     
    end

    confR.namesRMAX={};
    for i = 1:nMax
        confR.namesRMAX{i,1}=strcat('RandomMAX-', sprintf('%04d',i));     
    end

    confR.namesRSIM={};
    for i = 1:nSim
        confR.namesRSIM{i,1}=strcat('RandomSIM-', sprintf('%03d',i));     
    end
    
    confR.namesRICA={};
    for i = 1:nSim
        confR.namesRICA{i,1}=strcat('RandomICA-', sprintf('%03d',i));     
    end
    
    confR.namesRPCA={};
    for i = 1:nSim
        confR.namesRPCA{i,1}=strcat('RandomPCA-', sprintf('%03d',i));     
    end
    
    confR.namesRPCA90={};
    for i = 1:nPCA90
        confR.namesRPCA90{i,1}=strcat('RandomPCA90-', sprintf('%03d',i));     
    end
    
    confR.namesRPCA99={};
    for i = 1:nPCA99
        confR.namesRPCA99{i,1}=strcat('RandomPCA99-', sprintf('%03d',i));     
    end
end
