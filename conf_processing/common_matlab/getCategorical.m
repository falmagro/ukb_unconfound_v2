function [finalConfs, finalNames] = getCategorical(conf_name, ALL_IDs, ...
                                                    dataName)

    namesFileName = strcat('../data/NAMES_confounds/', conf_name, '.txt');
    baseName = getNames(namesFileName);
    finalNames = {};

    file_to_load=strcat('../data/',dataName,'_data/ID_', conf_name, '.txt');

    values = getValues(file_to_load, ALL_IDs);
    numVars = size(values,2);
    
    final_num_cols=0;
    
    finalConfs=[];

    % For each variable
    for k = 1:numVars
    
        numNewCols = 0;
        diffValues = my_unique(values(:,k));
        diffValues = diffValues(~isnan(diffValues));

        countDiffValues = arrayfun(@(x)length(find(values(:,k) == x)), ...
                                         diffValues, 'Uniform', false);

        % Remove a value if there are very few subjects (less than 3). 
        % This case is very likely to be an error in the settings of the
        % DICOM header.
        diffValuesToRemove = [];

        for j = 1:length(diffValues)
            if countDiffValues{j} <= 2
                diffValuesToRemove = [diffValuesToRemove j];
            end 
        end

        diffValues(diffValuesToRemove) = [];                                        

        numDiffValues = length(diffValues);

        % If all elements have the same value, we do not generate a column. 
        if numDiffValues > 1
            numNewCols = numDiffValues - 1;
            newConfound = zeros(length(ALL_IDs), numDiffValues-1);

            % Subjects with the first value 
            % get a -1 in all new columns
            indFinal = find(values(:,k) == diffValues(1));
            newConfound(indFinal,:)=-1;

            % Each new value gets a new column. Subjects 
            % with this value have 1 in this column. 
            for j=2:numDiffValues
                indFinal = find(values(:,k) == diffValues(j));

                newConfound(indFinal,j-1)=1;

                indNotZero = find(newConfound(:,j-1) ~= 0);
                tmpVar = newConfound(indNotZero,j-1);

                % Normalising only makes sense if there is more than 1
                % different value
                newConfound(indNotZero,j-1) = nets_normalise(newConfound(indNotZero,j-1));
            end

            % Add the new columns generated to the final set of columns
            finalConfs=[finalConfs, newConfound];

        end
        for j=1:numNewCols
            final_num_cols = final_num_cols + 1;
            finalNames{final_num_cols} = strcat(baseName{k}, '_', num2str(j));
        end      
    end
    finalNames = finalNames';
end
