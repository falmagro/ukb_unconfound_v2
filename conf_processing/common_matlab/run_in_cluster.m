function [status, cmdout] = run_in_cluster(command)

    finalCommand = strcat('ssh ' <REPLACE WITH USER / CLUSTER> ' bash -l -c ''' , ...
                         'source ~/.bashrc; cd ',...
                         32 ,  pwd, ';', 32, command, '''' );

    [status, cmdout] = system(finalCommand);
    
    % Takes just last line from the ouput (It should have the jobIDs)
    tmp = regexp(cmdout,char(10), 'split');
    if isempty(tmp{end})
        cmdout = tmp{end -1};
    else
        cmdout = tmp{end};
    end
    
    % Stop execution if the command did not return jobIDs (as numbers)
    tmp = regexp(cmdout, ',', 'split');
    
    for i = 1:length(tmp)
        if isempty(str2num(tmp{i}))
            error(['Command "' command '" failed']);
        end
    end
    
    % Wait for the command to finish
    wait_for(cmdout);
end
