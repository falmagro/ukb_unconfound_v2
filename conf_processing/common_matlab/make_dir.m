function make_dir(path_to_dir)
    while path_to_dir(end) == '/'
       path_to_dir = path_to_dir(1:end-1);
    end

    orig_dir=pwd;
    sub_directories = split(path_to_dir, '/');
    
    num_sub = length(sub_directories);
    
    for i = 1:num_sub
        if  exist(sub_directories{i}, 'dir') ~= 7
            mkdir(sub_directories{i});
        end
        cd(sub_directories{i});
    end   
    cd(orig_dir);
end
