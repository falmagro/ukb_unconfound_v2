function [finalConfs, finalNames] = duplicateCategorical(conf_name, ...
                                   ALL_IDs, subjectIndicesBySite, dataName)

    namesFileName = strcat('../data/NAMES_confounds/', conf_name, '.txt');
    baseName = getNames(namesFileName);
    finalNames = {};

    file_to_load=strcat('../data/',dataName,'_data/ID_', conf_name, '.txt');

    values = getValues(file_to_load, ALL_IDs);

    numSites = length(subjectIndicesBySite);
    
    numVars = size(values,2);
    
    final_num_cols=0;
    
    finalConfs=[];

    % For each variable
    for k = 1:numVars
    
        % For each Site
        for i = 1:numSites
            numNewCols = 0;
            subjBySite = subjectIndicesBySite{i};
            numSubj    = length(subjBySite);
            diffValues = my_unique(values(subjBySite,k));
            diffValues = diffValues(~isnan(diffValues));

            countDiffValues = arrayfun(@(x)length(find(values(subjBySite,k) == x)), ...
                                             diffValues, 'Uniform', false);

            % Remove a value for a certain site if there are very few subjects 
            % (less than 8). This case is very likely to be an error in the 
            % settings of the DICOM header.
            diffValuesToRemove = [];

            for j = 1:length(diffValues)
                if countDiffValues{j} <= 8
                    diffValuesToRemove = [diffValuesToRemove j];
                end 
            end

            diffValues(diffValuesToRemove) = [];                                        

            numDiffValues = length(diffValues);

            % If all elements for this site have the same value, we do not 
            % generate a column. Subjects from this site will have 0 in the 
            % columns for the other sites.
            if numDiffValues > 1
                numNewCols = numDiffValues-1;
                newConfound = zeros(length(ALL_IDs), numDiffValues-1);

                % Subjects (of this site) with the first 
                % value get a -1 in all new columns
                indAll = find(values(:,k) == diffValues(1));
                [~,indInt,~] = intersect(indAll,subjBySite);
                indFinal=indAll(indInt);

                newConfound(indFinal,:)=-1;

                % Each new value gets a new column. Subjects (of this Site) with 
                % this value have 1 in this column. All other subjects have a 0.
                for j=2:numDiffValues
                    indAll = find(values(:,k) == diffValues(j));
                    [~,indInt,~]=intersect(indAll,subjBySite);
                    indFinal=indAll(indInt);

                    newConfound(indFinal,j-1)=1;

                    indNotZero = find(newConfound(:,j-1) ~= 0);
                    tmpVar = newConfound(indNotZero,j-1);
                    
                    % Normalising only makes sense if there is more than 1
                    % different value
                    newConfound(indNotZero,j-1) = nets_normalise(newConfound(indNotZero,j-1));
                end

                % Add the new columns generated for this 
                % site to the final set of columns
                finalConfs=[finalConfs, newConfound];

            end
            for j=1:numNewCols
                final_num_cols = final_num_cols + 1;
                finalNames{final_num_cols} = strcat(baseName{k}, '_', ...
                                        num2str(j), '_Site_',num2str(i));
            end      
        end
    end
    finalNames = finalNames';
    %finalConfs = nets_normalise(finalConfs);
end
