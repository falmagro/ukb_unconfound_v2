function my_manhattan_plot(P, ind_IDP_groups, ind_nIDPs_groups, ...
                            nIDPs_group_names, plotTitle, file_name, ...
                            Ylabel, Xlabel)

    num_IDP_groups = length(ind_IDP_groups{1});
    num_nIDPs_groups = length(ind_nIDPs_groups);

    addpath(genpath('common_matlab/'));
    
    [FDR_pID,FDR_pN]=FDR(10.^-P(:),0.05);
    BONF=0.05/prod(size(P(:)));
    
    num_passing = sum(P(:)> (-log10(BONF)));

    f = figure('Visible', 'off', 'Position',[10 10 2000 1200]); 
    colorOrder=get(gca, 'ColorOrder'); 
    colorOrder=[colorOrder;colorOrder;colorOrder]; 
    grot=[];
    for i=1:num_IDP_groups
      plot(nanmax(P(ind_IDP_groups{1}(i).ind,:),[],1),'o','MarkerSize',12); 
      hold on;
      grot(:,i)=nanmax(P(ind_IDP_groups{1}(i).ind,:),[],1)';
    end
    
    max_val = nanmax(P(:));
    for i=1:num_nIDPs_groups
      ind = ind_nIDPs_groups{i}(1);
      plot([ind ind]-0.5,[1 max_val],'k:','LineWidth',2); 
      hold on;
    end


    for i=1:size(grot,1)
      for j=randperm(num_IDP_groups)
        if grot(i,j)>3;
          unique_modalities(j)=plot(i,grot(i,j),'o','MarkerSize',12,...
                                    'MarkerEdgeColor',colorOrder(j,:)); 
          hold on;
        end
      end
    end

    set(gcf,'PaperPositionMode','auto');
    yyaxis left;
    set(gca,'yscale','log','ylim',[1,nanmax(P(:))]);
    set(gca,'xlim',[-20,size(P,2)+20]);
    set(gca, 'FontSize',18);
    set(gca, 'LooseInset', get(gca, 'TightInset'));
    xlabel(Xlabel);
    ylabel(Ylabel);
    plot([1 size(P,2)],[-log10(FDR_pID) -log10(FDR_pID)],'k','LineWidth',3);
    plot([1 size(P,2)],[-log10(BONF) -log10(BONF)],'k','LineWidth',3); 
    legend(unique_modalities,{'T1','T2 FLAIR','swMRI','tfMRI','dMRI',...
                              'rfMRI amplitudes','rfMRI connectivity'},...
                              'Location', 'north');
    title(strcat('-log_1_0 (P values)', plotTitle, ...
                 ' -- Number of associations passing Bonferroni: ', ...
                  num2str(num_passing)));

    yyaxis right;
    set(gca,'yscale','log','ylim',[1,nanmax(P(:))]);
    set(gca,'xlim',[-20,size(P,2)+20]);
    
    set(gca, 'FontSize',18);
    set(gca, 'LooseInset', get(gca, 'TightInset'));

    yticks([-log10(FDR_pID)  -log10(BONF)]);
    yticklabels({'FDR', 'BONF'});
    
    new_x_ticks=[];
    for i = 1:num_nIDPs_groups
        new_x_ticks = [new_x_ticks ; floor(ind_nIDPs_groups{i}(1) + (length(ind_nIDPs_groups{i})/2))];
    end
    xticks(new_x_ticks);
    xticklabels(nIDPs_group_names);
    xtickangle(90);

    a = get(gca, 'XTickLabel');
    set(gca, 'XTickLabel', a, 'FontSize', 15);
    
    set(f, 'defaultAxesColorOrder', [[0 0 0]; [0 0 0]]);
    print('-dpng','-r70',strcat(file_name,'.png'));
end